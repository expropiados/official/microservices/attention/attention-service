package org.expropiados.cuidemonos.attentionservice.config;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.FinalizeAppointmentUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class ScheduleConfig {

    private final FinalizeAppointmentUseCase finalizeAppointmentUseCase;
    private final Logger logger = LoggerFactory.getLogger(ScheduleConfig.class);

    @Scheduled(cron = "0 5,35 * ? * *")
    public void scheduleTaskUsingCronExpression() {

        logger.info("Changing appointments status to FINISHED");
        finalizeAppointmentUseCase.finalizeAppointment();

        long now = System.currentTimeMillis() / 1000;
        System.out.println(
                "schedule tasks using cron jobs - " + now);

    }

}
