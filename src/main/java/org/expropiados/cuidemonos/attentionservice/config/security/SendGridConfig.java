package org.expropiados.cuidemonos.attentionservice.config.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter @Setter
@ConfigurationProperties(prefix = "security.sendgrid")
public class SendGridConfig {
    private Boolean enabled;
    private String apiKey;
    private String senderEmail;
    private Templates templates;
    private Variables variables;

    @Getter @Setter
    public static class Templates {
        private String appointmentRegisteredPatientId;
        private String appointmentRegisteredSpecialistId;
    }

    @Getter @Setter
    public static class Variables {
        private String checkAppointmentUrl;
    }
}
