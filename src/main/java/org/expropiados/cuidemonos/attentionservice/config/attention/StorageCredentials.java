package org.expropiados.cuidemonos.attentionservice.config.attention;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter @Setter
@ConfigurationProperties("attention.storage.aws")
public class StorageCredentials {
    private String region;
    private Credentials credentials;
    private Bucket bucket;

    @Getter @Setter
    public static class Credentials{
        private String accessKey;
        private String secretKey;
    }

    @Getter @Setter
    public static class Bucket{
        private String name;
    }

}
