package org.expropiados.cuidemonos.attentionservice.config;

import java.time.LocalDateTime;
import java.time.ZoneId;

public final class LocalDateTimePeruZone {
    private LocalDateTimePeruZone(){

    }
    //Zona Peru
    public static LocalDateTime now(){
        return LocalDateTime.now(ZoneId.of("GMT-05"));
    }
}
