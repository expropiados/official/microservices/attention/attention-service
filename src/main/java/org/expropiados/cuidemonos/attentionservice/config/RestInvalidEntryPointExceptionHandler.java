package org.expropiados.cuidemonos.attentionservice.config;

import lombok.Builder;
import lombok.Getter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.exception.InvalidDateEntryException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class RestInvalidEntryPointExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ InvalidDateEntryException.class })
    public ResponseEntity<InvalidDateRestError> handleBusinessAndSystemExceptions(Exception ex) {
        var status = HttpStatus.FORBIDDEN;

        var error = buildInvalidRestError((InvalidDateEntryException) ex);
        logger.error(error.getMessage());
        return new ResponseEntity<>(error, new HttpHeaders(), status);
    }

    @Builder @Getter
    public static class InvalidDateRestError{
        String code;
        String message;
        LocalDateTime initDate;
        LocalDateTime endDate;
    }


    private InvalidDateRestError buildInvalidRestError(InvalidDateEntryException ex){
        return InvalidDateRestError.builder()
                .code(HttpStatus.FORBIDDEN.toString())
                .message(ex.getMessage())
                .initDate(ex.getInitDate())
                .endDate(ex.getEndDate()).build();
    }

}
