package org.expropiados.cuidemonos.attentionservice.config.attention;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
@ConfigurationProperties(prefix = "attention.recording.app")
public class RecordingAppConfig {
    private Integer uidRecorder;
    private String username;
    private String password;
}
