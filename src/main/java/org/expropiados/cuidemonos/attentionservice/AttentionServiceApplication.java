package org.expropiados.cuidemonos.attentionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class AttentionServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AttentionServiceApplication.class, args);
    }

}
