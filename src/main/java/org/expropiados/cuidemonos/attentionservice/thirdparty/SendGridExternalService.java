package org.expropiados.cuidemonos.attentionservice.thirdparty;

import com.sendgrid.*;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.security.SendGridConfig;
import org.expropiados.cuidemonos.attentionservice.hexagonal.ExternalServiceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

@ExternalServiceAdapter
@RequiredArgsConstructor
public class SendGridExternalService {

    private final Logger logger = LoggerFactory.getLogger(SendGridExternalService.class);
    private final SendGridConfig sendGridConfig;

    public void sendEmail(String email, Map<String, String> data, String templateId) {
        if (!sendGridConfig.getEnabled()) {
            logger.warn("Avoid sending email to '{}' with template '{}'", email, templateId);
            return;
        }

        var from = new Email(sendGridConfig.getSenderEmail());
        var to = new Email(email);

        var mail = new Mail();
        mail.setFrom(from);
        mail.setTemplateId(templateId);

        var personalization = buildPersonalization(to, data);
        mail.addPersonalization(personalization);

        var sg = new SendGrid(sendGridConfig.getApiKey());
        var request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            logger.info("Sending email to '{}' with template '{}'", email, templateId);
            sg.api(request);

        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    private Personalization buildPersonalization(Email to, Map<String, String> data) {
        var personalization = new Personalization();
        personalization.addTo(to);
        data.forEach(personalization::addDynamicTemplateData);

        return personalization;
    }
}
