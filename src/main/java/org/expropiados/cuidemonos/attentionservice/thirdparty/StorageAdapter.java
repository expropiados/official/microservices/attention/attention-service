package org.expropiados.cuidemonos.attentionservice.thirdparty;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.attention.StorageCredentials;
import org.expropiados.cuidemonos.attentionservice.hexagonal.S3Adapter;
import org.expropiados.cuidemonos.attentionservice.hexagonal.converter.MultipartFileConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Data
@S3Adapter
@RequiredArgsConstructor
public class StorageAdapter {

    private final StorageCredentials storageCredentials;
    private final MultipartFileConverter multipartFileConverter;
    private final Logger logger = LoggerFactory.getLogger(StorageAdapter.class);

    public AmazonS3 getS3Client(){
        var awsCredentials = new BasicAWSCredentials(
                storageCredentials.getCredentials().getAccessKey(),
                storageCredentials.getCredentials().getSecretKey()
        );

        return AmazonS3ClientBuilder.standard()
                .withRegion(storageCredentials.getRegion())
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }

    public String saveFile(MultipartFile multipartFile){
        try {
            var file = multipartFileConverter.convertMultipartFileToFile(multipartFile);

            var request = new PutObjectRequest(storageCredentials.getBucket().getName(), file.getName(), file)
                    .withCannedAcl(CannedAccessControlList.PublicRead);

            this.getS3Client().putObject(request);

            return this.getS3Client()
                    .getUrl(storageCredentials.getBucket().getName(), file.getName())
                    .toString();

        } catch (IOException e) {
            logger.error("An error occurred while saving the data file to the AWS S3 service", e);
            return null;
        }
    }

}
