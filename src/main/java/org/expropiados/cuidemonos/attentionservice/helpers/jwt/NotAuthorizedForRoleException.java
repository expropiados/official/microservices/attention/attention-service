package org.expropiados.cuidemonos.attentionservice.helpers.jwt;

import lombok.Getter;
import org.expropiados.cuidemonos.attentionservice.hexagonal.errors.AuthenticationException;

@Getter
public class NotAuthorizedForRoleException extends RuntimeException implements AuthenticationException {
    private final String code = "SEC_TKN_003";
    private final String message;
    private final Object data;

    public NotAuthorizedForRoleException(UserRole role) {
        super();
        this.message = String.format("You're not authorized to execute this service with role %d.", role.getValue());
        this.data = null;
    }
}
