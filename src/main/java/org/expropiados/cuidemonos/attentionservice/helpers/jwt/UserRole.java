package org.expropiados.cuidemonos.attentionservice.helpers.jwt;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum UserRole {
    PATIENT(0),
    SPECIALIST(1),
    CLIENT_SUPERVISOR(2),
    PROVIDER_SUPERVISOR(3),
    ADMIN(4);

    private final Integer value;

    public static UserRole valueOf(Integer value) {
        switch (value) {
            case 0: return UserRole.PATIENT;
            case 1: return UserRole.SPECIALIST;
            case 2: return UserRole.CLIENT_SUPERVISOR;
            case 3: return UserRole.PROVIDER_SUPERVISOR;
            case 4: return UserRole.ADMIN;
            default: return null;
        }
    }
}
