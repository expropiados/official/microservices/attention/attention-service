package org.expropiados.cuidemonos.attentionservice.helpers.jwt;

import lombok.Getter;
import org.expropiados.cuidemonos.attentionservice.hexagonal.errors.AuthenticationException;

@Getter
public class InvalidRoleException extends RuntimeException implements AuthenticationException {
    private final String code = "SEC_TKN_002";
    private final String message;
    private final Object data;

    public InvalidRoleException(Integer role) {
        super();
        this.message = String.format("Invalid role '%s'. Need to send a right role.", role);
        this.data = null;
    }
}
