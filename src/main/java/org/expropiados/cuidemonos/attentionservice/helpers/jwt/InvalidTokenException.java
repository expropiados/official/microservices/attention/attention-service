package org.expropiados.cuidemonos.attentionservice.helpers.jwt;

import lombok.Getter;
import org.expropiados.cuidemonos.attentionservice.hexagonal.errors.AuthenticationException;

@Getter
public class InvalidTokenException extends RuntimeException implements AuthenticationException {
    private final String code = "SEC_TKN_001";
    private final String message;
    private final Object data;

    public InvalidTokenException(String token) {
        super();
        this.message = String.format("Invalid token '%s'. Please login again.", token);
        this.data = null;
    }
}
