package org.expropiados.cuidemonos.attentionservice.hexagonal.converter;

import org.expropiados.cuidemonos.attentionservice.hexagonal.S3Adapter;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@S3Adapter
public class MultipartFileConverter {

    public File convertMultipartFileToFile(MultipartFile multipartFile) throws IOException {
        var file = new File(multipartFile.getOriginalFilename());
        try (var fileOutputStream = new FileOutputStream(file)) {
            fileOutputStream.write(multipartFile.getBytes());
        }
        return file;
    }

}
