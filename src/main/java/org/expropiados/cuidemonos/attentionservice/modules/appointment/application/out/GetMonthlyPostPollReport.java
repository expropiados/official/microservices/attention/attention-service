package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out;

import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
@Builder
public class GetMonthlyPostPollReport {
    List<GetMonthlyGeneralReport> seriesLine;
    List<GetMonthlyPostPollByRatingReport> seriesStackedColumn;
}
