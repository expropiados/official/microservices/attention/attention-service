package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

public interface GetAnnotationsUseCase {
    String getAnnotations(Long idVideoCall);
}
