package org.expropiados.cuidemonos.attentionservice.modules.appointment.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class Appointment {
    private Long id;
    private AppointmentDetail appointmentDetail;
    private UUID specialistUuid;
    private UUID patientUuid;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private int state;
    private User patient;
    private User specialist;
    private Boolean isCancellable;
    private UUID videoCallUuid;
    private PreAppointmentPoll preAppointmentPoll;
}
