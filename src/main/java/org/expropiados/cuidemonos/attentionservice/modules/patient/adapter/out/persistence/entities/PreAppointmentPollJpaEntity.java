package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.entities;

import lombok.Data;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.AppointmentJpaEntity;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "POLL_PRE_APPOINMENT")
public class PreAppointmentPollJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "poll_pre_appointment_id")
    private Long idPreAppointmentPoll;

    @Column(name = "appointment_id")
    private Long idAppointment;

    @Column(name = "medical_reason_id")
    private Long idMedicalReason;

    @Column(name = "another_reason", length = 1000)
    private String anotherReason;

    @Column(name = "additional_info", length = 1000)
    private String additionalInfo;

    @OneToOne
    @JoinColumn(name = "medical_reason_id", insertable = false, updatable = false)
    private MedicalReasonJpaEntity medicalReasonJpaEntity;

    @OneToOne
    @JoinColumn(name = "appointment_id", insertable = false, updatable = false)
    private AppointmentJpaEntity appointmentJpaEntity;

}
