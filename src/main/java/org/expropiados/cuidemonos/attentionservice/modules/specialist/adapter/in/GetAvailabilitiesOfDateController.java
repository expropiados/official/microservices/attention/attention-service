package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in.mappers.GetAvailabilitiesOfDateMapper;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.commands.AvailabilityOfDateCommand;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetAvailabilitiesOfDateUseCase;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/specialists")
public class GetAvailabilitiesOfDateController {

    private final JwtHelper jwtHelper;
    private final GetAvailabilitiesOfDateUseCase getAvailabilitiesOfDateUseCase;

    @GetMapping("/availability")
    @ApiOperation("Register availability from specialist")
    public ResponseEntity<List<AvailabilityOfDateCommand>> getAvailabilitiesOfDate(
                                                     @RequestParam(name = "dateBegin") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate dateBegin,
                                                     @RequestParam(name = "dateEnd") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate dateEnd,
                                                     @RequestHeader("Authorization") String token,
                                                     @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var specialists = getAvailabilitiesOfDateUseCase.getSpecialistsAvailabilitiesOfRangeDate(dateBegin, dateEnd);

        return ResponseEntity.ok(specialists);
    }

}
