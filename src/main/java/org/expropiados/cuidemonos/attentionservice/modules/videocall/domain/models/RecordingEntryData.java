package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RecordingEntryData {
    String agoraTokenRecorder;
    Boolean firstTimeAgoraTokenRecorder;
    String resourceIndividualId;
    String sidIndividual;
    String resourceDualId;
    String sidDual;
    String authTokenRecorder;
    Integer uidRecorder;
}
