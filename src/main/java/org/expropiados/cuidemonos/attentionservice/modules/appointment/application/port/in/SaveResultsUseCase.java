package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;

public interface SaveResultsUseCase {
    DetailAppointment saveResults(Long appointmentDetail, String results);
}
