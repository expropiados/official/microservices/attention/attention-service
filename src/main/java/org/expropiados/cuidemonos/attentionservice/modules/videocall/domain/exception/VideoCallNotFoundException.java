package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class VideoCallNotFoundException extends RuntimeException{
    public VideoCallNotFoundException(){
        super("Videollamada no ha sido encontrada");
    }
}
