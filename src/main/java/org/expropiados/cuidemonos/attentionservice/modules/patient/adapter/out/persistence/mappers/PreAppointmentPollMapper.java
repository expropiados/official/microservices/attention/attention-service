package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.entities.PreAppointmentPollJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import java.util.List;

@Mapper(componentModel = "spring", uses = {MedicalReasonMapper.class})
public interface PreAppointmentPollMapper {
    @Mapping(source = "idPreAppointmentPoll", target = "id")
    @Mapping(source = "idAppointment", target = "appointmentId")
    @Mapping(source = "idMedicalReason", target = "medicalReasonId")
    @Mapping(source = "additionalInfo", target = "additionalInfo")
    @Mapping(source = "anotherReason", target = "anotherReason")
    @Mapping(source = "medicalReasonJpaEntity", target = "medicalReason")
    PreAppointmentPoll toPreAppointmentPoll(PreAppointmentPollJpaEntity preAppointmentPollJpaEntity);
    List<PreAppointmentPoll> toPreAppointmentPollList(List<PreAppointmentPollJpaEntity> preAppointmentPollJpaEntityList);

    @InheritInverseConfiguration
    @Mapping(target = "idPreAppointmentPoll", ignore = true)
    PreAppointmentPollJpaEntity toPreAppointmentPollEntity(PreAppointmentPoll preAppointmentPoll);
}
