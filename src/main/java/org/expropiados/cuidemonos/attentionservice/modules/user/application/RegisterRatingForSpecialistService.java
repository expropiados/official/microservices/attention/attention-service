package org.expropiados.cuidemonos.attentionservice.modules.user.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.in.RegisterRatingForSpecialistUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.RegisterRatingForSpecialistPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class RegisterRatingForSpecialistService implements RegisterRatingForSpecialistUseCase {
    private final Logger logger = LoggerFactory.getLogger(RegisterRatingForSpecialistService.class);
    private final RegisterRatingForSpecialistPort registerRatingForSpecialistPort;

    @Override
    public void registerRatingForSpecialist(UUID specialistUserUuid, Integer rating) {
        try {
            registerRatingForSpecialistPort.registerRatingForSpecialist(specialistUserUuid, rating);
        } catch (Exception exception) {
            logger.error("Unknown error when registering rating for specialist with user UUID '{}'", specialistUserUuid.toString());
        }
    }
}
