package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

public interface SendEmailForAppointmentPort {
    void sendEmailPatientForAppointment(User patient, User specialist, String email, Appointment appointment);
    void sendEmailSpecialistForAppointment(User patient, User specialist, String email, Appointment appointment);
}
