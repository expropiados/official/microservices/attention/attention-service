package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;

public interface SaveAnnotationsPort {
    DetailAppointment saveAnnotations(Long idAppointment, String annotations);
}
