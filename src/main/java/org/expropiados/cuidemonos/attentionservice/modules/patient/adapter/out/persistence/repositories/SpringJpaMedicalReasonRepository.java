package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.entities.MedicalReasonJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpringJpaMedicalReasonRepository extends JpaRepository<MedicalReasonJpaEntity, Long> {
    List<MedicalReasonJpaEntity> findAllByDeleted(Boolean deleted);
}
