package org.expropiados.cuidemonos.attentionservice.modules.videocall.util;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

public class RandomString {

    /**
     * Generate a random string.
     */
    public String nextString() {
        for (var idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    public String randomIntegerValue(){
        var rand = new SecureRandom();
        var key = rand.nextInt(MAX_UID);
        return ((Integer) key).toString();
    }

    public String randomFolderName(){
        var bufFolder = new char[20];
        for (var idx = 0; idx < 20; idx++)
            bufFolder[idx] = digitsOnly[random.nextInt(digitsOnly.length)];
        return new String(bufFolder);
    }

    public static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String LOWER = UPPER.toLowerCase(Locale.ROOT);

    public static final String DIGITS = "0123456789";

    private static final Integer MAX_UID = 99999999;

    public static final String ALPHANUM = UPPER + LOWER + DIGITS;

    private final Random random;

    private final char[] symbols;

    private final char[] digitsOnly ={'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    private final char[] buf;

    public RandomString(int length, Random random, String symbols) {
        if (length < 1) throw new IllegalArgumentException();
        if (symbols.length() < 2) throw new IllegalArgumentException();
        this.random = Objects.requireNonNull(random);
        this.symbols = symbols.toCharArray();
        this.buf = new char[length];
    }

    /**
     * Create an alphanumeric string generator.
     */
    public RandomString(int length, Random random) {
        this(length, random, ALPHANUM);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     */
    public RandomString(int length) {
        this(length, new SecureRandom());
    }

    /**
     * Create session identifiers.
     */
    public RandomString() {
        this(12);
    }

}
