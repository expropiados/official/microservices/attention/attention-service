package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;

import java.util.List;

public interface SavePostPollAnswersPort {
    PostPoll saveResults(PostPoll postPoll);
    void saveAudioIssues(List<PostPoll> postPolls);
    void saveSpecialistIssues(List<PostPoll> postPolls);
    void saveVideoIssues(List<PostPoll> postPolls);
}
