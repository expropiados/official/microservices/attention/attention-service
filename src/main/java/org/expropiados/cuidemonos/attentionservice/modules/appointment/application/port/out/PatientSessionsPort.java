package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface PatientSessionsPort {
    List<User> patientSessions(UUID specialistId, UUID patientId);
}
