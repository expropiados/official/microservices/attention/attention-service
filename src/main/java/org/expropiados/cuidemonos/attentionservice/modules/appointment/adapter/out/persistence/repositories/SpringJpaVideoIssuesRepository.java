package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.VideoIssuesJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpringJpaVideoIssuesRepository extends JpaRepository<VideoIssuesJpaEntity, Long> {
}
