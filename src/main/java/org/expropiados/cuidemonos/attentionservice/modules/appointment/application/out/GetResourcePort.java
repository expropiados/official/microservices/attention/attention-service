package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;

import java.util.List;

public interface GetResourcePort {
    List<Resource> getResources(Long idDetailAppointment);
}
