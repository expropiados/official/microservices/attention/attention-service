package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import lombok.Data;

import java.util.ArrayList;

@Data
public class PostPollDTO {
    private String callRating;
    private ArrayList<String> selectedAudioIssues;
    private ArrayList<String> selectedVideoIssues;
    private String callComments;
    private String doctorRating;
    private ArrayList<String> selectedDoctorIssues;
    private String doctorComments;
}
