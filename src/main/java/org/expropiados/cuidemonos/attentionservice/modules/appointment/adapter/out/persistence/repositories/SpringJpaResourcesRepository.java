package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.ResourceJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpringJpaResourcesRepository extends JpaRepository<ResourceJpaEntity, Long> {
    List<ResourceJpaEntity> findAllByAppointmentDetailJpaEntityIdResultAppointment(Long idDetailAppointment);

}
