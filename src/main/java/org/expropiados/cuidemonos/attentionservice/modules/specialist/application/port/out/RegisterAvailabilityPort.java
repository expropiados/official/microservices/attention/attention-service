package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.util.List;

public interface RegisterAvailabilityPort {
    void registerAvailability(List<Availability> availabilities);
}
