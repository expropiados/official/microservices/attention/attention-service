package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetPromScoreMonthlyReport;

import java.time.LocalDate;

public interface GetPromScoreMonthlyReportUseCase {
    GetPromScoreMonthlyReport getPromScoreMonthlyReport(LocalDate startDate, LocalDate endDate);
}
