package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class InvalidDateEntryException extends RuntimeException {
    private LocalDateTime initDate;
    private LocalDateTime endDate;

    public InvalidDateEntryException(LocalDateTime initDate,
                                     LocalDateTime endDate,
                                     String cause){
        super("No puede acceder a la videollamada: "+ cause);
        this.setInitDate(initDate);
        this.setEndDate(endDate);
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public LocalDateTime getInitDate() {
        return initDate;
    }

    public void setInitDate(LocalDateTime initDate) {
        this.initDate = initDate;
    }
}
