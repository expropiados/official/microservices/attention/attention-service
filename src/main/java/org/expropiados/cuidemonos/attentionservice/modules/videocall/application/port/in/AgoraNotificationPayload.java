package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import lombok.Data;

@Data
public class AgoraNotificationPayload {
    private String cname;
    private AgoraNotificationsDetails details;
    private Long sendts;
    private Integer sequence;
    private Integer serviceType;
    private String sid;
    private String uid;
}
