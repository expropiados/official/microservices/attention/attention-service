package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.errors.AppointmentForSpecialistNotFoundError;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.GetAppointmentUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.ListPatientsPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@UseCase
@RequiredArgsConstructor
public class GetAppointmentService implements GetAppointmentUseCase {

    private final GetAppointmentPort getAppointmentForSpecialistPort;
    private final ListPatientsPort listPatientsPort;

    @Override
    public List<Appointment> getAppointmentForSpecialist(UUID specialistUuid, LocalDate dateBegin, LocalDate dateEnd) {
        var appointments = getAppointmentForSpecialistPort
                .getAppointmentForSpecialist(specialistUuid,dateBegin, dateEnd)
                .orElseThrow(() -> new AppointmentForSpecialistNotFoundError(specialistUuid, dateBegin));
        var patientsUuid = listAppointmentUuids(appointments);
        var patients = listPatientsPort.listPatients(patientsUuid);
        return completeAppointmentWithPatientData(appointments,patients);
    }

    public List<UUID> listAppointmentUuids(List<Appointment> appointments){
        return appointments.stream().map(appointment -> appointment.getPatientUuid()).collect(Collectors.toList());
    }

    public List<Appointment> completeAppointmentWithPatientData(List<Appointment> appointments, Map<UUID,User> users){
        appointments
                .forEach(appointment -> {
                    var user = users.get(appointment.getPatientUuid());
                    appointment.setPatient(user);
                });
        return appointments;
    }

}
