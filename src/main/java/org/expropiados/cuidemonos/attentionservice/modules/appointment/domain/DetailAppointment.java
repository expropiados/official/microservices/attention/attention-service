package org.expropiados.cuidemonos.attentionservice.modules.appointment.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class DetailAppointment {
    private Appointment appointment;
    private Long appointmentDetailId;
    private String annotations;
    private String results;
    private ResourceOwner resourcesPatient;
    private ResourceOwner resourcesSpecialist;
}
