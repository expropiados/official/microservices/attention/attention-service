package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;

public interface GetDetailAppointmentUseCase {
    DetailAppointment getDetailAppointment(Long idAppointment);
}
