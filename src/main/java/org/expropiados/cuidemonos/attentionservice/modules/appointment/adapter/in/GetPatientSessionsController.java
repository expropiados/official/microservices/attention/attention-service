package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.GetAppointmentUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PatientSessionsDTO;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.PatientSessionsUseCase;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/specialists")
public class GetPatientSessionsController {

    private final JwtHelper jwtHelper;
    private final PatientSessionsUseCase patientSessionsUseCase;

    @GetMapping("/{uuidSpecialist}/patientSessions")
    @ApiOperation("Get appointments for specialist")
    public ResponseEntity<PatientSessionsDTO> getPatientSessions(@PathVariable UUID uuidSpecialist,
                                                             @RequestParam (name = "uuidPatient") UUID uuidPatient,
                                                             @RequestHeader("Authorization") String token,
                                                             @RequestHeader("role") Integer role) {

        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var patientSessions = patientSessionsUseCase.patientSessions(uuidSpecialist, uuidPatient);

        return ResponseEntity.ok(patientSessions);
    }

}

