package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.LocalDateTimePeruZone;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.entities.RecordingJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.mappers.RecordingMapper;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.persistence.SpringJpaRecordingRepository;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.GetLastRecordingPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveRecordingParametersPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveRecordingPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.RecordingType;

@PersistenceAdapter
@RequiredArgsConstructor
public class RecordingPersistenceAdapter implements SaveRecordingPort, GetLastRecordingPort, SaveRecordingParametersPort {

    private final SpringJpaRecordingRepository recordingRepository;
    private final RecordingMapper recordingMapper;

    @Override
    public Recording saveRecording(Long videoCallId, String folderName, Integer recordingType) {
        var lastResult = recordingRepository.getFirstByVideoCallIdOrderByCreatedAtDesc(videoCallId);
        Integer recordingNum;
        if (lastResult.isEmpty()) recordingNum = 1;
        else recordingNum = lastResult.get().getRecordingNum()+1;

        var entityJpa = new RecordingJpaEntity();
        entityJpa.setVideoCallId(videoCallId);
        entityJpa.setFolderName(folderName);
        entityJpa.setRecordingNum(recordingNum);
        entityJpa.setRecordingType(recordingType);
        entityJpa.setCreatedAt(LocalDateTimePeruZone.now());
        var result = recordingRepository.save(entityJpa);
        return recordingMapper.toRecording(result);
    }

    @Override
    public Recording getLastRecordingOfVideoCall(Long videoCallId) {
        var lastResult = recordingRepository.getFirstByVideoCallIdOrderByCreatedAtDesc(videoCallId);
        if (lastResult.isEmpty()) return null;
        return recordingMapper.toRecording(lastResult.get());
    }

    @Override
    public Recording getLastRecordingOfVideoCallFromType(Long videoCallId, RecordingType recordingType) {
        var lastResult = recordingRepository.getFirstByVideoCallIdAndRecordingTypeOrderByCreatedAtDesc(videoCallId, recordingType.getValue());
        if (lastResult.isEmpty()) return null;
        return recordingMapper.toRecording(lastResult.get());
    }

    @Override
    public Recording saveRecodingParameters(Long videoCallId, String resourceId, String sid, String folderName) {
        var lastResult = recordingRepository.getByVideoCallIdAndFolderName(videoCallId, folderName);
        if (lastResult.isEmpty()) return null;
        var entity = lastResult.get();
        entity.setResourceId(resourceId);
        entity.setSid(sid);
        var result = recordingRepository.save(entity);
        return recordingMapper.toRecording(result);
    }
}
