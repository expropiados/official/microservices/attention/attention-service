package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.ResourceMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories.SpringJpaResourcesRepository;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetResourcePort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveResourcePort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class ResourcePersistenceAdapter implements GetResourcePort, SaveResourcePort {

    private final SpringJpaResourcesRepository resourcesRepository;
    private final ResourceMapper resourceMapper;
    @Override
    public List<Resource> getResources(Long idAppointmentDetail) {

       var resourcesEnt = resourcesRepository.findAllByAppointmentDetailJpaEntityIdResultAppointment(idAppointmentDetail);
       return resourceMapper.toResources(resourcesEnt);

    }

    @Override
    public Resource saveResource(Resource resource) {
        var rowEntity = resourceMapper.toResourceJpaEntity(resource);
        var rowSaved = resourcesRepository.save(rowEntity);
        return resourceMapper.toResource(rowSaved);
    }
}
