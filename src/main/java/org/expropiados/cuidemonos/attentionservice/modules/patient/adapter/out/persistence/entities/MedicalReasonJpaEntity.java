package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "MEDICAL_REASON", schema = "backoffice")
public class MedicalReasonJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "medical_reason_id")
    private Long idMedicalReason;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "description", length = 300)
    private String description;

    @Column(name = "name", length = 100)
    private String name;

}
