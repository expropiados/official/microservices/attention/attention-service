package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

public interface GetAgoraChannelNameUseCase {
    String getAgoraNameChannelName(Long idAppointment);
}
