package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import lombok.Data;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class GetAvailabilitiesOfRangeDateWithAppointmentInfoDTO {
    private Long id;
    private UUID specialistUuid;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Integer state;
    private User patient;
    private Long sessionId;
}
