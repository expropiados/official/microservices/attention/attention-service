package org.expropiados.cuidemonos.attentionservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetAvailabilityUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.util.List;
import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetAvailabilityService implements GetAvailabilityUseCase {

    private final GetAvailabilityPort getAvailabilityPort;

    @Override
    public List<Availability> getAvailability(UUID especialistId) {
        return getAvailabilityPort.getAvailabilityList(especialistId);
    }
}
