package org.expropiados.cuidemonos.attentionservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.RegisterPrePollUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.RegisterPrePollPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class RegisterPreAppointmentPollService implements RegisterPrePollUseCase {
    private final RegisterPrePollPort registerPrePoolPort;

    @Override
    public void registerPrePoll(Long idAppointment, PreAppointmentPoll pool) {
        pool.setAppointmentId(idAppointment);
        registerPrePoolPort.registerPrePool(pool);
    }
}
