package org.expropiados.cuidemonos.attentionservice.modules.videocall.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RecordingParametersInput;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.SaveRecordingParametersUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveIsFirstTimeAgoraPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveRecordingParametersPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.exception.VideoCallNotFoundException;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;

@UseCase
@RequiredArgsConstructor
public class SaveRecordingParametersService implements SaveRecordingParametersUseCase {

    public final SaveIsFirstTimeAgoraPort saveIsFirstTimeAgoraPort;
    public final SaveRecordingParametersPort saveRecordingParametersPort;

    @Override
    public VideoCall saveFirstTimeAgora(RecordingParametersInput recordingParametersInput) {
        var result =  saveIsFirstTimeAgoraPort.saveFirstTokenBoolean(recordingParametersInput.getVideoCallId(),
                 recordingParametersInput.isFirstTimeAgoraTokenRecorder());
        if (result == null)
            throw new VideoCallNotFoundException();
        return result;
    }

    @Override
    public Recording saveRecordingParameters(RecordingParametersInput recordingParametersInput) {
        return saveRecordingParametersPort.saveRecodingParameters(recordingParametersInput.getVideoCallId(),
                recordingParametersInput.getResourceId(), recordingParametersInput.getSid(), recordingParametersInput.getFolderName());
    }
}
