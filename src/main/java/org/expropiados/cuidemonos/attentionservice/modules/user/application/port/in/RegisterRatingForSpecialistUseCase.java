package org.expropiados.cuidemonos.attentionservice.modules.user.application.port.in;

import java.util.UUID;

public interface RegisterRatingForSpecialistUseCase {
    void registerRatingForSpecialist(UUID specialistUserUuid, Integer rating);
}
