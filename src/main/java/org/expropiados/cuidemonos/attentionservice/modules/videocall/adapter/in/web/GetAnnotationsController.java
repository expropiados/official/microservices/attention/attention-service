package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.GetAnnotationsUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/video_call")
public class GetAnnotationsController {

    private final JwtHelper jwtHelper;
    private final GetAnnotationsUseCase getAnnotationsUseCase;

    @GetMapping("/{idVideoCall}/appointments/annotation")
    public ResponseEntity<String> saveAnnotations(@PathVariable Long idVideoCall,
                                                  @RequestHeader("Authorization") String token,
                                                  @RequestHeader("role") Integer role) {

        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if(userRole != UserRole.SPECIALIST){
            throw new NotAuthorizedForRoleException(userRole);
        }

        var annotations = getAnnotationsUseCase.getAnnotations(idVideoCall);

        return ResponseEntity.ok(annotations);

    }

}
