package org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface ListPatientsPort {
    Map<UUID, User> listPatients(List<UUID> uuidList);
    Map<UUID, User> listSpecialists(List<UUID> uuidList);
    List<User> listPatientsByName(UUID specialistUuid, String search);
}