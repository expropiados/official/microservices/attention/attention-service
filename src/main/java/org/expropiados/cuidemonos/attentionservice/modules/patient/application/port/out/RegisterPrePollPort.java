package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;

public interface RegisterPrePollPort {
    void registerPrePool(PreAppointmentPoll pool);
}
