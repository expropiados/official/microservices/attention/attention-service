package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonMonthlyFreqReport;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonMonthlyFreqReportUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonMonthlyReportUseCase;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/report/freq_monthly")
public class GetMedicalReasonMonthlyFreqReportController {
    private final GetMedicalReasonMonthlyReportUseCase getMedicalReasonMonthlyReportUseCase;
    private final GetMedicalReasonMonthlyFreqReportUseCase getMedicalReasonMonthlyFreqReportUseCase;

    @GetMapping("")
    public GetMedicalReasonMonthlyFreqReport getMedicalReasonMonthlyReport(GetMedicalReasonMonthlyFreqReportDTO getMedicalReasonMonthlyReportDTO){
        getMedicalReasonMonthlyReportDTO.setStartDate(LocalDate.of(getMedicalReasonMonthlyReportDTO.getStartDate().getYear(), getMedicalReasonMonthlyReportDTO.getStartDate().getMonth(), 1));
        getMedicalReasonMonthlyReportDTO.setEndDate(LocalDate.of(getMedicalReasonMonthlyReportDTO.getEndDate().getYear(), getMedicalReasonMonthlyReportDTO.getEndDate().getMonth(), 1));
        var monthlyReport = getMedicalReasonMonthlyReportUseCase.getMedicalReasonMonthlyReport(getMedicalReasonMonthlyReportDTO.getStartDate(), getMedicalReasonMonthlyReportDTO.getEndDate(), getMedicalReasonMonthlyReportDTO.getClientCompanyId());
        return getMedicalReasonMonthlyFreqReportUseCase.getMedicalReasonMonthlyFreqReport(monthlyReport);
    }
}
