package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value
@EqualsAndHashCode(callSuper = false)
public class GenerateVideoCallInput {
    @NotNull
    VideoCall videoCall;

    @NotNull
    UUID userUUID;

    @NotNull
    Integer userType;
}
