package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveResultsUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/appointments")
public class SaveResultsController {

    private final JwtHelper jwtHelper;
    private final SaveResultsUseCase saveResultsUseCase;

    @PostMapping("/{sessionId}/results")
    @ApiOperation("Register results of session")
    public ResponseEntity<DetailAppointment> saveResults(@PathVariable Long sessionId,
                                                         @RequestBody String results,
                                                         @RequestHeader("Authorization") String token,
                                                         @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var appointmentDetail = saveResultsUseCase.saveResults(sessionId, results);

        return ResponseEntity.ok(appointmentDetail);

    }




}
