package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.entities.RecordingJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring", uses = {VideoCallMapper.class})
public interface RecordingMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "videoCall", target = "videoCall")
    @Mapping(source = "recordingNum", target = "recordingNum")
    @Mapping(source = "resourceId", target = "resourceId")
    @Mapping(source = "sid", target = "sid")
    @Mapping(source = "recordingType", target = "recordingType")
    @Mapping(source = "folderName", target = "folderName")
    Recording toRecording(RecordingJpaEntity recordingJpaEntity);
}
