package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum RecordingType {
    DUAL(0),
    INDIVIDUAL(1);

    private final Integer value;

    public static RecordingType valueOf(Integer value) {
        switch (value) {
            case 0: return RecordingType.INDIVIDUAL;
            case 1: return RecordingType.DUAL;
            default: return null;
        }
    }
}
