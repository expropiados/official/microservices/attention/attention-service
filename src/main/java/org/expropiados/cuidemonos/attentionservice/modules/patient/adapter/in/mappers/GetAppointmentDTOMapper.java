package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in.GetAppointmentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface GetAppointmentDTOMapper {

    @Mapping(source = "id", target = "idAppointment")
    @Mapping(source = "startTime", target = "startTime")
    @Mapping(source = "endTime", target = "endTime")
    @Mapping(source = "state", target = "state")
    @Mapping(source = "specialist", target = "specialist")
    GetAppointmentDTO toGetAppointmentDTO(Appointment appointment);
    List<GetAppointmentDTO> toGetAppointmentDTOs(List<Appointment> appointments);
}
