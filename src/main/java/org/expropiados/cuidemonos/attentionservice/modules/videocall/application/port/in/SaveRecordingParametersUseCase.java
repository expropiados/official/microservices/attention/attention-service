package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;

public interface SaveRecordingParametersUseCase {
    VideoCall saveFirstTimeAgora(RecordingParametersInput recordingParametersInput);
    Recording saveRecordingParameters(RecordingParametersInput recordingParametersInput);
}
