package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in.GetMonthlyPostPollReportUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetMonthlyPostPollReport;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/report/post-poll/monthly")
public class GetMonthlyPostPollReportController {
    private final GetMonthlyPostPollReportUseCase getMonthlyPostPollReportUseCase;

    @GetMapping("")
    public GetMonthlyPostPollReport getMonthlyPostPollReport(GetPostPollMonthlyReportDTO getPostPollMonthlyReportDTO){
        getPostPollMonthlyReportDTO.setStartDate(LocalDate.of(getPostPollMonthlyReportDTO.getStartDate().getYear(), getPostPollMonthlyReportDTO.getStartDate().getMonth(), 1));
        getPostPollMonthlyReportDTO.setEndDate(LocalDate.of(getPostPollMonthlyReportDTO.getEndDate().getYear(), getPostPollMonthlyReportDTO.getEndDate().getMonth(), 1));
        return getMonthlyPostPollReportUseCase.getMonthlyPostPollReport(getPostPollMonthlyReportDTO.getStartDate(), getPostPollMonthlyReportDTO.getEndDate(), getPostPollMonthlyReportDTO.getSpecialistUUID());
    }
}
