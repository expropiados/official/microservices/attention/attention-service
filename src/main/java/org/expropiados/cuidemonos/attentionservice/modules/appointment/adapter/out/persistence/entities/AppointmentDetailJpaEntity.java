package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "APPOINTMENT_RESULT")
public class AppointmentDetailJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "appointment_result_id")
    private Long idResultAppointment;

    @Column(name = "appointment_id")
    private Long idAppointment;

    @Column(name = "annotations", length = 1000)
    private String annotations;

    @Column(name = "results", length = 1000)
    private String results;

    @OneToOne
    @JoinColumn(name = "appointment_id", insertable = false, updatable = false)
    private AppointmentJpaEntity appointmentJpaEntity;

}
