package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentDetailMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories.SpringJpaAppointmentDetailRepository;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetDetailAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAnnotationsPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveAnnotationsPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveDetailAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveResultsPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.AppointmentDetail;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;

@PersistenceAdapter
@RequiredArgsConstructor
public class AppointmentDetailPersistenceAdapter implements GetDetailAppointmentPort, SaveDetailAppointmentPort, SaveResultsPort,
        SaveAnnotationsPort, GetAnnotationsPort {
    private final SpringJpaAppointmentDetailRepository detailRepository;
    private final AppointmentDetailMapper appointmentDetailMapper;
    @Override
    public DetailAppointment getDetailAppointment(Long idAppointment) {

       var detailJpa = detailRepository.findAllByAppointmentJpaEntityIdAppointment(idAppointment);
       return appointmentDetailMapper.toAppointmentDetail(detailJpa);

    }

    @Override
    public void saveDetailAppointment(DetailAppointment detailAppointment) {
        var detailAppointmentJpa = appointmentDetailMapper.toAppointmentDetailEntity(detailAppointment);
        detailRepository.save(detailAppointmentJpa);
    }

    @Override
    public DetailAppointment saveResults(Long appointmentDetail, String results) {

        var detailJpa = detailRepository.findByIdResultAppointment(appointmentDetail);
        detailJpa.setResults(results);
        var detailJpaUpdated = detailRepository.save(detailJpa);
        return appointmentDetailMapper.toAppointmentDetail(detailJpaUpdated);
    }

    @Override
    public DetailAppointment saveAnnotations(Long idAppointment, String annotations) {
        var detailRow = detailRepository.findAllByAppointmentJpaEntityIdAppointment(idAppointment);
        detailRow.setAnnotations(annotations);
        var detailUpdated = detailRepository.save(detailRow);
        return appointmentDetailMapper.toAppointmentDetail(detailUpdated);
    }

    @Override
    public String getAnnotations(Long idAppointment) {
        var detailRow = detailRepository.findAllByAppointmentJpaEntityIdAppointment(idAppointment);
        return detailRow.getAnnotations();
    }


}
