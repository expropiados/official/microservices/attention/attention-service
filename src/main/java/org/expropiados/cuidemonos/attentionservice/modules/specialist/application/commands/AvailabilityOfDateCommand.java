package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.commands;

import lombok.Builder;
import lombok.Data;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.Specialist;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class AvailabilityOfDateCommand {
    private LocalDate date;
    private List<Specialist> specialists;
}
