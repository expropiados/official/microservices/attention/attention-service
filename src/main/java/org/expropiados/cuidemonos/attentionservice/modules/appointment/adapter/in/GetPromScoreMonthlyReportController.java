package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in.GetPromScoreMonthlyReportUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetPromScoreMonthlyReport;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/report/post-poll")
public class GetPromScoreMonthlyReportController {
    private final GetPromScoreMonthlyReportUseCase getPromScoreMonthlyReportUseCase;

    @GetMapping("/score")
    public GetPromScoreMonthlyReport getPromScoreMonthlyReport(GetPromScoreMonthlyReportDTO getPromScoreMonthlyReportDTO){
        getPromScoreMonthlyReportDTO.setStartDate(LocalDate.of(getPromScoreMonthlyReportDTO.getStartDate().getYear(), getPromScoreMonthlyReportDTO.getStartDate().getMonth(), 1));
        getPromScoreMonthlyReportDTO.setEndDate(LocalDate.of(getPromScoreMonthlyReportDTO.getEndDate().getYear(), getPromScoreMonthlyReportDTO.getEndDate().getMonth(), 1));
        return getPromScoreMonthlyReportUseCase.getPromScoreMonthlyReport(getPromScoreMonthlyReportDTO.getStartDate(), getPromScoreMonthlyReportDTO.getEndDate());
    }
}
