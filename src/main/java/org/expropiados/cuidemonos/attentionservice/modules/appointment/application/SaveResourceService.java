package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveLinkResourceDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveResourceUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveResourcePort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;
import org.expropiados.cuidemonos.attentionservice.thirdparty.StorageAdapter;
import org.springframework.web.multipart.MultipartFile;

@UseCase
@RequiredArgsConstructor
public class SaveResourceService implements SaveResourceUseCase {

    private final SaveResourcePort saveResourcePort;
    private final StorageAdapter storageAdapter;

    @Override
    public Resource saveLinkResource(Long idAppointmentDetail, SaveLinkResourceDTO saveLinkResourceDTO) {
        var resource = Resource.builder()
                .appointmentDetailId(idAppointmentDetail).name(saveLinkResourceDTO.getName())
                .url(saveLinkResourceDTO.getUrl()).owner(1).type(0).build();
        return saveResourcePort.saveResource(resource);
    }

    @Override
    public Resource saveDocumentResource(Long idAppointmentDetail, MultipartFile multipartFile, Integer role) {
        var nameDocument = multipartFile.getOriginalFilename();
        var documentUrl = storageAdapter.saveFile(multipartFile);
        if(role == 0) {
            var resource = Resource.builder()
                    .appointmentDetailId(idAppointmentDetail).name(nameDocument)
                    .url(documentUrl).owner(0).type(1).build();
            return saveResourcePort.saveResource(resource);
        }
        var resource = Resource.builder()
                .appointmentDetailId(idAppointmentDetail).name(nameDocument)
                .url(documentUrl).owner(1).type(1).build();
        return saveResourcePort.saveResource(resource);
    }

}
