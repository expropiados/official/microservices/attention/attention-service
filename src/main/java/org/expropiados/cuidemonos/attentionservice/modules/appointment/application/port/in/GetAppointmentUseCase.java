package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface GetAppointmentUseCase {
    List<Appointment> getAppointmentForSpecialist(UUID specialistUuid, LocalDate dateBegin, LocalDate dateEnd);
}
