package org.expropiados.cuidemonos.attentionservice.modules.videocall.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisterVideoCallUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisteredVideoCall;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.RegisterVideoCallPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.util.RandomString;

@UseCase
@RequiredArgsConstructor
public class RegisterVideoCallService implements RegisterVideoCallUseCase {

    public final RegisterVideoCallPort registerVideoCallPort;
    public final GetAppointmentPort getAppointmentPort;

    @Override
    public String registerVideoCall(Long appointmentId) {
        var registeredVideoCall = new RegisteredVideoCall(
                new RandomString().nextString(),
                new RandomString().randomIntegerValue(),
                new RandomString().randomIntegerValue(),
                appointmentId);
        var appointment = getAppointmentPort.getAppointment(appointmentId);
        if (appointment.isEmpty()) return null;
        return registerVideoCallPort.registerVideoCall(registeredVideoCall, appointment.get());
    }

}
