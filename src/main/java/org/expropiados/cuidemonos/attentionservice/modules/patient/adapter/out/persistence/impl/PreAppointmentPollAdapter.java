package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.mappers.PreAppointmentPollMapper;
import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.repositories.SpringJpaPreAppointmentPollRepository;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.GetPrePollPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.RegisterPrePollPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class PreAppointmentPollAdapter implements RegisterPrePollPort, GetPrePollPort {

    private final SpringJpaPreAppointmentPollRepository springJpaPreAppointmentPollRepository;
    private final PreAppointmentPollMapper preAppointmentPollMapper;

    @Override
    public void registerPrePool(PreAppointmentPoll poll) {
        var prePoll = preAppointmentPollMapper.toPreAppointmentPollEntity(poll);
        springJpaPreAppointmentPollRepository.save(prePoll);
    }

    @Override
    public List<PreAppointmentPoll> getPrePollBetweenDates(LocalDate startDate, LocalDate endDate) {
        var entities = springJpaPreAppointmentPollRepository.
                findAllByAppointmentJpaEntity_StartTimeAfterAndAppointmentJpaEntity_EndTimeBefore
                        (startDate.atStartOfDay(), endDate.atStartOfDay());
        return preAppointmentPollMapper.toPreAppointmentPollList(entities);
    }

    @Override
    public Optional<PreAppointmentPoll> getPrePoolByIdAppointment(Long idAppointment) {
        var row = springJpaPreAppointmentPollRepository.findByIdAppointment(idAppointment);
        return row.map(preAppointmentPollMapper::toPreAppointmentPoll);
    }

    @Override
    public List<PreAppointmentPoll> getPrePollBetweenDatesForClientCompany(LocalDate startDate, LocalDate endDate, Long clientCompanyId) {
        var entities = springJpaPreAppointmentPollRepository.
                searchPrePollBetweenDatesAndForClientCompany
                        (startDate.atStartOfDay(), endDate.atStartOfDay(), clientCompanyId);
        return preAppointmentPollMapper.toPreAppointmentPollList(entities);
    }

    @Override
    public PreAppointmentPoll getPrePollByAppointment(Long appointmentId) {
        var entities = springJpaPreAppointmentPollRepository.findByAppointmentJpaEntityIdAppointment(appointmentId);
        return preAppointmentPollMapper.toPreAppointmentPoll(entities);
    }
}
