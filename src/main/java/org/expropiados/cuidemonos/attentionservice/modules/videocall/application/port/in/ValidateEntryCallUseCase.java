package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;

public interface ValidateEntryCallUseCase {
    VideoCall validateEntryCall(EnterVideoCallInput enterVideoCallInput);
}
