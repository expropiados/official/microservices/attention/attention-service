package org.expropiados.cuidemonos.attentionservice.modules.specialist.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class Availability {
    private Long id;
    private UUID specialistUuid;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Integer state;
    private Appointment appointment;
}
