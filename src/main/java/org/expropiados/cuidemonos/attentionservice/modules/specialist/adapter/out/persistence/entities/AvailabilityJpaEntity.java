package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "AVAILABILITY")
public class AvailabilityJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "availability_id")
    private Long idAvailability;

    @Column(name = "specialist_uuid")
    private UUID specialistUuid;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    private LocalDateTime endTime;

    @Column(name = "state")
    private Integer state;

}
