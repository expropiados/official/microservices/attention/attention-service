package org.expropiados.cuidemonos.attentionservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetPatientsOfSpecialistUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.ListPatientsPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

import java.util.List;
import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetPatientsBySpecialistService implements GetPatientsOfSpecialistUseCase {

    private final ListPatientsPort listPatientsPort;

    @Override
    public List<User> getPatientsOfSpecialist(UUID specialistUuid, String search) {
        return listPatientsPort.listPatientsByName(specialistUuid, search);
    }

}
