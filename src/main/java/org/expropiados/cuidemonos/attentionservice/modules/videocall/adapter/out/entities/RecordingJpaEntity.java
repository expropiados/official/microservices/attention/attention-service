package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "RECORDING")
public class RecordingJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recording_id")
    private Long id;

    @Column(name = "videocall_id")
    private Long videoCallId;

    @Column(name = "recording_num")
    private Integer recordingNum;

    @Column(name = "folder_name")
    private String folderName;

    @Column(name = "resource_id", length = 750)
    private String resourceId;

    @Column(name = "sid")
    private String sid;

    @Column(name = "recording_type")
    private Integer recordingType;

    @ManyToOne
    @JoinColumn(name = "videocall_id", referencedColumnName = "videocall_id", insertable = false, updatable = false)
    private VideoCallJpaEntity videoCall;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;
}
