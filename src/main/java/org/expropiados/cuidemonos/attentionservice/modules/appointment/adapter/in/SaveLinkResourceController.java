package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveLinkResourceDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveResourceUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "appointments")
public class SaveLinkResourceController {

    private final JwtHelper jwtHelper;
    private final SaveResourceUseCase saveResourceUseCase;

    @PostMapping("/appointment-detail/{idAppointmentDetail}/resource/link")
    @ApiOperation(value = "Save link resource for appointment")
    public ResponseEntity<Resource> saveLinkResource(@PathVariable Long idAppointmentDetail ,
                                                     @RequestBody SaveLinkResourceDTO saveLinkResourceDTO,
                                                     @RequestHeader("Authorization") String token,
                                                     @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if(userRole!= UserRole.SPECIALIST){
            throw new NotAuthorizedForRoleException(userRole);
        }

        var resource = saveResourceUseCase.saveLinkResource(idAppointmentDetail, saveLinkResourceDTO);
        return ResponseEntity.ok(resource);

    }

}
