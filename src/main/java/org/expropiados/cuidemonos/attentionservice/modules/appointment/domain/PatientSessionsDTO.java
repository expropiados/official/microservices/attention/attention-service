package org.expropiados.cuidemonos.attentionservice.modules.appointment.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Optional;

import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.AppointmentDTO;
@Data
@Builder
@AllArgsConstructor
public class PatientSessionsDTO {
    private Optional<User> user;
    private List<AppointmentDTO> AppointmentDTO;

    public PatientSessionsDTO() {
    }
}
