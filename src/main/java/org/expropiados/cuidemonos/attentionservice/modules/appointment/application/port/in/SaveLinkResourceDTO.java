package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import lombok.Data;

@Data
public class SaveLinkResourceDTO {
    private String name;
    private String url;
}
