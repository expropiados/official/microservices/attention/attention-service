package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.PostPollJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(componentModel="spring")
@Component
public interface PostPollMapper {
    @Mapping(target = "idPostPoll", source = "idPostPoll")
    @Mapping(target = "idAppointment", source = "idAppointment")
    @Mapping(target = "callRating", source = "videoCallQuality")
    @Mapping(target = "selectedAudioIssues", ignore = true)
    @Mapping(target = "selectedVideoIssues", ignore = true)
    @Mapping(target = "callComments", source = "videoCallComments")
    @Mapping(target = "doctorRating", source = "specialistQuality")
    @Mapping(target = "selectedDoctorIssues", ignore = true)
    @Mapping(target = "doctorComments", source = "specialistComments")
    PostPoll toPostPoll(PostPollJpaEntity postPollJpaEntity);
    List<PostPoll> toPostPollList(List<PostPollJpaEntity> postPollJpaEntities);

    @InheritInverseConfiguration
    @Mapping(target = "idPostPoll", ignore = true)
    PostPollJpaEntity toPostPollEntity(PostPoll postPoll);

}
