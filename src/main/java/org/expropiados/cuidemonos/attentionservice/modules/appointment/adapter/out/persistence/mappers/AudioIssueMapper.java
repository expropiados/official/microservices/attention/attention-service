package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.AudioIssueJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(componentModel="spring")
@Component
public interface AudioIssueMapper {

    @Mapping(source = "idPostPoll", target = "idPostPoll")
    @Mapping(source = "selectedAudioIssues", target = "nameIssue")
    AudioIssueJpaEntity toAudioIssueJpaEntity(PostPoll postPoll);
    List<AudioIssueJpaEntity> toAudioIssueJpaEntityList(List<PostPoll> postPolls);

}
