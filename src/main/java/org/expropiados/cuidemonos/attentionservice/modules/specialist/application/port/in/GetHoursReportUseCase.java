package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in;

import java.time.LocalDate;
import java.util.UUID;

public interface GetHoursReportUseCase {
    GetHoursReport getHoursReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID);
}
