package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.entities.PreAppointmentPollJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface SpringJpaPreAppointmentPollRepository extends JpaRepository<PreAppointmentPollJpaEntity, Long> {
    List<PreAppointmentPollJpaEntity> findAllByAppointmentJpaEntity_StartTimeAfterAndAppointmentJpaEntity_EndTimeBefore(LocalDateTime startTime, LocalDateTime endTime);
    Optional<PreAppointmentPollJpaEntity> findByIdAppointment(Long idAppointment);

    @Query("SELECT DISTINCT PP FROM PreAppointmentPollJpaEntity PP INNER JOIN AppointmentJpaEntity A ON PP.idAppointment = A.idAppointment INNER JOIN UserJpaEntity U ON U.uuidUser = A.patientUUID INNER JOIN PatientJpaEntity P ON P.userId = U.id AND P.clientCompanyId = :clientCompanyId WHERE A.startTime >= :startTime AND A.endTime < :endTime")
    List<PreAppointmentPollJpaEntity> searchPrePollBetweenDatesAndForClientCompany(LocalDateTime startTime, LocalDateTime endTime, Long clientCompanyId);
    PreAppointmentPollJpaEntity findByAppointmentJpaEntityIdAppointment(Long appointmentId);
}
