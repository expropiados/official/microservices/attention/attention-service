package org.expropiados.cuidemonos.attentionservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonMonthly;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonMonthlyReport;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonMonthlyReportUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.GetMedicalReasonPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.GetPrePollPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MedicalReason;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MonthlyFormatsMap;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetMedicalReasonMonthlyReportService implements GetMedicalReasonMonthlyReportUseCase {

    private final GetPrePollPort getPrePollPort;
    private final GetMedicalReasonPort getMedicalReasonPort;
    private final HashMap<Integer, String> monthFormats = MonthlyFormatsMap.getMonthlyFormats();

    @Override
    public GetMedicalReasonMonthlyReport getMedicalReasonMonthlyReport(LocalDate startDate, LocalDate endDate, Long clientCompanyId) {

        var cantMonths = (endDate.getMonthValue()+12*(endDate.getYear()-startDate.getYear()))-startDate.getMonthValue()+1;
        var reportMedicalReason = fillList(cantMonths);
        var formatMonths = new ArrayList<String>(cantMonths);
        for (var i = 0; i < cantMonths; i++){
            var monthlyMap = fillMap();
            var iniAuxDate = startDate.plusMonths(i);
            var endAuxDate = startDate.plusMonths(i+1);
            List<PreAppointmentPoll> list;
            if (clientCompanyId == -1L)
                list = getPrePollPort.getPrePollBetweenDates(iniAuxDate, endAuxDate);
            else
                list = getPrePollPort.getPrePollBetweenDatesForClientCompany(iniAuxDate, endAuxDate, clientCompanyId);
            updateMonthlyMap(monthlyMap, list);
            updateMonthlyReport(monthlyMap, reportMedicalReason, i);
            formatMonths.add(getMonthFormatted(startDate, i));
        }
        return GetMedicalReasonMonthlyReport.builder()
                .seriesLine(reportMedicalReason).categories(formatMonths).build();
    }

    private HashMap<String, Integer> fillMap(){
        var medicalReasons = getMedicalReasonPort.getAllMedicalReasons();
        var aux = new HashMap<String, Integer>();
        for (MedicalReason medicalReason : medicalReasons){
            aux.put(medicalReason.getName(), 0);
        }
        return aux;
    }

    private List<GetMedicalReasonMonthly> fillList(int size){
        var medicalReasons = getMedicalReasonPort.getAllMedicalReasons();
        List<GetMedicalReasonMonthly> reportMedicalReason = new ArrayList<>();
        for (MedicalReason medicalReason : medicalReasons){
            var monthlyReport = new GetMedicalReasonMonthly();
            monthlyReport.setName(medicalReason.getName());
            monthlyReport.setData(new int[size]);
            reportMedicalReason.add(monthlyReport);
        }
        return reportMedicalReason;
    }

    private void updateMonthlyMap(HashMap<String, Integer> map, List<PreAppointmentPoll> list){
        for (PreAppointmentPoll preAppointmentPoll: list){
            var medicalReasonName = preAppointmentPoll.getMedicalReason().getName();
            if (map.containsKey(medicalReasonName)){
                map.put(medicalReasonName, map.get(medicalReasonName)+1);
            }
        }
    }

    private void updateMonthlyReport(HashMap<String, Integer> map, List<GetMedicalReasonMonthly> report, int month){
        for (GetMedicalReasonMonthly monthlyReport: report){
            monthlyReport.getData()[month] = map.get(monthlyReport.getName());
        }
    }

    private String getMonthFormatted(LocalDate startDate, int offset){
        var actualDate = startDate.plusMonths(offset);
        var formatYear = ((Integer)actualDate.getYear()).toString();
        return monthFormats.get(actualDate.getMonthValue())+ "-"+formatYear.substring(2);
    }
}
