package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;

public interface SaveDetailAppointmentPort {
    void saveDetailAppointment(DetailAppointment detailAppointment);
}
