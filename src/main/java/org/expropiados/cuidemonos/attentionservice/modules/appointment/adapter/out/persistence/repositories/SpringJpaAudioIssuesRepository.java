package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.AudioIssueJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpringJpaAudioIssuesRepository extends JpaRepository<AudioIssueJpaEntity, Long> {
}
