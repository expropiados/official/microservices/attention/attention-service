package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.email;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.security.SendGridConfig;
import org.expropiados.cuidemonos.attentionservice.hexagonal.EmailAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SendEmailForAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.expropiados.cuidemonos.attentionservice.thirdparty.SendGridExternalService;

import java.util.Map;

@EmailAdapter
@RequiredArgsConstructor
public class AppointmentSendGridEmailAdapter implements SendEmailForAppointmentPort {

    private final SendGridExternalService sendGridExternalService;
    private final SendGridConfig sendGridConfig;

    // example of using SendGrid External Service with its config (using env variables)
    // replace this method with @Override port method to implement
    @Override
    public void sendEmailPatientForAppointment(User patient, User specialist, String email, Appointment appointment) {
        var appointmentRegisteredPatientTemplateId = sendGridConfig
                .getTemplates()
                .getAppointmentRegisteredPatientId();

        var initAppointmentDate = appointment.getStartTime().toLocalDate().toString();
        var initAppointmentTime = appointment.getStartTime().toLocalTime().toString();
        var endAppointmentDate = appointment.getEndTime().toLocalDate().toString();
        var endAppointmentTime = appointment.getEndTime().toLocalTime().toString();

        var data = Map.of(
                "patient_name", patient.getFullName(),
                "specialist_name", specialist.getFullName(),
                "init_appointment_date", initAppointmentDate,
                "init_appointment_time", initAppointmentTime,
                "end_appointment_date", endAppointmentDate,
                "end_appointment_time", endAppointmentTime);

        sendGridExternalService.sendEmail(email, data, appointmentRegisteredPatientTemplateId);
    }

    // example of using SendGrid External Service with its config (using env variables)
    // replace this method with @Override port method to implement
    @Override
    public void sendEmailSpecialistForAppointment(User patient, User specialist, String email, Appointment appointment) {
        var appointmentRegisteredSpecialistTemplateId = sendGridConfig
                .getTemplates()
                .getAppointmentRegisteredSpecialistId();

        var initAppointmentDate = appointment.getStartTime().toLocalDate().toString();
        var initAppointmentTime = appointment.getStartTime().toLocalTime().toString();
        var endAppointmentDate = appointment.getEndTime().toLocalDate().toString();
        var endAppointmentTime = appointment.getEndTime().toLocalTime().toString();

        var data = Map.of(
                "patient_name", patient.getFullName(),
                "specialist_name", specialist.getFullName(),
                "init_appointment_date", initAppointmentDate,
                "init_appointment_time", initAppointmentTime,
                "end_appointment_date", endAppointmentDate,
                "end_appointment_time", endAppointmentTime);

        sendGridExternalService.sendEmail(email, data, appointmentRegisteredSpecialistTemplateId);
    }
}
