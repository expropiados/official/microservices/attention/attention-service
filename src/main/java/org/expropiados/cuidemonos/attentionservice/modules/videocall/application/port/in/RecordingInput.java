package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.RecordingType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordingInput {
    Long videoCallId;
    RecordingType recordingType;
}
