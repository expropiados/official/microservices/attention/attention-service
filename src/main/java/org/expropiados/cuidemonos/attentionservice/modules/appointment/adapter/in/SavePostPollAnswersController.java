package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.mappers.SavePostPollAnswersDTOMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.PostPollDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SavePostPollAnswersUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/appointments")
public class SavePostPollAnswersController {

    private final JwtHelper jwtHelper;
    private final SavePostPollAnswersUseCase savePostPollAnswersUseCase;
    private final SavePostPollAnswersDTOMapper savePostPollAnswersDTOMapper;

    @PostMapping("/{idAppointment}/post-poll")
    @ApiOperation("Register results of session")
    public ResponseEntity<PostPoll> savePostPollAnswers(@PathVariable Long idAppointment,
                                                        @RequestBody PostPollDTO postPollDTO,
                                                        @RequestHeader("Authorization") String token,
                                                        @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if(userRole!= UserRole.PATIENT){
            throw new NotAuthorizedForRoleException(userRole);
        }

        var postPoll = savePostPollAnswersDTOMapper.toPostPollAnswers(postPollDTO);
        var postPollSaved = savePostPollAnswersUseCase.savePostPollAnswers(idAppointment, postPoll, postPollDTO);

        return ResponseEntity.ok(postPollSaved);

    }




}
