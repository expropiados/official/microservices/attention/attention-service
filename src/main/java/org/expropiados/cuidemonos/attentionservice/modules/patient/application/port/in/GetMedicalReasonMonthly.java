package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in;

import lombok.Data;

@Data
public class GetMedicalReasonMonthly {
    private String name;
    private int[] data;
}
