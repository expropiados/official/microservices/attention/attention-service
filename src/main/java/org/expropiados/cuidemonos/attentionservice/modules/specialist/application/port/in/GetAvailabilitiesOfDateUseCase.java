package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.commands.AvailabilityOfDateCommand;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface GetAvailabilitiesOfDateUseCase {
    List<AvailabilityOfDateCommand> getSpecialistsAvailabilitiesOfRangeDate(LocalDate dateBegin, LocalDate dateEnd);
    List<Availability> getAvailabilitiesOfRangeDate(LocalDate dateBegin, LocalDate dateEnd, UUID specialistUuid);
}
