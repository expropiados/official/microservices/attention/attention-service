package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;


public interface GetDetailAppointmentPort {
    DetailAppointment getDetailAppointment(Long idAppointment);
}
