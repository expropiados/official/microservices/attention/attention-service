package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.LocalDateTimePeruZone;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in.GetDetailAppointmentUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetDetailAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetResourcePort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.utils.Constants;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.ResourceOwner;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.GetPrePollPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetPatientByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetSpecialistByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.GetVideoCallPort;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@UseCase
@RequiredArgsConstructor
public class GetDetailAppointmentService implements GetDetailAppointmentUseCase {

    public final GetDetailAppointmentPort getDetailAppointmentPort;
    public final GetResourcePort getResourcePort;
    private final GetPatientByUserUuidPort getPatientByUserUuidPort;
    private final GetSpecialistByUserUuidPort getSpecialistByUserUuidPort;
    private final GetVideoCallPort getVideoCallPort;
    private final GetPrePollPort getPrePollPort;

    @Override
    public DetailAppointment getDetailAppointment(Long idAppointment) {
        var detailAppointment = getDetailAppointmentPort.getDetailAppointment(idAppointment);

        var videoCall = getVideoCallPort.getVideoCallByIdAppointment(detailAppointment.getAppointment().getId());
        videoCall.ifPresent(call -> detailAppointment.getAppointment().setVideoCallUuid(call.getVideoCallUuid()));

        var prePool = getPrePollPort.getPrePoolByIdAppointment(detailAppointment.getAppointment().getId());
        prePool.ifPresent(call -> detailAppointment.getAppointment().setPreAppointmentPoll(call));

        var isCancellable = validateIsCancelable(detailAppointment.getAppointment());
        detailAppointment.getAppointment().setIsCancellable(isCancellable);

        getPatientByUserUuidPort.getPatientByUserUuid(detailAppointment.getAppointment().getPatientUuid())
                .ifPresent(user -> detailAppointment.getAppointment().setPatient(user));

        getSpecialistByUserUuidPort.getSpecialistByUserUuid(detailAppointment.getAppointment().getSpecialistUuid())
                .ifPresent(user -> detailAppointment.getAppointment().setSpecialist(user));

        var resources = getResourcePort.getResources(detailAppointment.getAppointmentDetailId());
        var patientResources = getResourcesOfOwner(resources, 0);
        var specialistResources = getResourcesOfOwner(resources, 1);
        detailAppointment.setResourcesPatient(patientResources);
        detailAppointment.setResourcesSpecialist(specialistResources);
        return detailAppointment;
    }

    public Boolean validateIsCancelable(Appointment appointment){

        var startTime = appointment.getStartTime();
        var now = LocalDateTimePeruZone.now();
        return now.plusHours(Constants.TIME_MARGIN_CANCEL_APPOINTMENT).isBefore(startTime);
    }

    public ResourceOwner getResourcesOfOwner(List<Resource> resources, Integer owner){
        ArrayList<Resource> resourcesOfOwner = new ArrayList<>();
        resources.forEach(resource -> {
            if(resource.getOwner().equals(owner)) resourcesOfOwner.add(resource);
        });

        var linkResources = getResourcesOfType(resourcesOfOwner, 0);
        var documentResources = getResourcesOfType(resourcesOfOwner, 1);

        return ResourceOwner.builder().linkResources(linkResources).documentResources(documentResources).build();
    }

    public List<Resource> getResourcesOfType(List<Resource> resourcesOfOwner, Integer type){
        ArrayList<Resource> resourcesOfType = new ArrayList<>();
        resourcesOfOwner.forEach(resource -> {
            if(resource.getType().equals(type)) resourcesOfType.add(resource);
        });

        return resourcesOfType;
    }

}
