package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.errors;

import java.time.LocalDate;
import java.util.UUID;

public class AppointmentForSpecialistNotFoundError extends RuntimeException{
    public AppointmentForSpecialistNotFoundError(UUID specialistUuid, LocalDate date){
        super(String.format("No se ha encontrado niguna cita del especialista con uuid %s en la fecha %s",
                specialistUuid.toString(),date.toString()));
    }
}
