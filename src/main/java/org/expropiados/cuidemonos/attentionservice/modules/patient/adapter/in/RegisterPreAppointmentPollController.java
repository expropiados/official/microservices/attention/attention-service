package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.RegisterPrePollUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/appointments")
public class RegisterPreAppointmentPollController {

    private final JwtHelper jwtHelper;
    private final RegisterPrePollUseCase registerPrePollUseCase;
    private final PreApoointmentPollMapper preApoointmentPollMapper;

    @PostMapping("/{idAppointment}/preAppointmentPool")
    @ApiOperation("Register pre appointment pool from patient")
    public ResponseEntity<HttpStatus> registerPrePool(@PathVariable Long idAppointment,
                                                      @RequestBody PreAppointmentPollDTO preAppointmentPoolDTO,
                                                      @RequestHeader("Authorization") String token,
                                                      @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var prePool = preApoointmentPollMapper.toPrePoll(preAppointmentPoolDTO);
        registerPrePollUseCase.registerPrePoll(idAppointment, prePool);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
