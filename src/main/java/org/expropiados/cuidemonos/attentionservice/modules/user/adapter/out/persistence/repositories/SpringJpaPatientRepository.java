package org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.entities.PatientJpaEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SpringJpaPatientRepository extends JpaRepository<PatientJpaEntity, Long> {
    List<PatientJpaEntity> findAllByUser_UuidUserIn(List<UUID> uuidList);
    Optional<PatientJpaEntity> findByUser_UuidUser(UUID userUuid);

    List<PatientJpaEntity> findDistinctByUser_UuidUserIn(List<UUID> uuidList);
    @Query("SELECT DISTINCT P FROM PatientJpaEntity P INNER JOIN AppointmentJpaEntity A ON P.user.uuidUser = A.patientUUID WHERE  A.specialistUUID = :specialistUuid AND A.state <> :state AND (UPPER(P.user.document) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(P.user.name) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(P.user.fatherLastname) LIKE CONCAT('%', UPPER(:search), '%') OR UPPER(P.user.motherLastname) LIKE CONCAT('%', UPPER(:search), '%')) ORDER BY P.user.id")
    Page<PatientJpaEntity> searchPatientsBySpecialist(Pageable pageable, @Param("search") String search, @Param("specialistUuid") UUID specialistUuid, @Param("state") Integer state);

}
