package org.expropiados.cuidemonos.attentionservice.modules.patient.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class MedicalReason {
    private Long idMedicalReason;
    private Boolean deleted;
    private String description;
    private String name;

}
