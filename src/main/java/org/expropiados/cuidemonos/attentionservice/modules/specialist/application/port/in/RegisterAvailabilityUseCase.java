package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface RegisterAvailabilityUseCase {
    void registerAvailability(UUID specialistUuid, List<Availability> availabilities,
                              int replicate, LocalDate dateBegin, LocalDate dateEnd);
}
