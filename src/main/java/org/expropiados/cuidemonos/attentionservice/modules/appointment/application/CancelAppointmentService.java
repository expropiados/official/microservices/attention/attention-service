package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.CancelAppointmentUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.CancelAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

@UseCase
@RequiredArgsConstructor
public class CancelAppointmentService implements CancelAppointmentUseCase {

    private final CancelAppointmentPort cancelAppointmentPort;

    @Override
    public Appointment cancelAppointment(Long idAppointment) {

        return cancelAppointmentPort.cancelAppointment(idAppointment);

    }

}
