package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in;

public interface GetMedicalReasonMonthlyFreqReportUseCase {
    GetMedicalReasonMonthlyFreqReport getMedicalReasonMonthlyFreqReport(GetMedicalReasonMonthlyReport getMedicalReasonMonthly);
}
