package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.GetAnnotationsUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAnnotationsPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.GetVideoCallPort;

@UseCase
@RequiredArgsConstructor
public class GetAnnotationsService implements GetAnnotationsUseCase {

    private final GetVideoCallPort getVideoCallPort;
    private final GetAnnotationsPort getAnnotationsPort;


    @Override
    public String getAnnotations(Long idVideoCall) {

        var videoCall = getVideoCallPort.getVideoCall(idVideoCall);

        var idAppointment = videoCall.getAppointment().getId();

        return getAnnotationsPort.getAnnotations(idAppointment);

    }
}
