package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in;

import lombok.Data;

@Data
public class GetMonthlyGeneralReport {
    private String name;
    private double[] data;
}
