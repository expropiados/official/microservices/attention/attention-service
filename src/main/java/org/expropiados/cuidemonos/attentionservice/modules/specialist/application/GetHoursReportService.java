package org.expropiados.cuidemonos.attentionservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetHoursReport;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetHoursReportUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetMonthlyGeneralReport;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilitiesOfDatePort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.utils.Constants;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetHoursReportService implements GetHoursReportUseCase {

    public final GetAvailabilitiesOfDatePort getAvailabilitiesOfDatePort;

    @Override
    public GetHoursReport getHoursReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID) {
        var cantMonths = (endDate.getMonthValue()+12*(endDate.getYear()-startDate.getYear()))-startDate.getMonthValue();
        var monthlyReportAvailable = new GetMonthlyGeneralReport();
        var monthlyReportReserved = new GetMonthlyGeneralReport();

        monthlyReportReserved.setName("Horas útiles");
        monthlyReportReserved.setData(new double[cantMonths]);
        monthlyReportAvailable.setName("Horas disponibles");
        monthlyReportAvailable.setData(new double[cantMonths]);
        for (var i = 0; i < cantMonths; i++) {
            var iniAuxDate = startDate.plusMonths(i);
            var endAuxDate = startDate.plusMonths(i+1);
            var availabilitiesAvailable = getAvailabilitiesOfDatePort.getAvailabilitiesOfRangeDateAndState(iniAuxDate, endAuxDate, specialistUUID, Constants.STATE_AVAILABILITY_AVAILABLE);
            var availabilitiesReserved = getAvailabilitiesOfDatePort.getAvailabilitiesOfRangeDateAndState(iniAuxDate, endAuxDate, specialistUUID, Constants.STATE_AVAILABILITY_RESERVED);

            monthlyReportReserved.getData()[i] = fillHours(availabilitiesReserved);
            monthlyReportAvailable.getData()[i] = fillHours(availabilitiesAvailable);
        }

        var seriesLine = new ArrayList<GetMonthlyGeneralReport>(2);
        seriesLine.add(monthlyReportAvailable);
        seriesLine.add(monthlyReportReserved);

        return GetHoursReport.builder()
                .seriesLine(seriesLine).build();
    }

    private Double fillHours(List<Availability> availabilityList){
        var hours = 0.0;
        for (Availability av: availabilityList){
            var endSeconds = av.getEndTime().atZone(ZoneId.of("EST", ZoneId.SHORT_IDS)).toInstant().getEpochSecond();
            var startSeconds = av.getStartTime().atZone(ZoneId.of("EST", ZoneId.SHORT_IDS)).toInstant().getEpochSecond();
            hours+= (endSeconds-startSeconds)/(double) 3600;
        }
        return hours;
    }
}
