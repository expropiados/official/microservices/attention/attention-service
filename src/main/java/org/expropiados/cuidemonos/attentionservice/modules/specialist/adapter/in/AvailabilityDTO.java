package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AvailabilityDTO {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Integer state;
}
