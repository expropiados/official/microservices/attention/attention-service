package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import lombok.Value;

@Value
public class RecordingParametersInput {
    Long videoCallId;
    String resourceId;
    String sid;
    boolean firstTimeAgoraTokenRecorder;
    String folderName;
}
