package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.PostPollDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SavePostPollAnswersUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SavePostPollAnswersPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;

import java.util.ArrayList;

@UseCase
@RequiredArgsConstructor
public class SavePostPollAnswersService implements SavePostPollAnswersUseCase {

    private final SavePostPollAnswersPort savePostPollAnswersPort;

    @Override
    public PostPoll savePostPollAnswers(Long appointmentId, PostPoll postPoll, PostPollDTO postPollDTO) {

        ArrayList<PostPoll> audioPools = new ArrayList<>();
        ArrayList<PostPoll> videoPools = new ArrayList<>();
        ArrayList<PostPoll> specialistPools = new ArrayList<>();

        postPoll.setIdAppointment(appointmentId);
        var postPollSaved = savePostPollAnswersPort.saveResults(postPoll);

        postPollDTO.getSelectedAudioIssues().forEach(s -> {
            var audioPool = PostPoll.builder().idPostPoll(postPollSaved.getIdPostPoll()).selectedAudioIssues(s).build();
            audioPools.add(audioPool);
        });

        savePostPollAnswersPort.saveAudioIssues(audioPools);

        postPollDTO.getSelectedDoctorIssues().forEach(s -> {
            var specialistPool = PostPoll.builder().idPostPoll(postPollSaved.getIdPostPoll()).selectedDoctorIssues(s).build();
            specialistPools.add(specialistPool);
        });

        savePostPollAnswersPort.saveSpecialistIssues(specialistPools);

        postPollDTO.getSelectedVideoIssues().forEach(s -> {
            var videoPool = PostPoll.builder().idPostPoll(postPollSaved.getIdPostPoll()).selectedVideoIssues(s).build();
            videoPools.add(videoPool);
        });

        savePostPollAnswersPort.saveVideoIssues(videoPools);

        return postPollSaved;


    }

}
