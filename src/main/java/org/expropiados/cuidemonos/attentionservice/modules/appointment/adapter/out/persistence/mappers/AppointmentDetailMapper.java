package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.AppointmentDetailJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Mapper(componentModel="spring")
@Component
public interface AppointmentDetailMapper {
    @Mapping(target = "appointmentDetailId", source = "idResultAppointment")
    @Mapping(target = "annotations", source = "annotations")
    @Mapping(target = "results", source = "results")
    @Mapping(target = "resourcesPatient", ignore = true)
    @Mapping(target = "resourcesSpecialist", ignore = true)
    @Mapping(target = "appointment.id", source = "appointmentJpaEntity.idAppointment")
    @Mapping(target = "appointment.specialistUuid", source = "appointmentJpaEntity.specialistUUID")
    @Mapping(target = "appointment.patientUuid", source = "appointmentJpaEntity.patientUUID")
    @Mapping(target = "appointment.startTime", source = "appointmentJpaEntity.startTime")
    @Mapping(target = "appointment.endTime", source = "appointmentJpaEntity.endTime")
    @Mapping(target = "appointment.state", source = "appointmentJpaEntity.state")
    DetailAppointment toAppointmentDetail(AppointmentDetailJpaEntity appointmentDetailJpaEntity);

    @Mapping(source = "appointmentDetailId", target = "idResultAppointment")
    @Mapping(source = "appointment.id", target = "idAppointment")
    @Mapping(source = "annotations", target = "annotations")
    @Mapping(source = "results", target = "results")
    @Mapping(target = "appointmentJpaEntity", ignore = true)
    AppointmentDetailJpaEntity toAppointmentDetailEntity(DetailAppointment detailAppointment);

}
