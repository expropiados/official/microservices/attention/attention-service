package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveAnnotationsDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveAnnotationsUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/video_call")
public class SaveAnnotationsController {

    private final JwtHelper jwtHelper;
    private final SaveAnnotationsUseCase saveAnnotationsUseCase;

    @PostMapping("/{idVideoCall}/appointments/annotation")
    public ResponseEntity<HttpStatus> saveAnnotations(@PathVariable Long idVideoCall,
                                                      @RequestBody SaveAnnotationsDTO saveAnnotationsDTO,
                                                      @RequestHeader("Authorization") String token,
                                                      @RequestHeader("role") Integer role) {

        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if(userRole != UserRole.SPECIALIST){
            throw new NotAuthorizedForRoleException(userRole);
        }

        saveAnnotationsUseCase.saveAnnotations(idVideoCall, saveAnnotationsDTO.getAnnotations());

        return ResponseEntity.ok(HttpStatus.OK);
    }

}
