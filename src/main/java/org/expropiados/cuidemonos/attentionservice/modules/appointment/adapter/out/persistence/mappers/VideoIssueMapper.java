package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.SpecialistIssueJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.VideoIssuesJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(componentModel="spring")
@Component
public interface VideoIssueMapper {

    @Mapping(source = "idPostPoll", target = "idPostPoll")
    @Mapping(source = "selectedVideoIssues", target = "nameIssue")
    VideoIssuesJpaEntity toVideoIssueJpaEntity(PostPoll postPoll);
    List<VideoIssuesJpaEntity> toVideoIssueJpaEntityList(List<PostPoll> postPolls);

}
