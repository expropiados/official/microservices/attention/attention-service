package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetHoursReport;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetHoursReportUseCase;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/report")
public class GetHoursReportController {

    private final GetHoursReportUseCase getHoursReportUseCase;

    @GetMapping(value = "/availabilities/hours")
    public GetHoursReport getHoursReport(GetHoursReportDTO getHoursReportDTO){
        getHoursReportDTO.setStartDate(LocalDate.of(getHoursReportDTO.getStartDate().getYear(), getHoursReportDTO.getStartDate().getMonth(), 1));
        getHoursReportDTO.setEndDate(LocalDate.of(getHoursReportDTO.getEndDate().getYear(), getHoursReportDTO.getEndDate().getMonth(), 1));
        return getHoursReportUseCase.getHoursReport(getHoursReportDTO.getStartDate(), getHoursReportDTO.getEndDate(), getHoursReportDTO.getSpecialistUUID());
    }
}
