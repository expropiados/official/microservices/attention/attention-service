package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "AUDIO_ISSUE")
public class AudioIssueJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "audio_issue_id")
    private Long idAudioIssue;

    @Column(name = "poll_post_appointment_id")
    private Long idPostPoll;

    @Column(name = "name_issue", length = 100)
    private String nameIssue;

}
