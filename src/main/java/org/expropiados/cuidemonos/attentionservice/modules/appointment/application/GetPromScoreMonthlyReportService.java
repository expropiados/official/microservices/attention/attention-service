package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in.GetPromScoreMonthlyReportUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetMonthlyGeneralReport;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetPromScoreMonthlyReport;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetPostPollPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MonthlyFormatsMap;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetPromScoreMonthlyReportService implements GetPromScoreMonthlyReportUseCase {

    public final GetPostPollPort getPostPollPort;
    private final HashMap<Integer, String> monthFormats = MonthlyFormatsMap.getMonthlyFormats();

    @Override
    public GetPromScoreMonthlyReport getPromScoreMonthlyReport(LocalDate startDate, LocalDate endDate) {
        var cantMonths = (endDate.getMonthValue()+12*(endDate.getYear()-startDate.getYear()))-startDate.getMonthValue();
        var monthlyGeneralReport = new GetMonthlyGeneralReport();
        monthlyGeneralReport.setName("Puntaje Promedio");
        monthlyGeneralReport.setData(new double[cantMonths]);

        var categories = new ArrayList<String>(cantMonths);

        for (var i = 0; i < cantMonths; i++){
            var iniAuxDate = startDate.plusMonths(i);
            var endAuxDate = startDate.plusMonths(i+1);
            var postPolls = getPostPollPort.getPostPollBetweenDates(iniAuxDate, endAuxDate);
            var rank = totalRanking(postPolls);
            if (postPolls.size() != 0) monthlyGeneralReport.getData()[i] = rank/(double) postPolls.size();
            else monthlyGeneralReport.getData()[i] = 0;
            categories.add(getMonthFormatted(startDate, i));
        }

        var seriesLine = new ArrayList<GetMonthlyGeneralReport>(1);
        seriesLine.add(monthlyGeneralReport);

        return GetPromScoreMonthlyReport.builder()
                .seriesLine(seriesLine)
                .categories(categories).build();
    }

    private Integer totalRanking(List<PostPoll> postPollList){
        var total = 0;
        for (PostPoll postPoll: postPollList){
            total+=postPoll.getDoctorRating();
        }
        return total;
    }

    private String getMonthFormatted(LocalDate startDate, int offset){
        var actualDate = startDate.plusMonths(offset);
        var formatYear = ((Integer)actualDate.getYear()).toString();
        return monthFormats.get(actualDate.getMonthValue())+ "-"+formatYear.substring(2);
    }
}
