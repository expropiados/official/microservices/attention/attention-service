package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PatientSessionsDTO;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
public interface PatientSessionsUseCase {

    PatientSessionsDTO patientSessions(UUID specialistId, UUID patientId);

}
