package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface GetPrePollPort {
    List<PreAppointmentPoll> getPrePollBetweenDates(LocalDate startDate, LocalDate endDate);
    Optional<PreAppointmentPoll> getPrePoolByIdAppointment(Long idAppointment);
    List<PreAppointmentPoll> getPrePollBetweenDatesForClientCompany(LocalDate startDate, LocalDate endDate, Long clientCompanyId);
    PreAppointmentPoll getPrePollByAppointment(Long appointmentId);
}
