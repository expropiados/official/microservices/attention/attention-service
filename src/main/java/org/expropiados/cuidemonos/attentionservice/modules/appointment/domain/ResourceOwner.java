package org.expropiados.cuidemonos.attentionservice.modules.appointment.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class ResourceOwner {
    private List<Resource> linkResources;
    private List<Resource> documentResources;
}
