package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class GetAppointmentDTO {
    private UUID patientUuid;
    private String patientName;
    private String patientFatherLastname;
    private String patientMotherLastname;
    private LocalDateTime appointmentStartTime;
    private LocalDateTime appointmentEndTime;
    private String profileImageUrl;
    private Long appointmentId;
}
