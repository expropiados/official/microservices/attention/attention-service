package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.mappers.VideoCallMapper;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.persistence.SpringJpaVideoCallRepository;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisteredVideoCall;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.GetVideoCallPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.RegisterVideoCallPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveIsFirstTimeAgoraPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveTokenPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.UUID;

@PersistenceAdapter
@RequiredArgsConstructor
public class VideoCallPersistenceAdapter implements RegisterVideoCallPort, SaveTokenPort, GetVideoCallPort, SaveIsFirstTimeAgoraPort {
    private final SpringJpaVideoCallRepository videoCallRepository;
    private final VideoCallMapper videoCallMapper;
    private final AppointmentMapper appointmentMapper;

    @Override
    public String registerVideoCall(RegisteredVideoCall registeredVideoCall, Appointment appointment) {
        var entityJpa = videoCallMapper.toVideoCallJpaEntity(registeredVideoCall);
        entityJpa.setAppointment(appointmentMapper.toAppointmentJpaEntity(appointment));
        entityJpa.setCreatedAt(LocalDateTime.now(ZoneOffset.UTC).minusHours(5));
        entityJpa.setFirstTimeAgoraTokenRecorder(true);
        entityJpa.setVideoCallUuid(UUID.randomUUID());
        var result = videoCallRepository.save(entityJpa);
        return result.getAgoraChannelName();
    }


    @Override
    public void saveToken(Long videoCallId, String token, int role) {
        var optionalJpaEntity = videoCallRepository.findById(videoCallId);
        if (optionalJpaEntity.isEmpty()) return;
        var jpaEntity = optionalJpaEntity.get();
        if (role == 1) jpaEntity.setSpecialistToken(token);
        else jpaEntity.setPatientToken(token);
        videoCallRepository.save(jpaEntity);
    }

    @Override
    public void saveAgoraTokenRecorder(Long videoCallId, String agoraTokenRecorder) {
        var optionalJpaEntity = videoCallRepository.findById(videoCallId);
        if (optionalJpaEntity.isEmpty()) return;
        var jpaEntity = optionalJpaEntity.get();
        jpaEntity.setAgoraTokenRecorder(agoraTokenRecorder);
        videoCallRepository.save(jpaEntity);
    }

    @Override
    public VideoCall getVideoCallByChannelName(String agoraName) {
        var entity = videoCallRepository.findByAgoraChannelNameEquals(agoraName);
        return videoCallMapper.toVideoCall(entity);
    }

    @Override
    public Optional<VideoCall> getVideoCallByIdAppointment(Long idAppointment) {
        var entity = videoCallRepository.findByAppointmentId(idAppointment);
        return entity.map(videoCallMapper::toVideoCall);
    }

    @Override
    public VideoCall getVideoCall(Long idVideoCall) {
        var row = videoCallRepository.findById(idVideoCall);
        if(row.isEmpty()) return null;
        return videoCallMapper.toVideoCall(row.get());
    }

    @Override
    public VideoCall saveFirstTokenBoolean(Long id, boolean isFirstTime) {
        var entity = videoCallRepository.findById(id);
        if (entity.isEmpty()) return null;
        entity.get().setFirstTimeAgoraTokenRecorder(isFirstTime);
        var result = videoCallRepository.save(entity.get());
        return videoCallMapper.toVideoCall(result);
    }
}
