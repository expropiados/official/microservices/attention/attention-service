package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.GetDetailAppointmentDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface GetDetailAppointmentDTOMapper {

    @Mapping(source = "appointment", target = "appointment")
    @Mapping(source = "appointmentDetailId", target = "idAppointmentDetail")
    @Mapping(source = "annotations", target = "annotations")
    @Mapping(source = "resourcesPatient", target = "patientResources")
    @Mapping(source = "resourcesSpecialist", target = "specialistResources")
    @Mapping(source = "appointment.preAppointmentPoll.appointmentId", target = "prePoolDTO.idAppointment")
    @Mapping(source = "appointment.preAppointmentPoll.medicalReason.name", target = "prePoolDTO.reason")
    @Mapping(source = "appointment.preAppointmentPoll.medicalReason.description", target = "prePoolDTO.descriptionReason")
    @Mapping(source = "appointment.preAppointmentPoll.additionalInfo", target = "prePoolDTO.additionalInfo")
    @Mapping(source = "appointment.preAppointmentPoll.anotherReason", target = "prePoolDTO.anotherReason")
    @Mapping(target = "appointment.preAppointmentPoll", ignore = true)
    GetDetailAppointmentDTO toGetDetailAppointmentDTO(DetailAppointment detailAppointment);

}
