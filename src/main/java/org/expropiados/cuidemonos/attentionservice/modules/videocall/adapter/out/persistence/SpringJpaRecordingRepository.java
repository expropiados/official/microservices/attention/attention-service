package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.persistence;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.entities.RecordingJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SpringJpaRecordingRepository extends JpaRepository<RecordingJpaEntity, Long> {
    Optional<RecordingJpaEntity> getFirstByVideoCallIdOrderByCreatedAtDesc(Long videoCallId);
    Optional<RecordingJpaEntity> getFirstByVideoCallIdAndRecordingTypeOrderByCreatedAtDesc(Long videoCallId, Integer recordingType);
    Optional<RecordingJpaEntity> getByVideoCallIdAndFolderName(Long videoCallId, String folderName);
}
