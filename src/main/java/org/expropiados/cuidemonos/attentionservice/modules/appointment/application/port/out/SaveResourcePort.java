package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;

public interface SaveResourcePort {
    Resource saveResource(Resource resource);
}
