package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RegisterAvailabilityMapper {
    @Mapping(source = "startTime", target = "startTime")
    @Mapping(source = "endTime", target = "endTime")
    @Mapping(source = "state", target = "state")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "specialistUuid", ignore = true)
    Availability toAvailability(AvailabilityDTO availabilityDTO);
    List<Availability> toAvailabilityList(List<AvailabilityDTO> availabilityDTOList);
}
