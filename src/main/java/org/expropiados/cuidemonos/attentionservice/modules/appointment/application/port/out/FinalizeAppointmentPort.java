package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import java.time.LocalDateTime;

public interface FinalizeAppointmentPort {
    void finalizeAppointment(LocalDateTime now);
}
