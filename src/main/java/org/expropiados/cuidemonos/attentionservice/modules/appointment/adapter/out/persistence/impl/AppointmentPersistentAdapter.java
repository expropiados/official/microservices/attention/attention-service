package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories.SpringJpaAppointmentRepository;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.utils.Constants;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.BookAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.CancelAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.FinalizeAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class AppointmentPersistentAdapter implements GetAppointmentPort, BookAppointmentPort, CancelAppointmentPort, FinalizeAppointmentPort {

    private final SpringJpaAppointmentRepository springJpaAppointmentRepository;
    private final AppointmentMapper appointmentMapper;

    @Override
    public Optional<List<Appointment>> getAppointmentForSpecialist(UUID specialistUuid, LocalDate dateBegin, LocalDate dateEnd) {
        var startTime = dateBegin.atStartOfDay();
        var endTime = dateEnd.plusDays(1L).atStartOfDay();
        var rows = springJpaAppointmentRepository
                .findBySpecialistUUIDEqualsAndStartTimeBetweenAndStateOrderByStartTime(specialistUuid, startTime, endTime, Constants.PENDENT_STATE_APPOINTMENT);
        return rows.map(appointmentMapper::toAppointmentList);
    }

    @Override
    public List<Appointment> getPendentAppointmentForPatient(UUID patientUuid, LocalDate dateBegin, LocalDate dateEnd) {
        var startTime = dateBegin.atStartOfDay();
        var endTime = dateEnd.plusDays(1L).atStartOfDay();
        var rows = springJpaAppointmentRepository.findAllByPatientUUIDAndStartTimeBetweenAndStateOrderByStartTime(patientUuid,
                startTime, endTime, Constants.PENDENT_STATE_APPOINTMENT);
        return  appointmentMapper.toAppointmentList(rows);
    }

    @Override
    public Optional<Appointment> getAppointment(Long appointmentId) {
        var entity = springJpaAppointmentRepository.findById(appointmentId);
        return entity.map(appointmentMapper::toAppointment);
    }

    @Override
    public Map<LocalDateTime, Appointment> getAppointmentOfAvailability(UUID specialistUuid) {
        var appointmentRows = springJpaAppointmentRepository.findAllBySpecialistUUIDEquals(specialistUuid);
        return appointmentMapper.toAppointmentList(appointmentRows)
                .stream()
                .collect(Collectors.toMap(Appointment::getStartTime, Function.identity()));
    }

    @Override
    public List<Appointment> getAppointmentsInDateRangeForClientCompany(LocalDate startDate, LocalDate endDate, Long clientCompanyId) {
        var appointmentRows = springJpaAppointmentRepository.searchAppointmentsByDateRangeAndClientCompany(startDate.atStartOfDay(), endDate.atStartOfDay(), clientCompanyId);
        return appointmentMapper.toAppointmentList(appointmentRows);
    }

    @Override
    public Appointment bookAppointment(Appointment appointment) {
        var rowAppointment = appointmentMapper.toAppointmentJpaEntity(appointment);
        var rowSaved = springJpaAppointmentRepository.save(rowAppointment);
        return appointmentMapper.toAppointment(rowSaved);
    }

    @Override
    public Appointment cancelAppointment(Long idAppointment) {
        var rowAppointment = springJpaAppointmentRepository.findById(idAppointment);
        if (rowAppointment.isEmpty()) return null;
        rowAppointment.get().setState(1);
        var rowUpdated = springJpaAppointmentRepository.save(rowAppointment.get());
        return appointmentMapper.toAppointment(rowUpdated);
    }

    @Override
    public void finalizeAppointment(LocalDateTime now) {
        springJpaAppointmentRepository.setFinalizeAppointment(now);
    }
}
