package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.util.List;
import java.util.UUID;

public interface GetAvailabilityUseCase {
    List<Availability> getAvailability(UUID specialistId);
}
