package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.entities.VideoCallJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisteredVideoCall;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring", uses = {AppointmentMapper.class})
public interface VideoCallMapper {
    @Mapping(source= "id", target = "id")
    @Mapping(source = "appointment", target = "appointment")
    @Mapping(source = "videoCallUuid", target = "videoCallUuid")
    @Mapping(source= "specialistAgoraUID", target = "specialistAgoraUID")
    @Mapping(source= "patientAgoraUID", target = "patientAgoraUID")
    @Mapping(source= "agoraChannelName", target = "agoraChannelName")
    @Mapping(source= "specialistToken", target = "specialistToken")
    @Mapping(source= "patientToken", target = "patientToken")
    @Mapping(source= "agoraTokenRecorder", target = "agoraTokenRecorder")
    @Mapping(source= "firstTimeAgoraTokenRecorder", target = "firstTimeAgoraTokenRecorder")
    @Mapping(source= "createdAt", target = "createdAt")
    VideoCall toVideoCall(VideoCallJpaEntity videoCallJpaEntity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "appointment", ignore = true)
    @Mapping(target = "videoCallUuid", ignore = true)
    @Mapping(source = "agoraChannelName", target = "agoraChannelName")
    @Mapping(source = "specialistUID", target = "specialistAgoraUID")
    @Mapping(source = "patientUID", target = "patientAgoraUID")
    @Mapping(target = "specialistToken", ignore = true)
    @Mapping(target = "patientToken", ignore = true)
    @Mapping(target = "agoraTokenRecorder", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    VideoCallJpaEntity toVideoCallJpaEntity(RegisteredVideoCall registeredVideoCall);
}
