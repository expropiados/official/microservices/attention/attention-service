package org.expropiados.cuidemonos.attentionservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.*;

import java.util.ArrayList;

@UseCase
@RequiredArgsConstructor
public class GetMedicalReasonMonthlyFreqReportService implements GetMedicalReasonMonthlyFreqReportUseCase {
    @Override
    public GetMedicalReasonMonthlyFreqReport getMedicalReasonMonthlyFreqReport(GetMedicalReasonMonthlyReport getMedicalReasonMonthly) {
        var motives = getMedicalReasonMonthly.getSeriesLine();
        var freqMonthly = new ArrayList<GetMedicalReasonLastFrequency>();
        for (GetMedicalReasonMonthly motive:motives){
            var freqMedicalReason = new GetMedicalReasonLastFrequency();
            freqMedicalReason.setMotivo(motive.getName());
            freqMedicalReason.setRate(calculateRate(motive.getData()));
            freqMedicalReason.setValue(motive.getData()[motive.getData().length-1]);
            freqMonthly.add(freqMedicalReason);
        }
        return GetMedicalReasonMonthlyFreqReport.builder()
                .motivos(freqMonthly)
                .series(getMedicalReasonMonthly.getSeriesLine())
                .categories(getMedicalReasonMonthly.getCategories()).build();
    }

    private Integer calculateRate(int [] data){
        int size = data.length-1;
        if (size == 0) return 0;
        int lastValue = data[size];
        int comparedValue = data[size-1];

        if (comparedValue == 0){
            if (lastValue == 0) return 0;
            return 100;
        }
        return 100*(lastValue-comparedValue)/comparedValue;
    }
}
