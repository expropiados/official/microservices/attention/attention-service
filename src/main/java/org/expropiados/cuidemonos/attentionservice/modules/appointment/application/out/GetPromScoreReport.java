package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GetPromScoreReport {
    private double averageScore;
    private List<GetPromScoreReason> scoreXReason;
    private int[][] scoreDetails;
}
