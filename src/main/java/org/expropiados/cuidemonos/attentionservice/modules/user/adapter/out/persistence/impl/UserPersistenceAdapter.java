package org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.utils.Constants;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.mappers.PatientMapper;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.mappers.SpecialistMapper;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.repositories.SpringJpaPatientRepository;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.repositories.SpringJpaSpecialistRepository;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.repositories.SpringJpaUserRepository;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetPatientByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetSpecialistByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.ListPatientsPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class UserPersistenceAdapter implements ListPatientsPort, GetUserPort, GetSpecialistByUserUuidPort, GetPatientByUserUuidPort {

    private final SpringJpaUserRepository springJpaUserRepository;
    private final SpringJpaPatientRepository springJpaPatientRepository;
    private final SpringJpaSpecialistRepository springJpaSpecialistRepository;

    private final SpecialistMapper specialistMapper;
    private final PatientMapper patientMapper;

    @Override
    public Map<UUID, User> listPatients(List<UUID> uuidList) {
        var rowsPatient = springJpaPatientRepository.findAllByUser_UuidUserIn(uuidList);
        return patientMapper.toUsers(rowsPatient)
                .stream()
                .collect(Collectors.toMap(User::getUuid, Function.identity()));
    }

    @Override
    public Map<UUID, User> listSpecialists(List<UUID> uuidList) {
        var rowsSpecialist = springJpaSpecialistRepository.findDistinctByUser_UuidUserIn(uuidList);
        return specialistMapper.toUsers(rowsSpecialist)
                .stream()
                .collect(Collectors.toMap(User::getUuid, Function.identity()));
    }

    @Override
    public List<User> listPatientsByName(UUID specialistUuid, String search) {
        var pageable = PageRequest.of(0, 10);
        var pagePatients = springJpaPatientRepository.searchPatientsBySpecialist(pageable,search, specialistUuid, 1);
        return pagePatients.getContent()
                .stream()
                .map(patientMapper::toUser)
                .collect(Collectors.toList());
    }

    @Override
    @Deprecated(since = "3.0.0", forRemoval = true)
    public Optional<User> getUser(UUID uuidUser) {
        var userRow = springJpaUserRepository.findById(uuidUser);
        return Optional.empty();
        // return userRow.map(userMapper::toUser);
    }

    public Optional<User> getPatientByUserUuid(UUID uuidUser) {
        var row = springJpaPatientRepository.findByUser_UuidUser(uuidUser);
         return row.map(patientMapper::toUser);
    }

    public Optional<User> getSpecialistByUserUuid(UUID uuidUser) {
        var row = springJpaSpecialistRepository.findByUser_UuidUser(uuidUser);
        return row.map(specialistMapper::toUser);
    }


}
