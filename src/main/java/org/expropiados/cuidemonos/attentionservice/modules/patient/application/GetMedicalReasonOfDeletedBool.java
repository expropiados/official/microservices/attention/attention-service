package org.expropiados.cuidemonos.attentionservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonsOfDeletedUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.GetMedicalReasonPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MedicalReason;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetMedicalReasonOfDeletedBool implements GetMedicalReasonsOfDeletedUseCase {

    private final GetMedicalReasonPort getMedicalReasonPort;

    @Override
    public List<MedicalReason> getMedicalReasonsOfDeleted() {
        return getMedicalReasonPort.getAllDeletedMedicalReasons();
    }
}
