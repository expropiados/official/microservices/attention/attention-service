package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in.GetAvailabilitiesOfDateDTO;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.Specialist;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GetAvailabilitiesOfDateMapper {

    @Mapping(source = "user.uuid", target = "uuid")
    @Mapping(source = "user.name", target = "name")
    @Mapping(source = "user.fatherLastname", target = "fatherLastname")
    @Mapping(source = "user.motherLastname", target = "motherLastname")
    @Mapping(source = "user.profileImageUrl", target = "profileImageUrl")
    @Mapping(source = "availabilities", target = "availabilities")
    GetAvailabilitiesOfDateDTO toGetAvailabilitiesOfDateDTO(Specialist specialist);
    List<GetAvailabilitiesOfDateDTO> toGetAvailabilitiesOfDateDTOList(List<Specialist> specialists);

}
