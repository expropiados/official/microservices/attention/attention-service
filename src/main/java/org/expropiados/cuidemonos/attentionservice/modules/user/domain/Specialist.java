package org.expropiados.cuidemonos.attentionservice.modules.user.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class Specialist {
    private User user;
    private List<Availability> availabilities;
}
