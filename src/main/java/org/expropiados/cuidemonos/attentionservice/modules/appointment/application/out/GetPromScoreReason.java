package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetPromScoreReason {
    private double[] data;
}
