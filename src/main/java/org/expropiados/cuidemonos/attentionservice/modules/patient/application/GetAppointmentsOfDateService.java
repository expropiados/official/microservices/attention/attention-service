package org.expropiados.cuidemonos.attentionservice.modules.patient.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetAppointmentsOfDateUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.ListPatientsPort;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetAppointmentsOfDateService implements GetAppointmentsOfDateUseCase {

    private final GetAppointmentPort getAppointmentPort;
    private final ListPatientsPort listPatientsPort;

    @Override
    public List<Appointment> getPendentAppointmentsOfDate(UUID patientUuid, LocalDate dateBegin, LocalDate dateEnd) {

        ArrayList<UUID> uuids = new ArrayList<>();

        var appointments = getAppointmentPort.getPendentAppointmentForPatient(patientUuid, dateBegin, dateEnd);

        appointments.forEach(appointment -> uuids.add(appointment.getSpecialistUuid()));

        var mapSpecialists = listPatientsPort.listSpecialists(uuids);

        appointments.forEach(appointment ->
            appointment.setSpecialist(mapSpecialists.get(appointment.getSpecialistUuid())));

        return appointments;
    }

}
