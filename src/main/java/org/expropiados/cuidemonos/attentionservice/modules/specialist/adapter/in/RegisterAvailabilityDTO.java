package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class RegisterAvailabilityDTO {
    List<AvailabilityDTO> availabilityDTOs;
    int replicate;
    LocalDate dateBegin;
    LocalDate dateEnd;
}
