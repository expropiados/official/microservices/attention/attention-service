package org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out;

import java.util.UUID;

public interface RegisterRatingForSpecialistPort {
    void registerRatingForSpecialist(UUID specialistUserUuid, Integer rating);
}
