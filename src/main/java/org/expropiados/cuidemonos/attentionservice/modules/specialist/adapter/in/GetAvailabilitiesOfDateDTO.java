package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import lombok.Data;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.util.List;
import java.util.UUID;

@Data
public class GetAvailabilitiesOfDateDTO {
    private UUID uuid;
    private String name;
    private String fatherLastname;
    private String motherLastname;
    private String profileImageUrl;
    private List<Availability> availabilities;
}
