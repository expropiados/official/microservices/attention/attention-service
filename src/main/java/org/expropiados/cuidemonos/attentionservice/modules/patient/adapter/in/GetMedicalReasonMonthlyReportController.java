package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonMonthlyReport;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonMonthlyReportUseCase;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;


@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/report/monthly")
public class GetMedicalReasonMonthlyReportController {

    private final GetMedicalReasonMonthlyReportUseCase getMedicalReasonMonthlyReportUseCase;

    @GetMapping("")
    public GetMedicalReasonMonthlyReport getMedicalReasonMonthlyReport(GetMedicalReasonMonthlyReportDTO getMedicalReasonMonthlyReportDTO){
        getMedicalReasonMonthlyReportDTO.setStartDate(LocalDate.of(getMedicalReasonMonthlyReportDTO.getStartDate().getYear(), getMedicalReasonMonthlyReportDTO.getStartDate().getMonth(), 1));
        getMedicalReasonMonthlyReportDTO.setEndDate(LocalDate.of(getMedicalReasonMonthlyReportDTO.getEndDate().getYear(), getMedicalReasonMonthlyReportDTO.getEndDate().getMonth(), 1));
        return getMedicalReasonMonthlyReportUseCase.getMedicalReasonMonthlyReport(getMedicalReasonMonthlyReportDTO.getStartDate(), getMedicalReasonMonthlyReportDTO.getEndDate(), -1L);
    }
}
