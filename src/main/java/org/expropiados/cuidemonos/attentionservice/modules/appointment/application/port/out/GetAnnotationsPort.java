package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

public interface GetAnnotationsPort {
    String getAnnotations(Long idAppointment);
}
