package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.RecordingType;

public interface GetLastRecordingPort {
    Recording getLastRecordingOfVideoCall(Long videoCallId);
    Recording getLastRecordingOfVideoCallFromType(Long videoCallId, RecordingType recordingType);
}
