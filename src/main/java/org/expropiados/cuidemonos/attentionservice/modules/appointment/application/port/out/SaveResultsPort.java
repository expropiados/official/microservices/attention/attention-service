package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;

public interface SaveResultsPort {
    DetailAppointment saveResults(Long appointmentDetail, String results);
}
