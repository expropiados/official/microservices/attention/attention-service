package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import lombok.Data;

import java.util.UUID;

@Data
public class AgoraNotification {
    private UUID noticeId;
    private Integer productId;
    private Integer eventType;
    private Long notifyMs;
    private AgoraNotificationPayload payload;
}
