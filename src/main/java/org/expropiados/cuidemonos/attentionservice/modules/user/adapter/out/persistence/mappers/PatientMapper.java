package org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.entities.PatientJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PatientMapper {

    @Mapping(target = "email", source = "email")
    @Mapping(target = "phoneNumber", source = "phoneNumber")
    @Mapping(target = "profileImageUrl", source = "profileImage")
    @Mapping(target = "dni", source = "user.document")
    @Mapping(target = "name", source = "user.name")
    @Mapping(target = "uuid", source = "user.uuidUser")
    @Mapping(target = "motherLastname", source = "user.motherLastname")
    @Mapping(target = "fatherLastname", source = "user.fatherLastname")
    User toUser(PatientJpaEntity patientJpaEntity);

    List<User> toUsers(List<PatientJpaEntity> patientJpaEntities);
}
