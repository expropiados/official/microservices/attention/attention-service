package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.entities;

import lombok.Data;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.AppointmentJpaEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.utils.Constants.*;

@Data
@Entity
@Table(name = "VIDEOCALL")
public class VideoCallJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "videocall_id")
    private Long id;

    @Column(name = "appointment_id")
    private Long appointmentId;

    @Column(name = "specialist_agora_uid", length = AGORA_UID_LENGTH, nullable = false)
    private Integer specialistAgoraUID;

    @Column(name = "patient_agora_uid", length = AGORA_UID_LENGTH, nullable = false)
    private Integer patientAgoraUID;

    @Column(name = "agora_channel_name", length = AGORA_CHANNEL_NAME_LENGTH, nullable = false)
    private String agoraChannelName;

    @Column(name = "specialist_token", length = TOKEN_LENGTH)
    private String specialistToken;

    @Column(name = "patient_token", length = TOKEN_LENGTH)
    private String patientToken;

    @Column(name = "agora_token_recorder", length = TOKEN_LENGTH)
    private String agoraTokenRecorder;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @Column(name = "first_time_agora_token_recorder", columnDefinition = "boolean default true")
    private boolean firstTimeAgoraTokenRecorder;

    @Column(name = "videocall_uuid", nullable = false)
    private UUID videoCallUuid;

    @OneToOne
    @JoinColumn(name = "appointment_id", referencedColumnName = "appointment_id", insertable = false, updatable = false)
    private AppointmentJpaEntity appointment;

}
