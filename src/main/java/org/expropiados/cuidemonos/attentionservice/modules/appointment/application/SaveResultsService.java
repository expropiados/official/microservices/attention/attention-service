package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveResultsUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveResultsPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;

@UseCase
@RequiredArgsConstructor
public class SaveResultsService implements SaveResultsUseCase {

    private final SaveResultsPort saveResultsPort;

    @Override
    public DetailAppointment saveResults(Long appointmentDetail, String results) {

        return saveResultsPort.saveResults(appointmentDetail, results);

    }

}
