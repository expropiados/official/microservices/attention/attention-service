package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value
public class EnterVideoCallInput {
    @NotNull
    UUID userUUID;

    @NotNull
    Integer userType;

    @NotNull
    String agoraName;

    public EnterVideoCallInput(UUID userUUID, Integer userType,
                               String agoraName){
        this.userUUID = userUUID;
        this.userType = userType;
        this.agoraName = agoraName;
    }
}
