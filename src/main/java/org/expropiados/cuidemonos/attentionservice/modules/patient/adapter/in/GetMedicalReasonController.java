package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetMedicalReasonsOfDeletedUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MedicalReason;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/appointments")
public class GetMedicalReasonController {

    private final JwtHelper jwtHelper;
    private final GetMedicalReasonsOfDeletedUseCase getMedicalReasonsOfDeletedUseCase;

    @GetMapping("/medical-reasons")
    public ResponseEntity<List<MedicalReason>> getMedicalReason( @RequestHeader("Authorization") String token,
                                                                 @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var reasons = getMedicalReasonsOfDeletedUseCase.getMedicalReasonsOfDeleted();
        return ResponseEntity.ok(reasons);
    }

}
