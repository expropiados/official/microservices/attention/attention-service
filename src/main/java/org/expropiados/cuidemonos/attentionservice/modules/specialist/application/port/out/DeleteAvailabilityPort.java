package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out;

import java.time.LocalDate;
import java.util.UUID;

public interface DeleteAvailabilityPort {
    void deleteAvailability(UUID uuidSpecialist, LocalDate dateBegin, LocalDate dateEnd);
}
