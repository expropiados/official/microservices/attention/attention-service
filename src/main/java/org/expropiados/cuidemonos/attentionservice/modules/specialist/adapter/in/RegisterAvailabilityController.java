package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.RegisterAvailabilityUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.UUID;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@Transactional
@RequestMapping(value = "/specialists")
public class RegisterAvailabilityController {

    private final JwtHelper jwtHelper;
    private final RegisterAvailabilityUseCase registerAvailabilityUseCase;
    private final RegisterAvailabilityMapper registerAvailabilityMapper;

    @PostMapping("/{uuidSpecialist}/availability")
    @ApiOperation("Register availability from specialist")
    public ResponseEntity<HttpStatus> registerAvailability(@PathVariable UUID uuidSpecialist,
                                                  @RequestBody RegisterAvailabilityDTO registerAvailabilityDTO,
                                                           @RequestHeader("Authorization") String token,
                                                           @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var availabilities = registerAvailabilityMapper.toAvailabilityList(registerAvailabilityDTO.getAvailabilityDTOs());
        registerAvailabilityUseCase.registerAvailability(uuidSpecialist, availabilities,
                registerAvailabilityDTO.getReplicate(), registerAvailabilityDTO.getDateBegin(),
                registerAvailabilityDTO.getDateEnd());
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
