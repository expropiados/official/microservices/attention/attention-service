package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;

import java.util.Optional;


public interface GetVideoCallPort {
    VideoCall getVideoCallByChannelName(String agoraName);
    Optional<VideoCall> getVideoCallByIdAppointment(Long idAppointment);
    VideoCall getVideoCall(Long idVideoCall);
}
