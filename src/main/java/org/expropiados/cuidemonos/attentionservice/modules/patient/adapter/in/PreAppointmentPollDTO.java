package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in;

import lombok.Data;

@Data
public class PreAppointmentPollDTO {
    private Long idMedicalReason;
    private String additionalInfo;
    private String anotherReason;
}
