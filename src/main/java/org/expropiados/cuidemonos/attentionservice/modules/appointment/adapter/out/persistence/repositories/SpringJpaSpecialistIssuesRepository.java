package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.SpecialistIssueJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpringJpaSpecialistIssuesRepository extends JpaRepository<SpecialistIssueJpaEntity, Long> {
}
