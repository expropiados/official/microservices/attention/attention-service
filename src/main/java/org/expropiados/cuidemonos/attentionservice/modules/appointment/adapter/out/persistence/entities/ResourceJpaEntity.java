package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "RESOURCE")
public class ResourceJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "resource_id")
    private Long idResource;
    @Column(name = "appointment_result_id")
    private Long idAppointmentResult;
    @Column(name = "name", length =  100)
    private String name;
    @Column(name = "type")
    private Integer type;
    @Column(name = "url", length =  150)
    private String url;
    @Column(name = "owner")
    private Integer owner;

    @ManyToOne
    @JoinColumn(name = "appointment_result_id", insertable = false, updatable = false)
    private AppointmentDetailJpaEntity appointmentDetailJpaEntity;



}
