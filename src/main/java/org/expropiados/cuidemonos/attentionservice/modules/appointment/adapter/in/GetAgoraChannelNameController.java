package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.GetAgoraChannelNameUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/appointments")
public class GetAgoraChannelNameController {

    private final JwtHelper jwtHelper;
    private final GetAgoraChannelNameUseCase getAgoraChannelNameUseCase;

    @GetMapping("/{idAppointment}/video-call/agora-channel")
    @ApiOperation(value = "Get agora channel name of video-call")
    public ResponseEntity<String> getAgoraChannelName(@PathVariable Long idAppointment,
                                                      @RequestHeader("Authorization") String token,
                                                      @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var agoraChannel = getAgoraChannelNameUseCase.getAgoraNameChannelName(idAppointment);
        return ResponseEntity.ok(agoraChannel);
    }

}
