package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.utils;

public class Constants {

    private Constants(){
        //
    }
    //VideoCall
    public static final int AGORA_UID_LENGTH = 8;
    public static final int AGORA_CHANNEL_NAME_LENGTH = 12;
    public static final int TOKEN_LENGTH = 139;
}
