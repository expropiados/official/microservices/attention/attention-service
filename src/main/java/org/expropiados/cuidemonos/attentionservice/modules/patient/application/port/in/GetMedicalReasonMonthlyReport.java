package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in;

import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
@Builder
public class GetMedicalReasonMonthlyReport {
    private List<GetMedicalReasonMonthly> seriesLine;
    private List<String> categories;
}
