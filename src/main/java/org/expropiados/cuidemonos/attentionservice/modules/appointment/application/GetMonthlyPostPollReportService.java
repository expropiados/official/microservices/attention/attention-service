package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in.GetMonthlyPostPollReportUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetMonthlyPostPollByRatingReport;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetMonthlyGeneralReport;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetMonthlyPostPollReport;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetPostPollPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetMonthlyPostPollReportService implements GetMonthlyPostPollReportUseCase {

    public final GetPostPollPort getPostPollPort;

    @Override
    public GetMonthlyPostPollReport getMonthlyPostPollReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID) {
        var cantMonths = (endDate.getMonthValue()+12*(endDate.getYear()-startDate.getYear()))-startDate.getMonthValue()+1;
        var monthlyRatingReport = fillList(cantMonths);
        var monthlyGeneralRating = new ArrayList<GetMonthlyGeneralReport>(1);
        monthlyGeneralRating.add(new GetMonthlyGeneralReport());
        monthlyGeneralRating.get(0).setName("Puntaje promedio");
        monthlyGeneralRating.get(0).setData(new double[cantMonths]);
        for (var i = 0; i < cantMonths; i++) {
            var monthlyMap = fillMap();
            var iniAuxDate = startDate.plusMonths(i);
            var endAuxDate = startDate.plusMonths(i+1);
            var postPolls = getPostPollPort.getPostPollBetweenDatesForSpecialist(iniAuxDate, endAuxDate, specialistUUID);
            updateMonthlyMap(monthlyMap, postPolls);
            updateMonthlyReport(monthlyMap, monthlyRatingReport, i);
            monthlyGeneralRating.get(0).getData()[i] = calculateMonthlyProm(monthlyMap);
        }
        return GetMonthlyPostPollReport.builder()
                .seriesLine(monthlyGeneralRating)
                .seriesStackedColumn(monthlyRatingReport).build();
    }

    private HashMap<Integer, Integer> fillMap(){
        var aux = new HashMap<Integer, Integer>();
        for (var i = 0; i < 5; i++){
            aux.put((i+1), 0);
        }
        return aux;
    }

    private List<GetMonthlyPostPollByRatingReport> fillList(int size){
        List<GetMonthlyPostPollByRatingReport> monthlyReportByRating = new ArrayList<>(size);
        for (int i = 1; i <= 5; i++){
            var monthlyReport = new GetMonthlyPostPollByRatingReport();
            monthlyReport.setName("Puntaje "+i);
            monthlyReport.setData(new int[size]);
            monthlyReportByRating.add(monthlyReport);
        }
        return monthlyReportByRating;
    }

    private void updateMonthlyMap(HashMap<Integer, Integer> map, List<PostPoll> list){
        for (PostPoll postPoll: list){
            if (map.containsKey(postPoll.getDoctorRating())){
                map.put(postPoll.getDoctorRating(), map.get(postPoll.getDoctorRating())+1);
            }
        }
    }

    private void updateMonthlyReport(HashMap<Integer, Integer> map, List<GetMonthlyPostPollByRatingReport> list, int month){
        int count = 1;
        for (GetMonthlyPostPollByRatingReport monthlyReport : list){
            monthlyReport.getData()[month] = map.get(count);
            count++;
        }
    }

    private double calculateMonthlyProm(HashMap<Integer, Integer> map){
        double sum = 0;
        double cant = 0;
        for (var i=1; i<=5; i++){
            cant+= map.get(i);
            sum += cant*i;
        }
        if (cant == 0) return 0;
        return sum/cant;
    }
}
