package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.ResourceJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(componentModel="spring")
@Component
public interface ResourceMapper {
    @Mapping(target = "resourceId", source = "idResource")
    @Mapping(target = "appointmentDetailId", source = "idAppointmentResult")
    @Mapping(target = "type", source = "type")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "owner", source = "owner")
    @Mapping(target = "url", source = "url")
    Resource toResource(ResourceJpaEntity resourceJpaEntity);
    List<Resource> toResources(List<ResourceJpaEntity> resourceJpaEntity);


    @InheritInverseConfiguration
    @Mapping(target = "appointmentDetailJpaEntity", ignore = true)
    ResourceJpaEntity toResourceJpaEntity(Resource resource);
}
