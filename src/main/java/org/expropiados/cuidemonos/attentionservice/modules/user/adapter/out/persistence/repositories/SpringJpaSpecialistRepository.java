package org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.entities.SpecialistJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SpringJpaSpecialistRepository extends JpaRepository<SpecialistJpaEntity, Long> {
    List<SpecialistJpaEntity> findAllByUser_UuidUserIn(List<UUID> uuidList);

    Optional<SpecialistJpaEntity> findByUser_UuidUser(UUID userUuid);

    List<SpecialistJpaEntity> findDistinctByUser_UuidUserIn(List<UUID> uuidList);
}
