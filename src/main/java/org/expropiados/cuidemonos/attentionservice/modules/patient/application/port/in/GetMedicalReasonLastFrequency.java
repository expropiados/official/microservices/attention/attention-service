package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in;

import lombok.Data;

@Data
public class GetMedicalReasonLastFrequency {
    private String motivo;
    private Integer rate;
    private Integer value;
}
