package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.AppointmentJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SpringJpaAppointmentRepository extends JpaRepository<AppointmentJpaEntity,Long> {
    Optional<List<AppointmentJpaEntity>> findBySpecialistUUIDEqualsAndStartTimeBetweenAndStateOrderByStartTime(UUID specialistUuid,
                                                                                       LocalDateTime startTime,
                                                                                       LocalDateTime endTime, int state);
    List<AppointmentJpaEntity> findAllBySpecialistUUIDEquals(UUID specialistUuid);

    List<AppointmentJpaEntity> findAllByPatientUUIDEquals(UUID patientUuid);

    List<AppointmentJpaEntity> findAllByPatientUUIDAndStartTimeBetweenAndStateOrderByStartTime(UUID patientUuid,
                                                                      LocalDateTime startTime,
                                                                      LocalDateTime endTime, int state);

    @Modifying
    @Query("UPDATE AppointmentJpaEntity SET state = 2 WHERE endTime < :now")
    void setFinalizeAppointment(@Param("now") LocalDateTime now);

    @Query("SELECT DISTINCT A FROM AppointmentJpaEntity A INNER JOIN UserJpaEntity U ON U.uuidUser = A.patientUUID INNER JOIN PatientJpaEntity P ON P.userId = U.id AND P.clientCompanyId = :clientCompanyId WHERE A.startTime >= :startTime AND A.endTime < :endTime")
    List<AppointmentJpaEntity> searchAppointmentsByDateRangeAndClientCompany(LocalDateTime startTime, LocalDateTime endTime, Long clientCompanyId);

}
