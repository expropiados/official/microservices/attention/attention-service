package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.errors.AvailabilityAlreadyReservedError;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.BookAppointmentUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.BookAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveDetailAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SendEmailForAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetPatientByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetSpecialistByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetUserPort;

@UseCase
@RequiredArgsConstructor
public class BookAppointmentService implements BookAppointmentUseCase {

    private final BookAppointmentPort bookAppointmentPort;
    private final GetAvailabilityPort getAvailabilityPort;
    private final SaveDetailAppointmentPort saveDetailAppointmentPort;
    private final GetSpecialistByUserUuidPort getSpecialistByUserUuidPort;
    private final GetPatientByUserUuidPort getPatientByUserUuidPort;
    private final SendEmailForAppointmentPort sendEmailForAppointmentPort;

    @Override
    public Appointment bookAppointment(Appointment appointment, Long idAvailability){

        var availabilityOfReserved = getAvailabilityPort.getAvailability(idAvailability);

        if(availabilityOfReserved.isPresent() && availabilityOfReserved.get().getState().equals(1)){
            throw new AvailabilityAlreadyReservedError(availabilityOfReserved.get().getStartTime(),
                    availabilityOfReserved.get().getEndTime());
        }

        var availabilityUpdated =getAvailabilityPort.changeStateAvailability(idAvailability);

        availabilityUpdated.ifPresent(availability -> {
            appointment.setStartTime(availability.getStartTime());
            appointment.setEndTime(availability.getEndTime());
            appointment.setState(0);
        });

        var appointmentSaved =  bookAppointmentPort.bookAppointment(appointment);

        var patient = getPatientByUserUuidPort.getPatientByUserUuid(appointment.getPatientUuid());

        patient.ifPresent(appointmentSaved::setPatient);

        var specialist = getSpecialistByUserUuidPort.getSpecialistByUserUuid(appointment.getSpecialistUuid());

        specialist.ifPresent(appointmentSaved::setSpecialist);

        var detailAppointment = DetailAppointment.builder().appointment(appointmentSaved).build();
        saveDetailAppointmentPort.saveDetailAppointment(detailAppointment);


        sendEmailForAppointmentPort.sendEmailPatientForAppointment(appointmentSaved.getPatient(),
                appointmentSaved.getSpecialist(), appointmentSaved.getPatient().getEmail(), appointmentSaved);

        sendEmailForAppointmentPort.sendEmailSpecialistForAppointment(appointmentSaved.getPatient(),
                appointmentSaved.getSpecialist(), appointmentSaved.getSpecialist().getEmail(), appointmentSaved);


        return appointmentSaved;

    }
}
