package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out;


public interface SaveTokenPort {
    void saveToken(Long appointmentId, String token, int role);
    void saveAgoraTokenRecorder(Long videoCallId, String agoraTokenRecorder);
}
