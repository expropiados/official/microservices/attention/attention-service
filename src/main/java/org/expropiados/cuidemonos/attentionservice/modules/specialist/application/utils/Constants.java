package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.utils;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Constants {
    //State Availability
    public static final int STATE_AVAILABILITY_AVAILABLE= 0;
    public static final int STATE_AVAILABILITY_RESERVED= 1;
}
