package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface AppointmentDTOMapper {

    @Mapping(source = "patientUuid", target = "patientUuid")
    @Mapping(source = "patient.name", target = "patientName")
    @Mapping(source = "patient.fatherLastname", target = "patientFatherLastname")
    @Mapping(source = "patient.motherLastname", target = "patientMotherLastname")
    @Mapping(source = "startTime", target = "appointmentStartTime")
    @Mapping(source = "endTime", target = "appointmentEndTime")
    @Mapping(source = "patient.profileImageUrl", target = "profileImageUrl")
    @Mapping(source = "id", target = "appointmentId")
    GetAppointmentDTO toAppointmentDto (Appointment appointment);
    List<GetAppointmentDTO> toAppointmentsDto (List<Appointment> appointments);

}
