package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.LocalDateTimePeruZone;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.FinalizeAppointmentUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.FinalizeAppointmentPort;

import javax.transaction.Transactional;

@UseCase
@Transactional
@RequiredArgsConstructor
public class FinalizeAppointmentService implements FinalizeAppointmentUseCase {

    private final FinalizeAppointmentPort finalizeAppointmentPort;

    @Override
    public void finalizeAppointment() {
        var now = LocalDateTimePeruZone.now();
        finalizeAppointmentPort.finalizeAppointment(now);
    }

}
