package org.expropiados.cuidemonos.attentionservice.modules.videocall.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.attention.RecordingAppConfig;
import org.expropiados.cuidemonos.attentionservice.config.attention.VideocallingAppConfig;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.GetRecordingDataUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.GetLastRecordingPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveTokenPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.*;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.util.RtcTokenBuilder;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;

@UseCase
@RequiredArgsConstructor
public class GetRecordingDataService implements GetRecordingDataUseCase {

    private final VideocallingAppConfig videocallingAppConfig;
    private final RecordingAppConfig recordingAppConfig;
    private final SaveTokenPort saveTokenPort;
    private final GetLastRecordingPort getLastRecordingPort;

    @Override
    public RecordingEntryData getRecordingData(VideoCallCredentials videoCallCredentials,
                                               Integer userType, VideoCall videocall) {
        if (userType != 1) return null;
        String result;
        if (videocall.getAgoraTokenRecorder() == null){
            var token = new RtcTokenBuilder();
            var duration = setVideoCallDurationRange(videocall.getAppointment().getEndTime());
            var appId = videocallingAppConfig.getId();
            var appCertificate = videocallingAppConfig.getCertificate();

            result = token.buildTokenWithUid(appId, appCertificate,
                    videocall.getAgoraChannelName(), recordingAppConfig.getUidRecorder(), RtcTokenBuilder.Role.ROLE_PUBLISHER, duration);
            saveTokenPort.saveAgoraTokenRecorder(videocall.getId(), result);
        }
        else{
            result = videocall.getAgoraTokenRecorder();
        }
        var recordingIndividual = getLastRecordingPort.getLastRecordingOfVideoCallFromType(videocall.getId(), RecordingType.INDIVIDUAL);
        var recordingDual = getLastRecordingPort.getLastRecordingOfVideoCallFromType(videocall.getId(), RecordingType.DUAL);

        var authTokenRecorder = generateAuthToken();
        return RecordingEntryData.builder()
                .agoraTokenRecorder(result)
                .firstTimeAgoraTokenRecorder(videocall.isFirstTimeAgoraTokenRecorder())
                .sidIndividual(checkSID(recordingIndividual))
                .resourceIndividualId(checkResourceId(recordingIndividual))
                .sidDual(checkSID(recordingDual))
                .resourceDualId(checkResourceId(recordingDual))
                .authTokenRecorder(authTokenRecorder)
                .uidRecorder(recordingAppConfig.getUidRecorder())
                .build();
    }

    private int setVideoCallDurationRange(LocalDateTime endDate){
        long endSeconds = endDate.atZone(ZoneId.of("EST", ZoneId.SHORT_IDS)).toInstant().getEpochSecond();
        long nowSeconds= LocalDateTime.now().atZone(ZoneId.of("EST", ZoneId.SHORT_IDS)).toInstant().getEpochSecond();
        return (int) ((nowSeconds) + (endSeconds-nowSeconds));
    }

    private String generateAuthToken() {
        var encoder = Base64.getEncoder();
        var originalString = recordingAppConfig.getUsername()+":"+recordingAppConfig.getPassword();
        return encoder.encodeToString(originalString.getBytes());
    }

    private String checkResourceId(Recording recording){
        if (recording == null) return null;
        return recording.getResourceId();
    }

    private String checkSID(Recording recording){
        if (recording == null) return null;
        return recording.getSid();
    }
}
