package org.expropiados.cuidemonos.attentionservice.modules.videocall.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RecordingInput;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.SaveRecordingUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveRecordingPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.RecordingType;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.util.RandomString;

@UseCase
@RequiredArgsConstructor
public class SaveRecordingService implements SaveRecordingUseCase {

    public final SaveRecordingPort saveRecordingPort;

    @Override
    public Recording saveRecord(RecordingInput recordingInput) {
        var folderNameStr = new RandomString().randomFolderName();
        return saveRecordingPort.saveRecording(recordingInput.getVideoCallId(), folderNameStr, recordingInput.getRecordingType().getValue());
    }
}
