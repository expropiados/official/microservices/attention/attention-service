package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MedicalReason;
import java.util.List;

public interface GetMedicalReasonPort {
    List<MedicalReason> getAllMedicalReasons();
    List<MedicalReason> getAllDeletedMedicalReasons();
}
