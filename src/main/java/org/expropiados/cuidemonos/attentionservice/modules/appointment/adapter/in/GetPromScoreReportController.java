package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in.GetPromScoreReportUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetPromScoreReport;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/report/appointment")
public class GetPromScoreReportController {
    private final GetPromScoreReportUseCase getPromScoreReportUseCase;

    @GetMapping("/score")
    public GetPromScoreReport getPromScoreReport(GetPromScoreReportDTO getPromScoreReportDTO){
        getPromScoreReportDTO.setStartDate(LocalDate.of(getPromScoreReportDTO.getStartDate().getYear(), getPromScoreReportDTO.getStartDate().getMonth(), 1));
        getPromScoreReportDTO.setEndDate(LocalDate.of(getPromScoreReportDTO.getEndDate().getYear(), getPromScoreReportDTO.getEndDate().getMonth(), 1));
        return getPromScoreReportUseCase.getPromPostPollReport(getPromScoreReportDTO);
    }
}
