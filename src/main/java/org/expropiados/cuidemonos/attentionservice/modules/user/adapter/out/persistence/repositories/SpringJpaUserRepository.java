package org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.entities.UserJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface SpringJpaUserRepository extends JpaRepository<UserJpaEntity, UUID> {
    List<UserJpaEntity> findAllByUuidUserIn(List<UUID> uuidList);
    List<UserJpaEntity> findDistinctByUuidUserIn(List<UUID> uuidList);
}