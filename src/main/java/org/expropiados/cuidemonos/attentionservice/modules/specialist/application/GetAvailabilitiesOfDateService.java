package org.expropiados.cuidemonos.attentionservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.LocalDateTimePeruZone;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.commands.AvailabilityOfDateCommand;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetAvailabilitiesOfDateUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilitiesOfDatePort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.ListPatientsPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.Specialist;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Stream;

@UseCase
@RequiredArgsConstructor
public class GetAvailabilitiesOfDateService implements GetAvailabilitiesOfDateUseCase {

    private final GetAvailabilitiesOfDatePort getAvailabilitiesOfDatePort;
    private final GetAppointmentPort getAppointmentPort;
    private final ListPatientsPort listPatientsPort;

    @Override
    public List<AvailabilityOfDateCommand> getSpecialistsAvailabilitiesOfRangeDate(LocalDate dateBegin,
                                                                    LocalDate dateEnd) {

        ArrayList<AvailabilityOfDateCommand> availabilityOfDateCommands = new ArrayList<>();

        Stream.iterate(dateBegin, date -> date.plusDays(1L))
                .limit(ChronoUnit.DAYS.between(dateBegin, dateEnd)+1)
                .forEach(date -> availabilityOfDateCommands.add(AvailabilityOfDateCommand.builder()
                        .date(date).build()));

        List<Availability> availabilities = getAvailabilitiesOfDatePort.getSpecialistsAvailabilitiesOfDate(dateBegin, dateEnd);

        ArrayList<UUID> uuids = new ArrayList<>();

        availabilities.forEach(availability -> uuids.add(availability.getSpecialistUuid()));

        var mapSpecialist = listPatientsPort.listSpecialists(uuids);

        ArrayList<Specialist> specialists = new ArrayList<>();

        mapSpecialist.forEach((uuid, user) -> {
            var specialist = Specialist.builder().user(user).build();
            specialists.add(specialist);
        });

        specialists.forEach(specialist -> {
            var uuid = specialist.getUser().getUuid();
            List<Availability> availabilityList = new ArrayList<>();
            availabilities.forEach(availability -> {
                if(availability.getSpecialistUuid().equals(uuid)) availabilityList.add(availability);
            });
            specialist.setAvailabilities(availabilityList);
        });

        availabilityOfDateCommands.forEach(availabilityOfDateCommand -> {
            var dateStart = availabilityOfDateCommand.getDate().atStartOfDay();
            var dateFinalize = availabilityOfDateCommand.getDate().plusDays(1L).atStartOfDay();
            List<Specialist> specialistForDate = new ArrayList<>();
            specialists.forEach(specialist -> {
                List<Availability> availabilityList = new ArrayList<>();
                specialist.getAvailabilities().forEach(availability -> {
                    var now = LocalDateTimePeruZone.now();
                    if(availability.getStartTime().isAfter(dateStart) &&
                       availability.getEndTime().isBefore(dateFinalize)
                            && availability.getStartTime().isAfter(now)) availabilityList.add(availability);
                });
                if(!availabilityList.isEmpty())
                        specialistForDate.add(Specialist.builder().user(specialist.getUser())
                        .availabilities(availabilityList).build());
            });
            availabilityOfDateCommand.setSpecialists(specialistForDate);
        });

        return availabilityOfDateCommands;
    }


    @Override
    public List<Availability> getAvailabilitiesOfRangeDate(LocalDate dateBegin, LocalDate dateEnd, UUID specialistUuid) {

        ArrayList<UUID> patientUuids = new ArrayList<>();

        var availabilities = getAvailabilitiesOfDatePort
                .getAvailabilitiesOfRangeDate(dateBegin, dateEnd, specialistUuid);

        var appointments = getAppointmentPort.getAppointmentOfAvailability(specialistUuid);

        appointments.forEach((localDateTime, appointment) ->
            patientUuids.add(appointment.getPatientUuid())
        );

        var mapPatients = listPatientsPort.listPatients(patientUuids);

        appointments.forEach((localDateTime, appointment) ->
            appointment.setPatient(mapPatients.get(appointment.getPatientUuid()))
        );

        availabilities.forEach(availability ->
            availability.setAppointment(appointments.get(availability.getStartTime()))
        );

        return availabilities;
    }
}
