package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.RecordingType;

public interface SaveRecordingUseCase {
    Recording saveRecord(RecordingInput recordingInput);
}
