package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Optional;

public interface GetAvailabilityPort {
    List<Availability> getAvailabilityList(UUID especialistId);
    Map<LocalDateTime, Availability> getAvailabilityReservedListByUuidSpecialist(UUID specialistUuid);
    Optional<Availability> getAvailability(Long idAvailability);
    Optional<Availability> changeStateAvailability(Long idAvailability);
}