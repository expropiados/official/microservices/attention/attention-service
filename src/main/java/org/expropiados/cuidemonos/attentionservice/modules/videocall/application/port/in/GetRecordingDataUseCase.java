package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.RecordingEntryData;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCallCredentials;

public interface GetRecordingDataUseCase {
    RecordingEntryData getRecordingData(VideoCallCredentials videoCallCredentials, Integer userType,
                                        VideoCall videoCall);
}
