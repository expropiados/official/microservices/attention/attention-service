package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface GetAppointmentsOfDateUseCase {
    public List<Appointment> getPendentAppointmentsOfDate(UUID patientUuid, LocalDate dateBegin, LocalDate dateEnd);
}
