package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out;

import lombok.Data;

@Data
public class GetMonthlyPostPollByRatingReport {
    private String name;
    private int[] data;
}
