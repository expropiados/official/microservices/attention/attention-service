package org.expropiados.cuidemonos.attentionservice.modules.videocall.util;

public interface PackableEx extends Packable {
    void unmarshal(ByteBuf in);
}
