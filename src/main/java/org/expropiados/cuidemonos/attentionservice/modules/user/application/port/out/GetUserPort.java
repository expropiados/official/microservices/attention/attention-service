package org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

import java.util.Optional;
import java.util.UUID;

public interface GetUserPort {
    Optional<User> getUser(UUID uuidUser);
}
