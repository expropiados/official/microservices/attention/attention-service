package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.errors;

import lombok.Getter;
import org.expropiados.cuidemonos.attentionservice.hexagonal.errors.BusinessException;

import java.time.LocalDateTime;


@Getter
public class AvailabilityAlreadyReservedError extends RuntimeException implements BusinessException {
    private final String code = "APP_ALRREGI_001";
    private final String message;
    private final Object data;
    public AvailabilityAlreadyReservedError(LocalDateTime startTime, LocalDateTime endTime){
        super();
        this.message = String.format("No se ha podido registrar la cita debido a que alguien reservo " +
                "en el horario escogido de %s a %s", startTime.toString(), endTime.toString());
        this.data = null;
    }
}
