package org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "PATIENT", schema = "backoffice")
public class PatientJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "patient_id")
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Column(name = "email", length = 50, nullable = false)
    private String email;

    @Column(name = "address", length = 100)
    private String address;

    @Column(name = "profile_image", length = 300)
    private String profileImage;

    @Column(name = "phone_number", length = 9)
    private String phoneNumber;

    @Column(name = "client_company_id")
    private Long clientCompanyId;

    @Column(name = "is_blocked")
    private Boolean isBlocked;


    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private UserJpaEntity user;
}
