package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.AgoraNotification;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/video_call/agora")
public class NotifyAgoraController {
    @PostMapping("")
    public Integer notifyAgora(
            @RequestBody AgoraNotification agoraNotification,
            @RequestHeader("Agora-Signature") String agoraSignature) {
        if (agoraNotification.getEventType() == 31) return 1;
        else return 0;
    }
}
