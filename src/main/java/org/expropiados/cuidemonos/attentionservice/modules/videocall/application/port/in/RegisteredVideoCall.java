package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
@EqualsAndHashCode(callSuper = false)
public class RegisteredVideoCall {
    String agoraChannelName;

    String specialistUID;

    String patientUID;

    @NotNull
    Long appointmentId;

    public RegisteredVideoCall(String agoraChannelName, String specialistUID,
                               String patientUID, Long appointmentId){
        this.agoraChannelName = agoraChannelName;
        this.specialistUID = specialistUID;
        this.patientUID = patientUID;
        this.appointmentId = appointmentId;
    }
}
