package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

public interface RegisterVideoCallUseCase {
    String registerVideoCall(Long appointmentId);
}
