package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCallCredentials;

public interface GenerateVideoCallUseCase {
    VideoCallCredentials generateVideoCalling(GenerateVideoCallInput generateVideoCallInput);
}
