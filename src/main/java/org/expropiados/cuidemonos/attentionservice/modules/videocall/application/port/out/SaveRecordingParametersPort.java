package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;

public interface SaveRecordingParametersPort {
    Recording saveRecodingParameters(Long videoCallId, String resourceId, String sid, String folderName);

}
