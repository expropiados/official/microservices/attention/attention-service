package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.PostPollDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel="spring")
public interface SavePostPollAnswersDTOMapper {

    @Mapping(target = "callRating", ignore = true)
    @Mapping(source = "callComments", target = "callComments")
    @Mapping(target = "doctorRating", ignore = true)
    @Mapping(source = "doctorComments", target = "doctorComments")
    @Mapping(target = "selectedVideoIssues", ignore = true)
    @Mapping(target = "selectedDoctorIssues", ignore = true)
    @Mapping(target = "selectedAudioIssues", ignore = true)
    @Mapping(target = "idPostPoll", ignore = true)
    @Mapping(target = "idAppointment", ignore = true)
    PostPoll toPostPollAnswers(PostPollDTO postPollDTO);

    @AfterMapping
    default void convertStringToIntegerRating(PostPollDTO postPollDTO, @MappingTarget PostPoll postPoll){
        postPoll.setCallRating(Integer.parseInt(postPollDTO.getCallRating()));
        postPoll.setDoctorRating(Integer.parseInt(postPollDTO.getDoctorRating()));
    }


}
