package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

public interface BookAppointmentUseCase {
    Appointment bookAppointment(Appointment appointment, Long idAvailability);
}
