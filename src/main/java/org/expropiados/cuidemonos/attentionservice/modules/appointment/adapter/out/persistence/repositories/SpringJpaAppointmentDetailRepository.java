package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.AppointmentDetailJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpringJpaAppointmentDetailRepository extends JpaRepository<AppointmentDetailJpaEntity, Long> {
    AppointmentDetailJpaEntity findAllByAppointmentJpaEntityIdAppointment(Long idAppointment);
    AppointmentDetailJpaEntity findByIdResultAppointment(Long idAppointmentDetail);
    AppointmentDetailJpaEntity findByAppointmentJpaEntityIdAppointment(Long idAppointment);
}
