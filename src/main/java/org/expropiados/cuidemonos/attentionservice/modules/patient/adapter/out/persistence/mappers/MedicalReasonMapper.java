package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.entities.MedicalReasonJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MedicalReason;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MedicalReasonMapper {
    @Mapping(source = "deleted", target = "deleted")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "idMedicalReason", target = "idMedicalReason")
    MedicalReason toMedicalReason(MedicalReasonJpaEntity medicalReasonJpaEntity);
    List<MedicalReason> toMedicalReasonList(List<MedicalReasonJpaEntity> medicalReasonJpaEntityList);
}
