package org.expropiados.cuidemonos.attentionservice.modules.videocall.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.TreeMap;

import static org.expropiados.cuidemonos.attentionservice.modules.videocall.util.UtilsDecoding.crc32;

public class AccessToken {
    public enum Privileges {
        K_JOIN_CHANNEL(1),
        K_PUBLISH_AUDIO_STREAM(2),
        K_PUBLISH_VIDEO_STREAM(3),
        K_PUBLISH_DATA_STREAM(4);


        short intValue;

        Privileges(int value) {
            intValue = (short) value;
        }
    }

    private static final String VER = "006";

    String appId;
    String appCertificate;
    String channelName;
    String uid;
    byte[] signature;
    byte[] messageRawContent;
    int crcChannelName;
    int crcUid;
    PrivilegeMessage message;

    public AccessToken(String appId, String appCertificate, String channelName, String uid) {
        this.appId = appId;
        this.appCertificate = appCertificate;
        this.channelName = channelName;
        this.uid = uid;
        this.crcChannelName = 0;
        this.crcUid = 0;
        this.message = new PrivilegeMessage();
    }

    public String build() throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        if (! UtilsDecoding.isUUID(appId)) {
            return "";
        }

        if (!UtilsDecoding.isUUID(appCertificate)) {
            return "";
        }

        messageRawContent = UtilsDecoding.pack(message);
        signature = generateSignature(appCertificate,
        		appId, channelName, uid, messageRawContent);
        crcChannelName = crc32(channelName);
        crcUid = crc32(uid);

        var packContent = new PackContent(signature, crcChannelName, crcUid, messageRawContent);
        byte[] content = UtilsDecoding.pack(packContent);
        return getVersion() + this.appId + UtilsDecoding.base64Encode(content);
    }

    public void addPrivilege(Privileges privilege, int expireTimestamp) {
        message.messages.put(privilege.intValue, expireTimestamp);
    }

    public static String getVersion() {
        return VER;
    }

    public static byte[] generateSignature(String appCertificate,
    		String appID, String channelName, String uid, byte[] message)
            throws IOException, NoSuchAlgorithmException, InvalidKeyException {

        var baos = new ByteArrayOutputStream();
        try {
            baos.write(appID.getBytes());
            baos.write(channelName.getBytes());
            baos.write(uid.getBytes());
            baos.write(message);
        } catch (IOException e) {
            baos.close();
        }
        return UtilsDecoding.hmacSign(appCertificate, baos.toByteArray());
    }

    public static class PrivilegeMessage implements PackableEx {
        int salt;
        int ts;
        TreeMap<Short, Integer> messages;

        public PrivilegeMessage() {
            salt = UtilsDecoding.randomInt();
            ts = UtilsDecoding.getTimestamp() + 24 * 3600;
            messages = new TreeMap<>();
        }

        @Override
        public ByteBuf marshal(ByteBuf out) {
            return out.put(salt).put(ts).putIntMap(messages);
        }

        @Override
        public void unmarshal(ByteBuf in) {
            salt = in.readInt();
            ts = in.readInt();
            messages = in.readIntMap();
        }
    }

    public static class PackContent implements PackableEx {
        byte[] signature;
        int crcChannelName;
        int crcUid;
        byte[] rawMessage;


        public PackContent(byte[] signature, int crcChannelName, int crcUid, byte[] rawMessage) {
            this.signature = signature;
            this.crcChannelName = crcChannelName;
            this.crcUid = crcUid;
            this.rawMessage = rawMessage;
        }

        @Override
        public ByteBuf marshal(ByteBuf out) {
            return out.put(signature).put(crcChannelName).put(crcUid).put(rawMessage);
        }

        @Override
        public void unmarshal(ByteBuf in) {
            signature = in.readBytes();
            crcChannelName = in.readInt();
            crcUid = in.readInt();
            rawMessage = in.readBytes();
        }
    }
}
