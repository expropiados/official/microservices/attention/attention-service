package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

public interface CancelAppointmentUseCase {
    Appointment cancelAppointment(Long idAppointment);
}
