package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.BookAppointmentUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
public class BookAppointmentController {

    private final JwtHelper jwtHelper;
    private final BookAppointmentUseCase bookAppointmentUseCase;
    //private final BookAppointmentMapper bookAppointmentMapper;

    @PostMapping("/appointments")
    @ApiOperation(value = "Book appointment")
    public Appointment bookAppointment(@RequestBody BookAppointmentDTO bookAppointmentDTO,
                                       @RequestHeader("Authorization") String token,
                                       @RequestHeader("role") Integer role) {

        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if(userRole!= UserRole.PATIENT){
            throw new NotAuthorizedForRoleException(userRole);
        }

        //var appointment = bookAppointmentMapper.toAppointment(bookAppointmentDTO);
        var appointmentPostMap = Appointment.builder()
                .patientUuid(bookAppointmentDTO.getUuidPatient())
                .specialistUuid(bookAppointmentDTO.getUuidSpecialist()).build();
        return bookAppointmentUseCase.bookAppointment(appointmentPostMap, bookAppointmentDTO.getIdAvailability());

    }
}
