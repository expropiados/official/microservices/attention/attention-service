package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface GetPostPollPort {
    List<PostPoll> getPostPollBetweenDatesForSpecialist(LocalDate startDate, LocalDate endDate, UUID specialistUUID);
    List<PostPoll> getPostPollBetweenDates(LocalDate startDate, LocalDate endDate);
    PostPoll getPostPollByAppointment(Long appointmentId);
}
