package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.mappers.GetDetailAppointmentDTOMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in.GetDetailAppointmentUseCase;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/appointments")
public class GetDetailAppointmentController {

    private final JwtHelper jwtHelper;
    private final GetDetailAppointmentUseCase getDetailAppointmentUseCase;
    private final GetDetailAppointmentDTOMapper getDetailAppointmentDTOMapper;

    @GetMapping("/{idAppointment}/appointment-detail")
    public ResponseEntity <GetDetailAppointmentDTO> appointmentDetail(@PathVariable Long idAppointment,
                                                                      @RequestHeader("Authorization") String token,
                                                                      @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var detailAppointment = getDetailAppointmentUseCase.getDetailAppointment(idAppointment);
        var detailAppointmentDTO = getDetailAppointmentDTOMapper
                .toGetDetailAppointmentDTO(detailAppointment);

        return ResponseEntity.ok(detailAppointmentDTO);
    }
}
