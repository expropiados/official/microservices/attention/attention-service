package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities;

import lombok.Data;
import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.entities.MedicalReasonJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.entities.PreAppointmentPollJpaEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "APPOINTMENT")
public class AppointmentJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "appointment_id")
    private Long idAppointment;

    @Column(name = "specialist_uuid")
    private UUID specialistUUID;

    @Column(name = "patient_uuid")
    private UUID patientUUID;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    private LocalDateTime endTime;

    @Column(name = "state")
    private Integer state;

}
