package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.entities.AvailabilityJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;


public interface SpringJpaAvailabilityRepository extends JpaRepository<AvailabilityJpaEntity, Long> {
    List<AvailabilityJpaEntity> findAllBySpecialistUuid(UUID specialistId);
    List<AvailabilityJpaEntity> findAllByStartTimeBetweenAndState(LocalDateTime startDate, LocalDateTime endDate, int state);
    List<AvailabilityJpaEntity> findAllBySpecialistUuidAndStartTimeBetween(UUID specialistUuid, LocalDateTime startDate, LocalDateTime endDate);
    void removeBySpecialistUuidAndStartTimeBetweenAndState(UUID specialistUuid, LocalDateTime startOfWeek, LocalDateTime endOfWeek, int state);
    List<AvailabilityJpaEntity> findAllBySpecialistUuidAndState(UUID specialistUuid, int state);
    List<AvailabilityJpaEntity> findAllBySpecialistUuidAndStartTimeBetweenAndStateEquals(UUID specialistUuid, LocalDateTime startDate, LocalDateTime endDate, int state);
}
