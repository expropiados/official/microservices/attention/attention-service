package org.expropiados.cuidemonos.attentionservice.modules.patient.domain;

import java.util.HashMap;
import java.util.Map;

public final class MonthlyFormatsMap {
    private MonthlyFormatsMap(){

    }

    public static HashMap<Integer, String> getMonthlyFormats(){
        return new HashMap<Integer, String>(){{
            put(1, "Ene");
            put(2, "Feb");
            put(3, "Mar");
            put(4, "Abr");
            put(5, "May");
            put(6, "Jun");
            put(7, "Jul");
            put(8, "Ago");
            put(9, "Set");
            put(10, "Oct");
            put(11, "Nov");
            put(12, "Dec");
        }};
    }
}
