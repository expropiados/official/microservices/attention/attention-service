package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in.GetAvailabilitiesOfRangeDateWithAppointmentInfoDTO;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GetAvailabilitiesOfRangeDateWithAppointmentInfoMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "specialistUuid", target = "specialistUuid")
    @Mapping(source = "startTime", target = "startTime")
    @Mapping(source = "endTime", target = "endTime")
    @Mapping(source = "state", target = "state")
    @Mapping(source = "appointment.patient", target = "patient")
    @Mapping(source = "appointment.id", target = "sessionId")
    GetAvailabilitiesOfRangeDateWithAppointmentInfoDTO toGetAvailabilitiesOfRangeDateWithAppointmentInfoDTO(Availability availability);
    List<GetAvailabilitiesOfRangeDateWithAppointmentInfoDTO> toGetAvailabilitiesOfRangeDateWithAppointmentInfoDTOList(List<Availability> availabilities);
}
