package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.NotAuthorizedForRoleException;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.UserRole;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetPatientsOfSpecialistUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/specialists")
public class GetPatientsBySpecialistController {

    private final JwtHelper jwtHelper;
    private final GetPatientsOfSpecialistUseCase getPatientsOfSpecialistUseCase;

    @GetMapping("{uuidSpecialist}")
    public ResponseEntity<List<User>> getPatientsBySpecialist(@PathVariable UUID uuidSpecialist,
                                                              @RequestParam(defaultValue = "") String search,
                                                              @RequestHeader("Authorization") String token,
                                                              @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        if (userRole != UserRole.SPECIALIST) {
            throw new NotAuthorizedForRoleException(userRole);
        }

        var patients = getPatientsOfSpecialistUseCase.getPatientsOfSpecialist(uuidSpecialist, search);

        return ResponseEntity.ok(patients);
    }

}
