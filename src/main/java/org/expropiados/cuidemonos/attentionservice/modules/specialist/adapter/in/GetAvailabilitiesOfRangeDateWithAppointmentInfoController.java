package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in.mappers.GetAvailabilitiesOfRangeDateWithAppointmentInfoMapper;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetAvailabilitiesOfDateUseCase;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/specialists")
public class GetAvailabilitiesOfRangeDateWithAppointmentInfoController {

    private final JwtHelper jwtHelper;
    private final GetAvailabilitiesOfDateUseCase getAvailabilitiesOfDateUseCase;
    private final GetAvailabilitiesOfRangeDateWithAppointmentInfoMapper getAvailabilitiesOfRangeDateWithAppointmentInfoMapper;

    @GetMapping("/{specialistUuid}/availabilityRangeSpecialist")
    @ApiOperation("Get availabilities of range date with reserved appointments info")
    public ResponseEntity<List<GetAvailabilitiesOfRangeDateWithAppointmentInfoDTO>> GetAvailabilitiesOfRangeDateWithAppointmentInfo
                                                                                    (@PathVariable UUID specialistUuid,
                                                                                     @RequestParam(name = "dateBegin") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate dateBegin,
                                                                                     @RequestParam(name = "dateEnd") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) LocalDate dateEnd,
                                                                                     @RequestHeader("Authorization") String token,
                                                                                     @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        var availabilities = getAvailabilitiesOfDateUseCase
                .getAvailabilitiesOfRangeDate(dateBegin, dateEnd, specialistUuid);
        var availabilitiesDTO = getAvailabilitiesOfRangeDateWithAppointmentInfoMapper
                .toGetAvailabilitiesOfRangeDateWithAppointmentInfoDTOList(availabilities);

        return ResponseEntity.ok(availabilitiesDTO);
    }

}
