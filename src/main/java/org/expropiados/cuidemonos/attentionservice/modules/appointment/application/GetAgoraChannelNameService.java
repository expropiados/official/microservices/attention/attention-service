package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.GetAgoraChannelNameUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.GetVideoCallPort;

@UseCase
@RequiredArgsConstructor
public class GetAgoraChannelNameService implements GetAgoraChannelNameUseCase {

    private final GetVideoCallPort getVideoCallPort;


    @Override
    public String getAgoraNameChannelName(Long idAppointment) {
        var videoCall = getVideoCallPort.getVideoCallByIdAppointment(idAppointment);
        if(videoCall.isEmpty()) return "";
        return videoCall.get().getAgoraChannelName();
    }

}
