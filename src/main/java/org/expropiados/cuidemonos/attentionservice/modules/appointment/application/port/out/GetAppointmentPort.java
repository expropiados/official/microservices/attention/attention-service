package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public interface GetAppointmentPort {
    Optional<List<Appointment>> getAppointmentForSpecialist(UUID specialistUuid, LocalDate dateBegin, LocalDate dateEnd);

    List<Appointment> getPendentAppointmentForPatient(UUID patientUuid, LocalDate dateBegin, LocalDate dateEnd);

    Optional<Appointment> getAppointment(Long appointmentId);

    Map<LocalDateTime, Appointment> getAppointmentOfAvailability(UUID specialistUuid);

    List<Appointment> getAppointmentsInDateRangeForClientCompany(LocalDate startDate, LocalDate endDate, Long clientCompanyId);
}
