package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import lombok.Data;

@Data
public class AgoraNotificationsDetails {
    private Integer errorCode;
    private Integer errorLevel;
    private String errorMsg;
    private Integer module;
    private String msgName;
    private Integer stat;
}
