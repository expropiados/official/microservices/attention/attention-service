package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "POLL_POST_APPOINTMENT")
public class PostPollJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "poll_post_appointment_id")
    private Long idPostPoll;
    @Column(name = "appointment_id")
    private Long idAppointment;
    @Column(name = "specialist_quality")
    private Integer specialistQuality;
    @Column(name = "specialist_comments", length = 1000)
    private String specialistComments;
    @Column(name = "videocall_quality", length = 1000)
    private String videoCallQuality;
    @Column(name = "videocall_comments", length = 1000)
    private String videoCallComments;

    @OneToOne
    @JoinColumn(name = "appointment_id", insertable = false, updatable = false)
    private AppointmentJpaEntity appointmentJpaEntity;

}
