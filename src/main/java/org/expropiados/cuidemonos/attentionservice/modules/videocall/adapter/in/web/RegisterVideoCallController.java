package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisterVideoCallUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisteredVideoCall;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.RegisterVideoCallDTO;
import org.springframework.web.bind.annotation.*;


@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/video_call")
public class RegisterVideoCallController {

    private final JwtHelper jwtHelper;
    private final RegisterVideoCallUseCase registerVideoCallUseCase;

    @PostMapping("")
    public RegisterVideoCallDTO registerVideoCall(@RequestBody RegisteredVideoCall registeredVideoCall,
                                                  @RequestHeader("Authorization") String token,
                                                  @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        var channelName =  registerVideoCallUseCase.registerVideoCall(registeredVideoCall.getAppointmentId());
        return RegisterVideoCallDTO.builder().
                agoraChannelName(channelName).
                build();
    }
}
