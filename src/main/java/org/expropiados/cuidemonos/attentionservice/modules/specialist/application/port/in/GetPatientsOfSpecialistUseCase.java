package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

import java.util.List;
import java.util.UUID;

public interface GetPatientsOfSpecialistUseCase {
    List<User> getPatientsOfSpecialist(UUID specialistUuid, String search);
}
