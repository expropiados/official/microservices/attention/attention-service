package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AudioIssueMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.PostPollMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.SpecialistIssueMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.VideoIssueMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories.SpringJpaAudioIssuesRepository;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories.SpringJpaPostPollRepository;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories.SpringJpaSpecialistIssuesRepository;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories.SpringJpaVideoIssuesRepository;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetPostPollPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SavePostPollAnswersPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@PersistenceAdapter
@RequiredArgsConstructor
public class PostPollPersistenceAdapter implements SavePostPollAnswersPort, GetPostPollPort {

    private final SpringJpaPostPollRepository postPollRepository;
    private final SpringJpaAudioIssuesRepository springJpaAudioIssuesRepository;
    private final SpringJpaVideoIssuesRepository springJpaVideoIssuesRepository;
    private final SpringJpaSpecialistIssuesRepository springJpaSpecialistIssuesRepository;
    private final PostPollMapper postPollMapper;
    private final AudioIssueMapper audioIssueMapper;
    private final VideoIssueMapper videoIssueMapper;
    private final SpecialistIssueMapper specialistIssueMapper;

    @Override
    public PostPoll saveResults(PostPoll postPoll) {
        var row = postPollMapper.toPostPollEntity(postPoll);
        var rowSaved = postPollRepository.save(row);
        return postPollMapper.toPostPoll(rowSaved);
    }

    @Override
    public void saveAudioIssues(List<PostPoll> postPolls) {
        var rows = audioIssueMapper.toAudioIssueJpaEntityList(postPolls);
        springJpaAudioIssuesRepository.saveAll(rows);
    }

    @Override
    public void saveSpecialistIssues(List<PostPoll> postPolls) {
        var rows = specialistIssueMapper.toSpecialistIssueJpaEntityList(postPolls);
        springJpaSpecialistIssuesRepository.saveAll(rows);
    }

    @Override
    public void saveVideoIssues(List<PostPoll> postPolls) {
        var rows = videoIssueMapper.toVideoIssueJpaEntityList(postPolls);
        springJpaVideoIssuesRepository.saveAll(rows);
    }

    @Override
    public List<PostPoll> getPostPollBetweenDatesForSpecialist(LocalDate startDate, LocalDate endDate, UUID specialistUUID) {
        var entities = postPollRepository.
                searchPostPollBySpecialist
                        (startDate.atStartOfDay(), endDate.atStartOfDay(), specialistUUID);
        return postPollMapper.toPostPollList(entities);
    }

    @Override
    public List<PostPoll> getPostPollBetweenDates(LocalDate startDate, LocalDate endDate) {
        var entities = postPollRepository.
                searchPostPollByDateRange
                        (startDate.atStartOfDay(), endDate.atStartOfDay());
        return postPollMapper.toPostPollList(entities);
    }

    @Override
    public PostPoll getPostPollByAppointment(Long appointmentId) {
        var postPoll = postPollRepository.findByAppointmentJpaEntityIdAppointment(appointmentId);
        return postPollMapper.toPostPoll(postPoll);
    }
}
