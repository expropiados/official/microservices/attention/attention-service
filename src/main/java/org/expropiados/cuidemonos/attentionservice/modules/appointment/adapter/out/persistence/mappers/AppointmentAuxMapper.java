package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.AppointmentJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.AppointmentDTO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AppointmentAuxMapper {

        @Mapping(source = "id", target = "id")
        @Mapping(source = "startTime", target = "startTime")
        @Mapping(source = "endTime", target = "endTime")
        @Mapping(source = "state", target = "state")

        AppointmentDTO toAppointmentDTO(Appointment appointment);
        List<AppointmentDTO> toAppointmensDTO(List<Appointment> appointments);



}
