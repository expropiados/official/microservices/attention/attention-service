package org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.enums;

public enum AppointmentType {
    DATE,
    SESSION
}
