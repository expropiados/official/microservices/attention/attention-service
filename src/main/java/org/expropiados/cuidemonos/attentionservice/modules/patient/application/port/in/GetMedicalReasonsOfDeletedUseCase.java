package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MedicalReason;

import java.util.List;

public interface GetMedicalReasonsOfDeletedUseCase {
    List<MedicalReason> getMedicalReasonsOfDeleted();
}
