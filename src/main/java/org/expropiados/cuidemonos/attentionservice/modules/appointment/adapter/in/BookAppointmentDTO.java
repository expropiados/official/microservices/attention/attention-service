package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class BookAppointmentDTO {

    private UUID uuidSpecialist;
    private UUID uuidPatient;
    private Long idAvailability;

}