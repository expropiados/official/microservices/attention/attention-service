package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.attention.RecordingAppConfig;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.*;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCallEntryData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/video_call")
public class EnterVideoCallController {

    private final JwtHelper jwtHelper;
    private final ValidateEntryCallUseCase validateEntryCallUseCase;
    private final GenerateVideoCallUseCase generateVideoCallUseCase;
    private final GetVideoCallCredentialsUseCase getVideoCallCredentialsUseCase;
    private final GetRecordingDataUseCase getRecordingDataUseCase;
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterVideoCallController.class);

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @PostMapping("/entry")
    public VideoCallEntryData getVideoCallEntryData(
            @RequestBody EnterVideoCallInput enterVideoCallInput,
            @RequestHeader("Authorization") String token,
            @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        try {
            var videoCall = validateEntryCallUseCase.validateEntryCall(enterVideoCallInput);
            var videoCallEntryData = getVideoCallCredentialsUseCase.getVideoCallCredentials(videoCall, enterVideoCallInput);
            if (videoCall != null && videoCallEntryData == null){
                var result = generateVideoCallUseCase.
                        generateVideoCalling(new GenerateVideoCallInput(videoCall,
                                enterVideoCallInput.getUserUUID(), enterVideoCallInput.getUserType()));
                var recordingData = getRecordingDataUseCase.getRecordingData(result,
                        enterVideoCallInput.getUserType(), videoCall);
                videoCallEntryData = VideoCallEntryData.builder()
                        .videoCallId(videoCall.getId())
                        .initDateFormat(videoCall.getAppointment().getStartTime().format(formatter))
                        .endDateFormat(videoCall.getAppointment().getEndTime().format(formatter))
                        .appointmentId(videoCall.getAppointment().getId())
                        .recordingEntryData(recordingData)
                        .specialistName(getVideoCallCredentialsUseCase.getVideoCallSpecialistName(videoCall))
                        .patientName(getVideoCallCredentialsUseCase.getVideoCallPatientName(videoCall))
                        .token(result.getToken()).appID(result.getAppId()).userUID(result.getUserUID())
                        .patientAgoraUID(videoCall.getPatientAgoraUID())
                        .build();
            }else if (videoCall != null){
                var recordingData = getRecordingDataUseCase.getRecordingData(null,
                        enterVideoCallInput.getUserType(), videoCall);
                videoCallEntryData.setVideoCallId(videoCall.getId());
                videoCallEntryData.setInitDateFormat(videoCall.getAppointment().getStartTime().format(formatter));
                videoCallEntryData.setEndDateFormat(videoCall.getAppointment().getEndTime().format(formatter));
                videoCallEntryData.setRecordingEntryData(recordingData);
            }
            return videoCallEntryData;
        }
        catch (Exception ex){
            LOGGER.error("There was an exception in the generation of the video call");
            throw ex;
        }
    }
}
