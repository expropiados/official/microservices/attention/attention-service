package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;

import java.util.UUID;

public interface RegisterPrePollUseCase {
    void registerPrePoll(Long idAppointment, PreAppointmentPoll pool);
}