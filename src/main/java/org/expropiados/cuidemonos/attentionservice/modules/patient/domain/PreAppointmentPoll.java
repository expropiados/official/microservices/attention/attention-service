package org.expropiados.cuidemonos.attentionservice.modules.patient.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class PreAppointmentPoll {
    private Long id;
    private Long appointmentId;
    private Long medicalReasonId;
    private MedicalReason medicalReason;
    private String anotherReason;
    private String additionalInfo;
}
