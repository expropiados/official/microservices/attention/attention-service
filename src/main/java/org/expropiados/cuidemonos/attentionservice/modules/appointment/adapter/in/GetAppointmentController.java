package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.GetAppointmentUseCase;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/specialists")
public class GetAppointmentController {

    private final JwtHelper jwtHelper;
    private final GetAppointmentUseCase getAppointmentUseCase;
    private final AppointmentDTOMapper appointmentDTOMapper;

    @GetMapping("/{uuidSpecialist}/appointments")
    @ApiOperation("Get appointments for specialist")
    public ResponseEntity<List<GetAppointmentDTO>> getAppointment(@PathVariable UUID uuidSpecialist,
                                                                  @RequestParam (name = "dateBegin") @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
                                                                          LocalDate dateBegin,
                                                                  @RequestParam (name = "dateEnd") @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
                                                                              LocalDate dateEnd,
                                                                  @RequestHeader("Authorization") String token,
                                                                  @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        var appointments = getAppointmentUseCase.getAppointmentForSpecialist(uuidSpecialist, dateBegin, dateEnd);
        var appointmentsDto = appointmentDTOMapper.toAppointmentsDto(appointments);
        return ResponseEntity.ok(appointmentsDto);
    }

}

