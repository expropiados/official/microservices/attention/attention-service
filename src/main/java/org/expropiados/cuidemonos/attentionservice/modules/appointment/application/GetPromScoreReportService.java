package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.GetPromScoreReportDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in.GetPromScoreReportUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetPromScoreReport;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetPromScoreReason;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetPostPollPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.GetMedicalReasonPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.GetPrePollPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MedicalReason;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetPromScoreReportService implements GetPromScoreReportUseCase {

    public final GetAppointmentPort getAppointmentPort;
    public final GetMedicalReasonPort getMedicalReasonPort;
    public final GetPostPollPort getPostPollPort;
    public final GetPrePollPort getPrePollPort;

    @Override
    public GetPromScoreReport getPromPostPollReport(GetPromScoreReportDTO getPromScoreReportDTO) {
        var startDate = getPromScoreReportDTO.getStartDate();
        var endDate = getPromScoreReportDTO.getEndDate();

        var medicalReasons = getMedicalReasonPort.getAllMedicalReasons();

        var scoreReasonsMap = new HashMap<String, Integer>();
        var scoreDetailsMap = new HashMap<String, HashMap<Integer, Integer>>();

        var cantNotValid = 0;

        fillMaps(medicalReasons, scoreReasonsMap, scoreDetailsMap);

        var appointments = getAppointmentPort.getAppointmentsInDateRangeForClientCompany(startDate, endDate, getPromScoreReportDTO.getClientCompanyId());

        for (Appointment appointment: appointments){
            var prePoll = getPrePollPort.getPrePollByAppointment(appointment.getId());
            var postPoll = getPostPollPort.getPostPollByAppointment(appointment.getId());
            if (prePoll == null || postPoll == null){
                cantNotValid++;
            }
            else{
                var medicalReasonName = prePoll.getMedicalReason().getName();
                scoreReasonsMap.put(medicalReasonName, scoreReasonsMap.get(medicalReasonName) + postPoll.getDoctorRating());
                var aux = scoreDetailsMap.get(medicalReasonName);
                aux.put(postPoll.getDoctorRating(), aux.get(postPoll.getDoctorRating())+1);
                scoreDetailsMap.put(medicalReasonName, aux);
            }
        }

        return fillScoreData(medicalReasons, scoreDetailsMap, scoreReasonsMap, appointments.size()-cantNotValid);
    }

    private void fillMaps(List<MedicalReason> medicalReasonList, HashMap<String, Integer> scoreReasons, HashMap<String, HashMap<Integer, Integer>> scoreDetails){
        for (MedicalReason medicalReason: medicalReasonList){
            scoreReasons.put(medicalReason.getName(), 0);
            scoreDetails.put(medicalReason.getName(), fillScoreDetailsMap());
        }
    }

    private HashMap<Integer, Integer> fillScoreDetailsMap(){
        var aux = new HashMap<Integer, Integer>();
        for (var i = 1; i<=5; i++){
            aux.put(i, 0);
        }
        return aux;
    }

    private GetPromScoreReport fillScoreData(List<MedicalReason> medicalReasons, HashMap<String, HashMap<Integer, Integer>> scoreDetailsMap, HashMap<String, Integer> scoreReasonsMap, int total){
        var scoreDetails =new int[medicalReasons.size()][5];
        var scoreReasons = new double[medicalReasons.size()];
        var i = 0;

        var cantTotal = 0;

        for (MedicalReason medicalReason: medicalReasons){
            var cantPerReason = 0;
            var cantWithWeight = 0;
            for(int j = 0; j <5; j++){
                scoreDetails[i][j] = scoreDetailsMap.get(medicalReason.getName()).get(5-j);
                cantPerReason+=scoreDetails[i][j];
                cantWithWeight+=scoreDetails[i][j]*(5-j);
            }

            if (cantPerReason != 0) scoreReasons[i] = scoreReasonsMap.get(medicalReason.getName())/(double)cantPerReason;
            else scoreReasons[i] = 0.0;
            cantTotal+=cantWithWeight;
            i++;
        }

        var scoreXReasons = new ArrayList<GetPromScoreReason>(1);
        scoreXReasons.add(new GetPromScoreReason(scoreReasons));
        double avgScore;
        if (total != 0) avgScore = cantTotal/(double)total;
        else avgScore = 0;

        return GetPromScoreReport.builder()
                .scoreXReason(scoreXReasons)
                .scoreDetails(scoreDetails)
                .averageScore(avgScore).build();
    }
}
