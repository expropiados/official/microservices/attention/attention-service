package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

public interface CancelAppointmentPort {
    Appointment cancelAppointment(Long idAppointment);
}
