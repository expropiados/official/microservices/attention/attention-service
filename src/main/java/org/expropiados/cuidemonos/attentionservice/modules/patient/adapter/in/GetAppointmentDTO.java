package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in;

import lombok.Data;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;

import java.time.LocalDateTime;

@Data
public class GetAppointmentDTO {
    private Long idAppointment;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Integer state;
    private User specialist;
}
