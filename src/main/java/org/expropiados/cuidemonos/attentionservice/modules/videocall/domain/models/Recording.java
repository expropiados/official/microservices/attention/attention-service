package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Recording {
    private Long id;
    private VideoCall videoCall;
    private Integer recordingNum;
    private String folderName;
    private String resourceId;
    private String sid;
    private Integer recordingType;
}
