package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PostPoll;

public interface SavePostPollAnswersUseCase {
    PostPoll savePostPollAnswers(Long appointmentId, PostPoll postPoll, PostPollDTO postPollDTO);
}
