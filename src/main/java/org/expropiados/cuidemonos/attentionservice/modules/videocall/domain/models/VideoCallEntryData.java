package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder @Getter @Setter
public class VideoCallEntryData {
    Long videoCallId;
    String token;
    String appID;
    String initDateFormat;
    String endDateFormat;
    Integer userUID;
    String specialistName;
    String patientName;
    RecordingEntryData recordingEntryData;
    Long appointmentId;
    Integer patientAgoraUID;
}
