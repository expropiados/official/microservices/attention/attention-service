package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.GetPromScoreReportDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetPromScoreReport;

public interface GetPromScoreReportUseCase {
    GetPromScoreReport getPromPostPollReport(GetPromScoreReportDTO getPromScoreReportDTO);
}
