package org.expropiados.cuidemonos.attentionservice.modules.user.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class User {
    private UUID uuid;
    private String dni;
    private String name;
    private String fatherLastname;
    private String motherLastname;
    private String profileImageUrl;
    private String phoneNumber;
    private String email;

    public String getFullName(){
        return name+" "+fatherLastname+" "+motherLastname;
    }
}
