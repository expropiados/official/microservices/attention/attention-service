package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out;

import lombok.Data;

@Data
public class GetMonthlyGeneralReport {
    private String name;
    private double[] data;
}
