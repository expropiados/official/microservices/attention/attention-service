package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import lombok.Data;

@Data
public class PrePoolDTO {
    private Long idAppointment;
    private String reason;
    private String descriptionReason;
    private String additionalInfo;
    private String anotherReason;
}
