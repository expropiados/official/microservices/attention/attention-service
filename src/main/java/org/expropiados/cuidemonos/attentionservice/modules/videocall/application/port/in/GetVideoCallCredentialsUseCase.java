package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCallEntryData;

public interface GetVideoCallCredentialsUseCase {
    VideoCallEntryData getVideoCallCredentials(VideoCall videoCall, EnterVideoCallInput videoCallInput);
    String getVideoCallSpecialistName(VideoCall videoCall);
    String getVideoCallPatientName(VideoCall videoCall);
}
