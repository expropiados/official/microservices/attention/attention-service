package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GetMedicalReasonMonthlyFreqReport {
    private List<GetMedicalReasonLastFrequency> motivos;
    private List<GetMedicalReasonMonthly> series;
    private List<String> categories;
}
