package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RecordingInput;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.SaveRecordingUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/video_call/recording")
public class SaveRecordingController {

    private final JwtHelper jwtHelper;
    private final SaveRecordingUseCase saveRecordingUseCase;

    @PostMapping("")
    public Recording saveRecording(@RequestBody RecordingInput recordingInput,
                                   @RequestHeader("Authorization") String token,
                                   @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        return saveRecordingUseCase.saveRecord(recordingInput);
    }
}
