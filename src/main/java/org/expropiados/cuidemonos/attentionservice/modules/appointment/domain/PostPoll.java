package org.expropiados.cuidemonos.attentionservice.modules.appointment.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class PostPoll {
    private Long idPostPoll;
    private Long idAppointment;
    private Integer callRating;
    private String callComments;
    private Integer doctorRating;
    private String doctorComments;
    private String selectedAudioIssues;
    private String selectedVideoIssues;
    private String selectedDoctorIssues;
}
