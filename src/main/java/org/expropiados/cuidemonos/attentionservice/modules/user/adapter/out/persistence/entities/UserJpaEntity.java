package org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Entity
@Table(name = "GENERAL_USER", schema = "backoffice")
public class UserJpaEntity {

    @Id @Column(name = "user_id")
    private Long id;

    @Column(name = "user_uuid", nullable = false)
    private UUID uuidUser;

    @Column(name = "document", length = 12)
    private String document;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "father_lastname", length = 100)
    private String fatherLastname;

    @Column(name = "mother_lastname", length = 100)
    private String motherLastname;
}
