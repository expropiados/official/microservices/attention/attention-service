package org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in;

import java.time.LocalDate;

public interface GetMedicalReasonMonthlyReportUseCase {
    GetMedicalReasonMonthlyReport getMedicalReasonMonthlyReport(LocalDate startDate, LocalDate endDate, Long clientCompanyId);
}
