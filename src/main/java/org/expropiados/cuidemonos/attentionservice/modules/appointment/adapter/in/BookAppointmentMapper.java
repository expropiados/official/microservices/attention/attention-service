package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BookAppointmentMapper {

    @Mapping(source = "uuidPatient", target = "patientUuid")
    @Mapping(source = "uuidSpecialist", target = "specialistUuid")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "specialist", ignore = true)
    @Mapping(target = "patient", ignore = true)
    @Mapping(target = "appointmentDetail", ignore = true)
    @Mapping(target = "state", ignore = true)
    @Mapping(target = "endTime", ignore = true)
    @Mapping(target = "startTime", ignore = true)
    Appointment toAppointment(BookAppointmentDTO bookAppointmentDTO);

}
