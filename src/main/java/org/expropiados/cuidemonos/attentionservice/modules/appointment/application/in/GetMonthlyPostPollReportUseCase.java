package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetMonthlyPostPollReport;

import java.time.LocalDate;
import java.util.UUID;

public interface GetMonthlyPostPollReportUseCase {
    GetMonthlyPostPollReport getMonthlyPostPollReport(LocalDate startDate, LocalDate endDate, UUID specialistUUID);
}
