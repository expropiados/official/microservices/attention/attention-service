package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class RegisterVideoCallDTO {
    String agoraChannelName;
}
