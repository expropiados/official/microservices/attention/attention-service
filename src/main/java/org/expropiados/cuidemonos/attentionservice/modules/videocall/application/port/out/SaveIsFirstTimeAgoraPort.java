package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;

public interface SaveIsFirstTimeAgoraPort {
    VideoCall saveFirstTokenBoolean(Long id, boolean isFirstTime);
}
