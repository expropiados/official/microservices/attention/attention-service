package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface GetAvailabilitiesOfDatePort {
    List<Availability> getSpecialistsAvailabilitiesOfDate(LocalDate dateBegin, LocalDate dateEnd);
    List<Availability> getAvailabilitiesOfRangeDate(LocalDate dateBegin, LocalDate dateEnd, UUID specialistUuid);
    List<Availability> getAvailabilitiesOfRangeDateAndState(LocalDate dateBegin, LocalDate dateEnd, UUID specialistUuid, Integer state);
}
