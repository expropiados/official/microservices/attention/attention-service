package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import lombok.Data;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.PrePoolDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.ResourceOwner;

import java.util.List;

@Data
public class GetDetailAppointmentDTO {
    private Appointment appointment;
    private Long idAppointmentDetail;
    private String annotations;
    private String results;
    private ResourceOwner patientResources;
    private ResourceOwner specialistResources;
    private PrePoolDTO prePoolDTO;
}
