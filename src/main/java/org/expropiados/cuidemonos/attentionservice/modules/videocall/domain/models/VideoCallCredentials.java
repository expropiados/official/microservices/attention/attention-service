package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VideoCallCredentials {
    private String token;
    private String channel;
    private Integer userUID;
    private String appId;
}
