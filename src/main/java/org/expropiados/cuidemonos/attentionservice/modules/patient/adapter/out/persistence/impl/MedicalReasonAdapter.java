package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.mappers.MedicalReasonMapper;
import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.out.persistence.repositories.SpringJpaMedicalReasonRepository;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.GetMedicalReasonPort;
import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.MedicalReason;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class MedicalReasonAdapter implements GetMedicalReasonPort {

    public final MedicalReasonMapper medicalReasonMapper;
    public final SpringJpaMedicalReasonRepository springJpaMedicalReasonRepository;

    @Override
    public List<MedicalReason> getAllMedicalReasons() {
        var entities = springJpaMedicalReasonRepository.findAll();
        return medicalReasonMapper.toMedicalReasonList(entities);
    }

    @Override
    public List<MedicalReason> getAllDeletedMedicalReasons() {
        var rows = springJpaMedicalReasonRepository.findAllByDeleted(false);
        return medicalReasonMapper.toMedicalReasonList(rows);
    }
}
