package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in.mappers.GetAppointmentDTOMapper;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.in.GetAppointmentsOfDateUseCase;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/patients")
public class GetPendentAppointmentOfDateController {

    private final JwtHelper jwtHelper;
    private final GetAppointmentsOfDateUseCase getAppointmentsOfDateUseCase;
    private final GetAppointmentDTOMapper getAppointmentDTOMapper;

    @GetMapping("/{patientUuid}/appointments")
    public ResponseEntity<List<GetAppointmentDTO>> getPendentAppointmentOfDate(@PathVariable UUID patientUuid,
                                                                               @RequestParam (name = "dateBegin") @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
                                                                                       LocalDate dateBegin,
                                                                               @RequestParam (name = "dateEnd") @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
                                                                                           LocalDate dateEnd,
                                                                               @RequestHeader("Authorization") String token,
                                                                               @RequestHeader("role") Integer role) {

        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var appointments = getAppointmentsOfDateUseCase.getPendentAppointmentsOfDate(patientUuid, dateBegin, dateEnd);
        var appointmentsDTO = getAppointmentDTOMapper.toGetAppointmentDTOs(appointments);

        return ResponseEntity.ok(appointmentsDTO);
    }

}
