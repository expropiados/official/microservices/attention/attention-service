package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.mappers.AvailabilityMapper;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.repositories.SpringJpaAvailabilityRepository;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.DeleteAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilitiesOfDatePort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.RegisterAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.utils.Constants;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class AvailabilityPersistenceAdapter implements RegisterAvailabilityPort, GetAvailabilitiesOfDatePort, GetAvailabilityPort,
        DeleteAvailabilityPort {

    private final SpringJpaAvailabilityRepository springJpaAvailabilityRepository;
    private final AvailabilityMapper availabilityMapper;

    @Override
    public void registerAvailability(List<Availability> availabilities) {
        var availabilityRows = availabilityMapper.toAvailabilityEntityList(availabilities);
        springJpaAvailabilityRepository.saveAll(availabilityRows);
    }

    public List<Availability> getAvailabilityList(UUID specialistId){
        var avalJpa = springJpaAvailabilityRepository.findAllBySpecialistUuid(specialistId);
        return availabilityMapper.toAvailabilityList(avalJpa);
    }

    @Override
    public Map<LocalDateTime, Availability> getAvailabilityReservedListByUuidSpecialist(UUID specialistUuid) {
        var rows = springJpaAvailabilityRepository.findAllBySpecialistUuidAndState(specialistUuid,
                Constants.STATE_AVAILABILITY_RESERVED);
        return availabilityMapper.toAvailabilityList(rows)
                .stream().collect(Collectors.toMap(Availability::getStartTime, Function.identity()));
    }

    @Override
    public List<Availability> getSpecialistsAvailabilitiesOfDate(LocalDate dateBegin, LocalDate dateEnd) {

        var startOfDay = dateBegin.atStartOfDay();
        var endOfDay = dateEnd.plusDays(1L).atStartOfDay();

        var availabilityJpaRows = springJpaAvailabilityRepository
                    .findAllByStartTimeBetweenAndState(startOfDay, endOfDay, Constants.STATE_AVAILABILITY_AVAILABLE);
        return availabilityMapper.toAvailabilityList(availabilityJpaRows);

    }

    @Override
    public Optional<Availability> getAvailability(Long idAvailability) {
        var row = springJpaAvailabilityRepository.findById(idAvailability);
        return row.map(availabilityMapper::toAvailability);
    }

    @Override
    public Optional<Availability> changeStateAvailability(Long idAvailability) {
        var row = springJpaAvailabilityRepository.findById(idAvailability);
        row.ifPresent(r->{
            r.setState(Constants.STATE_AVAILABILITY_RESERVED);
            springJpaAvailabilityRepository.save(r);
        });
        return row.map(availabilityMapper::toAvailability);
    }

    @Override
    public List<Availability> getAvailabilitiesOfRangeDate(LocalDate dateBegin,LocalDate dateEnd, UUID specialistUuid) {

        var startRangeDate = dateBegin.atStartOfDay();
        var endRangeDate = dateEnd.plusDays(1L).atStartOfDay();

        var availabilitiesRow = springJpaAvailabilityRepository
                .findAllBySpecialistUuidAndStartTimeBetween(specialistUuid, startRangeDate, endRangeDate);
        return availabilityMapper.toAvailabilityList(availabilitiesRow);

    }

    @Override
    public List<Availability> getAvailabilitiesOfRangeDateAndState(LocalDate dateBegin, LocalDate dateEnd, UUID specialistUuid, Integer state) {
        var startRangeDate = dateBegin.atStartOfDay();
        var endRangeDate = dateEnd.plusDays(1L).atStartOfDay();
        var availabilities = springJpaAvailabilityRepository.
                findAllBySpecialistUuidAndStartTimeBetweenAndStateEquals(specialistUuid, startRangeDate, endRangeDate, state);
        return availabilityMapper.toAvailabilityList(availabilities);
    }

    @Override
    public void deleteAvailability(UUID uuidSpecialist, LocalDate dateBegin, LocalDate dateEnd) {
        var startOfWeek = dateBegin.atStartOfDay();
        var endOfWeek = dateEnd.plusDays(1L).atStartOfDay();
        springJpaAvailabilityRepository.removeBySpecialistUuidAndStartTimeBetweenAndState(uuidSpecialist,
                startOfWeek, endOfWeek, Constants.STATE_AVAILABILITY_AVAILABLE);
    }
}
