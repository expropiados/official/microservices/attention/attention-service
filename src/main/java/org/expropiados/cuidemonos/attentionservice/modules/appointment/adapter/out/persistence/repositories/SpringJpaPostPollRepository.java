package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.PostPollJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface SpringJpaPostPollRepository extends JpaRepository<PostPollJpaEntity, Long> {
    @Query("SELECT DISTINCT PP FROM PostPollJpaEntity PP INNER JOIN AppointmentJpaEntity A ON PP.idAppointment = A.idAppointment INNER JOIN SpecialistJpaEntity S ON S.uuid = A.specialistUUID AND S.uuid = :specialistUUID WHERE A.startTime >= :startTime AND A.endTime < :endTime")
    List<PostPollJpaEntity> searchPostPollBySpecialist(LocalDateTime startTime, LocalDateTime endTime, UUID specialistUUID);
    @Query("SELECT DISTINCT PP FROM PostPollJpaEntity PP INNER JOIN AppointmentJpaEntity A ON PP.idAppointment = A.idAppointment WHERE A.startTime >= :startTime AND A.endTime < :endTime")
    List<PostPollJpaEntity> searchPostPollByDateRange(LocalDateTime startTime, LocalDateTime endTime);

    PostPollJpaEntity findByAppointmentJpaEntityIdAppointment(Long appointmentId);
}
