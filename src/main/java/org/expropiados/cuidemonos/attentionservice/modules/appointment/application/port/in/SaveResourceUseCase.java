package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface SaveResourceUseCase {
    Resource saveLinkResource(Long idAppointmentDetail, SaveLinkResourceDTO saveLinkResourceDTO);
    Resource saveDocumentResource(Long idAppointmentDetail, MultipartFile multipartFile, Integer role);
}
