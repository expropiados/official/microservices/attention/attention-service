package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UserNotValidException extends RuntimeException{
    public UserNotValidException(){
        super("El usuario no tiene permisos para ingresar a esta sesión de videollamada");
    }
}
