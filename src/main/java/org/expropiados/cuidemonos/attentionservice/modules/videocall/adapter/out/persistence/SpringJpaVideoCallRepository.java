package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.persistence;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.entities.VideoCallJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SpringJpaVideoCallRepository extends JpaRepository<VideoCallJpaEntity, Long> {
    VideoCallJpaEntity findByAgoraChannelNameEquals(String agoraName);
    Optional<VideoCallJpaEntity> findByAppointmentId(Long idAppointment);
}
