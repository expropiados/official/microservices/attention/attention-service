package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.CancelAppointmentUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(value = "/appointments")
public class CancelAppointmentController {

    private final JwtHelper jwtHelper;
    private final CancelAppointmentUseCase cancelAppointmentUseCase;

    @DeleteMapping("/{idAppointment}/cancel")
    @ApiOperation(value = "Cancel Appointment")
    public ResponseEntity<Appointment> cancelAppointment(@PathVariable Long idAppointment,
                                                         @RequestHeader("Authorization") String token,
                                                         @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        var appointmentUpdated = cancelAppointmentUseCase.cancelAppointment(idAppointment);

        return ResponseEntity.ok(appointmentUpdated);

    }

}
