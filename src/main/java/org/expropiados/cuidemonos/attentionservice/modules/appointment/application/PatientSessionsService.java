package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.errors.AppointmentForSpecialistNotFoundError;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.PatientSessionsDTO;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.ListPatientsPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentAuxMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.PatientSessionsUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.PatientSessionsPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.mappers.PatientMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories.SpringJpaAppointmentRepository;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetPatientByUserUuidPort;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class PatientSessionsService implements PatientSessionsUseCase {

    //private final PatientSessionsPort patientSessionsPort;
    private final ListPatientsPort listPatientsPort;
    private final SpringJpaAppointmentRepository springJpaAppointmentRepository;
    private final AppointmentMapper appointmentMapper;
    private final AppointmentAuxMapper appointmentAuxMapper;
    private final GetPatientByUserUuidPort patientPort;
    @Override
    public PatientSessionsDTO patientSessions(UUID specialistUuid, UUID patientUuid) {

            var patientInfo = patientPort.getPatientByUserUuid(patientUuid);
            var sessionsJPA = springJpaAppointmentRepository.findAllByPatientUUIDEquals(patientUuid);
            var sessions = appointmentMapper.toAppointmentList(sessionsJPA);
            //System.out.println(sessions.get(0));
            var sessionsDTO = appointmentAuxMapper.toAppointmensDTO(sessions);
            PatientSessionsDTO patientSessions = new PatientSessionsDTO();
            patientSessions.setUser(patientInfo);
            patientSessions.setAppointmentDTO(sessionsDTO);
        return patientSessions;
    }



}
