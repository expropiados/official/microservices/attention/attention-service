package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.Recording;

public interface SaveRecordingPort {
    Recording saveRecording(Long videoCallId, String folderName, Integer recordingType);
}
