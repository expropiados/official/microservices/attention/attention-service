package org.expropiados.cuidemonos.attentionservice.modules.specialist.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.RegisterAvailabilityUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.DeleteAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilitiesOfDatePort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.RegisterAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.utils.Constants;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@UseCase
@RequiredArgsConstructor
public class RegisterAvailabilityService implements RegisterAvailabilityUseCase {

    private final RegisterAvailabilityPort registerAvailabilityPort;
    private final DeleteAvailabilityPort deleteAvailabilityPort;
    private final GetAvailabilityPort getAvailabilityPort;

    @Override
    public void registerAvailability(UUID specialistUuid, List<Availability> availabilities,
                                     int replicate, LocalDate dateBegin, LocalDate dateEnd) {

        if(replicate == 0){
            deleteAvailabilityPort.deleteAvailability(specialistUuid, dateBegin, dateEnd);
            var freeAvailabilities = getAvailabilityFree(availabilities);
            var completeAvailabilities = completeAvailability(freeAvailabilities, specialistUuid);
            registerAvailabilityPort.registerAvailability(completeAvailabilities);
        }
        else{
            var dateEndOfReplicateWeek = dateEnd.plusWeeks(replicate);
            deleteAvailabilityPort.deleteAvailability(specialistUuid, dateBegin, dateEndOfReplicateWeek);
            var freeAvailabilities = getAvailabilityFree(availabilities);
            var reservedAvailabilities = getAvailabilityPort.
                    getAvailabilityReservedListByUuidSpecialist(specialistUuid);
            var availabilitiesReplicate = replicateAvailability(freeAvailabilities, reservedAvailabilities,
                    specialistUuid, replicate);
            registerAvailabilityPort.registerAvailability(availabilitiesReplicate);
        }

    }

    public List<Availability> getAvailabilityFree(List<Availability> availabilities){
        ArrayList<Availability> freeAvailabilities = new ArrayList<>();
        availabilities.forEach(availability -> {
            if(availability.getState().equals(Constants.STATE_AVAILABILITY_AVAILABLE)) freeAvailabilities.add(availability);
        });
        return freeAvailabilities;
    }

    public List<Availability> replicateAvailability(List<Availability> availabilities, Map<LocalDateTime, Availability> reservedAvailabilities,
                                                    UUID specialistUuid, int replicate){

        var availabilitiesComplete = completeAvailability(availabilities,specialistUuid);
        List<Availability> availabilitiesFinal = new ArrayList<>(availabilitiesComplete);

        for(var i=1; i<=replicate; i++) {
            var availabilitiesReplicated = createReplicatedAvailability(availabilities,reservedAvailabilities,
                                                                    specialistUuid,i);
            availabilitiesFinal.addAll(availabilitiesReplicated);
        }
        return availabilitiesFinal;
    }

    public List<Availability> completeAvailability(List<Availability> availabilities, UUID specialistUuid){
        List<Availability> availabilitiesComplete = new ArrayList<>();
        for (Availability availability : availabilities) {
            availability.setSpecialistUuid(specialistUuid);
            availability.setState(Constants.STATE_AVAILABILITY_AVAILABLE);
            availabilitiesComplete.add(availability);
        }
        return availabilitiesComplete;
    }

    public List<Availability> createReplicatedAvailability(List<Availability> availabilities, Map<LocalDateTime, Availability> reservedAvailabilities,
                                                           UUID specialistUuid, int numberReplicate){
        List<Availability> availabilitiesReplicated = new ArrayList<>();
        for (Availability availability : availabilities) {
            if(availability.getState().equals(Constants.STATE_AVAILABILITY_AVAILABLE)) {
                var replicateStartTime = availability.getStartTime().plusWeeks(numberReplicate);
                var replicateEndTime = availability.getEndTime().plusWeeks(numberReplicate);
                if(!reservedAvailabilities.containsKey(replicateStartTime)) {
                    var newAvailability = new Availability();
                    newAvailability.setSpecialistUuid(specialistUuid);
                    newAvailability.setStartTime(replicateStartTime);
                    newAvailability.setEndTime(replicateEndTime);
                    newAvailability.setState(Constants.STATE_AVAILABILITY_AVAILABLE);
                    availabilitiesReplicated.add(newAvailability);
                }
            }
        }
        return availabilitiesReplicated;
    }

}
