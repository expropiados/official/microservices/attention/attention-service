package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.entities.AppointmentJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AppointmentMapper {

        @Mapping(source = "idAppointment", target = "id")
        @Mapping(source = "specialistUUID", target = "specialistUuid")
        @Mapping(source = "patientUUID", target = "patientUuid")
        @Mapping(source = "startTime", target = "startTime")
        @Mapping(source = "endTime", target = "endTime")
        @Mapping(source = "state", target = "state")
        @Mapping(target = "patient", ignore = true)
        @Mapping(target = "specialist", ignore = true)
        @Mapping(target = "appointmentDetail", ignore = true)
        @Mapping(target = "isCancellable", ignore = true)
        Appointment toAppointment(AppointmentJpaEntity appointmentJpaEntity);
        List<Appointment> toAppointmentList(List<AppointmentJpaEntity> appointmentJpaEntityList);

        @InheritInverseConfiguration
        AppointmentJpaEntity toAppointmentJpaEntity(Appointment appointment);



}
