package org.expropiados.cuidemonos.attentionservice.modules.videocall.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.attention.VideocallingAppConfig;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.GenerateVideoCallInput;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.GenerateVideoCallUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveTokenPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.util.RtcTokenBuilder;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCallCredentials;

import java.time.LocalDateTime;
import java.time.ZoneId;

@UseCase
@RequiredArgsConstructor
public class GenerateVideoCallService implements GenerateVideoCallUseCase {

    public static final Integer IS_SPECIALIST = 1;
    public final SaveTokenPort saveTokenPort;
    private final VideocallingAppConfig videocallingAppConfig;

    @Override
    public VideoCallCredentials generateVideoCalling(GenerateVideoCallInput generateVideoCallInput) {
        var token = new RtcTokenBuilder();
        var agoraRole = checkAgoraRole(generateVideoCallInput.getUserType());

        var videoCall = generateVideoCallInput.getVideoCall();

        var duration = setVideoCallDurationRange(videoCall.getAppointment().getEndTime());
        var appId = videocallingAppConfig.getId();
        var appCertificate = videocallingAppConfig.getCertificate();

        var uid = (generateVideoCallInput.getUserType() == 1 ? videoCall.getSpecialistAgoraUID(): videoCall.getPatientAgoraUID());

        String result = token.buildTokenWithUid(appId, appCertificate,
                videoCall.getAgoraChannelName(), uid, agoraRole, duration);

        var videoCallCredentials = VideoCallCredentials.builder()
                .token(result)
                .channel(videoCall.getAgoraChannelName())
                .appId(appId)
                .userUID(uid)
                .build();
        saveTokenPort.saveToken(videoCall.getId(),
                videoCallCredentials.getToken(), generateVideoCallInput.getUserType());
        return videoCallCredentials;
    }

    private RtcTokenBuilder.Role checkAgoraRole(Integer agoraRole){
        if(agoraRole.equals(IS_SPECIALIST)) return RtcTokenBuilder.Role.ROLE_PUBLISHER;
        else return RtcTokenBuilder.Role.ROLE_SUBSCRIBER;
    }

    //Getting duration range in token in seconds
    private int setVideoCallDurationRange(LocalDateTime endDate){
        long endSeconds = endDate.atZone(ZoneId.of("EST", ZoneId.SHORT_IDS)).toInstant().getEpochSecond();
        long nowSeconds= LocalDateTime.now().atZone(ZoneId.of("EST", ZoneId.SHORT_IDS)).toInstant().getEpochSecond();
        return (int) ((nowSeconds) + (endSeconds-nowSeconds));
    }
}
