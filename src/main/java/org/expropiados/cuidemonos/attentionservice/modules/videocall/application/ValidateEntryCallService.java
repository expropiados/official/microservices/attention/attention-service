package org.expropiados.cuidemonos.attentionservice.modules.videocall.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.config.attention.VideocallingAppConfig;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetPatientByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetSpecialistByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.EnterVideoCallInput;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.GetVideoCallCredentialsUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.ValidateEntryCallUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.GetVideoCallPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.exception.InvalidDateEntryException;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.exception.UserNotValidException;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.exception.VideoCallNotFoundException;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCallEntryData;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

@UseCase
@RequiredArgsConstructor
public class ValidateEntryCallService implements ValidateEntryCallUseCase, GetVideoCallCredentialsUseCase {

    public final GetVideoCallPort getVideoCallPort;
    public final GetSpecialistByUserUuidPort getSpecialistByUserUuidPort;
    public final GetPatientByUserUuidPort getPatientByUserUuidPort;
    private final VideocallingAppConfig videocallingAppConfig;

    @Override
    public VideoCall validateEntryCall(EnterVideoCallInput enterVideoCallInput) {
        var videoCall = getVideoCallPort.getVideoCallByChannelName(enterVideoCallInput.getAgoraName());
        if (videoCall == null)
            throw new VideoCallNotFoundException();

        if(!checkValidInput(enterVideoCallInput, videoCall)){
            throw new UserNotValidException();
        }

        if(!checkValidDate(videoCall)){
            var startDate = videoCall.getAppointment().getStartTime();
            var endDate = videoCall.getAppointment().getEndTime();
            throw new InvalidDateEntryException(startDate, endDate, checkBeforeOrAfterDate(endDate));
        }
        return videoCall;
    }

    @Override
    public VideoCallEntryData getVideoCallCredentials(VideoCall videoCall, EnterVideoCallInput videoCallInput) {
        if (videoCall == null) return null;
        var isSpecialist = videoCallInput.getUserType().equals(1);
        String token;
        Integer userUID;

        var specialist = getSpecialistByUserUuidPort.getSpecialistByUserUuid(videoCall.getAppointment().getSpecialistUuid());
        var patient = getPatientByUserUuidPort.getPatientByUserUuid(videoCall.getAppointment().getPatientUuid());

        if (specialist.isEmpty() || patient.isEmpty()) return null;

        if (isSpecialist && videoCall.getSpecialistToken() != null){
            token = videoCall.getSpecialistToken();
            userUID = videoCall.getSpecialistAgoraUID();
        }
        else if (!isSpecialist && videoCall.getPatientToken() != null){
            token = videoCall.getPatientToken();
            userUID = videoCall.getPatientAgoraUID();
        }else return null;

        var appId = videocallingAppConfig.getId();
        return VideoCallEntryData.builder().specialistName(specialist.get().getFullName())
                .patientName(patient.get().getFullName()).appointmentId(videoCall.getAppointment().getId())
                .token(token).appID(appId).userUID(userUID).patientAgoraUID(videoCall.getPatientAgoraUID()).build();
    }

    @Override
    public String getVideoCallSpecialistName(VideoCall videoCall) {
        var specialist = getSpecialistByUserUuidPort.getSpecialistByUserUuid(videoCall.getAppointment().getSpecialistUuid());
        if (specialist.isEmpty()) return null;
        return specialist.get().getFullName();
    }

    @Override
    public String getVideoCallPatientName(VideoCall videoCall) {
        var patient = getPatientByUserUuidPort.getPatientByUserUuid(videoCall.getAppointment().getPatientUuid());
        if (patient.isEmpty()) return null;
        return patient.get().getFullName();
    }

    private boolean checkValidInput(EnterVideoCallInput enterVideoCallInput, VideoCall videoCall){
        if (enterVideoCallInput.getUserType() == 1)
            return (enterVideoCallInput.getUserUUID().equals(videoCall.getAppointment().getSpecialistUuid()));
        else
            return (enterVideoCallInput.getUserUUID().equals(videoCall.getAppointment().getPatientUuid()));
    }

    private boolean checkValidDate(VideoCall videoCall){
        var entryDate = LocalDateTime.now(ZoneOffset.UTC).minusHours(5);
        if(entryDate.isAfter(videoCall.getAppointment().getEndTime()))
            return false;
        else return !entryDate.isBefore(videoCall.getAppointment().getStartTime());
    }

    private String checkBeforeOrAfterDate(LocalDateTime endDate){
        var entryDate = LocalDateTime.now(ZoneOffset.UTC).minusHours(5);
        if(entryDate.isAfter(endDate))
            return "El tiempo de sesión de la videollamada ya concluyó";
        else
            return "La videollamada aún no inicia, aún no es posible el acceso";
    }


}
