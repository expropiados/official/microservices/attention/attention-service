package org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.repositories.SpringJpaSpecialistRepository;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.RegisterRatingForSpecialistPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

@PersistenceAdapter
@RequiredArgsConstructor
public class SpecialistPersistenceAdapter implements RegisterRatingForSpecialistPort {

    private final Logger logger = LoggerFactory.getLogger(SpecialistPersistenceAdapter.class);
    private final SpringJpaSpecialistRepository specialistRepository;

    @Override
    public void registerRatingForSpecialist(UUID specialistUserUuid, Integer rating) {
        var optSpecialist = specialistRepository.findByUser_UuidUser(specialistUserUuid);
        if (optSpecialist.isEmpty()) {
            logger.warn("Cannot register new rating because specialist with user UUID {} was not found!", specialistUserUuid.toString());
            return;
        }

        var specialist = optSpecialist.get();
        var totalRating = specialist.getTotalRating();
        var totalAppointments = specialist.getTotalAppointments();

        specialist.setTotalAppointments(totalAppointments + 1);
        specialist.setTotalRating(totalRating + rating);
    }
}
