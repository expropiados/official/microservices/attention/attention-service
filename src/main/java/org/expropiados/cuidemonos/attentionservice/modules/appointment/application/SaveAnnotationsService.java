package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveAnnotationsUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveAnnotationsPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.GetVideoCallPort;

@UseCase
@RequiredArgsConstructor
public class SaveAnnotationsService implements SaveAnnotationsUseCase {

    private final GetVideoCallPort getVideoCallPort;
    private final SaveAnnotationsPort saveAnnotationsPort;

    @Override
    public void saveAnnotations(Long idVideoCall, String annotations) {

        var videoCall = getVideoCallPort.getVideoCall(idVideoCall);

        saveAnnotationsPort.saveAnnotations(videoCall.getAppointment().getId(), annotations);

    }

}
