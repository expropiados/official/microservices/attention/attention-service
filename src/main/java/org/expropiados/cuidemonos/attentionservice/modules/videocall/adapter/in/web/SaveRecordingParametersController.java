package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RecordingParametersInput;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.SaveRecordingParametersUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.exception.VideoCallNotFoundException;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.SaveRecordingParametersDTO;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/video_call/recording")
public class SaveRecordingParametersController {

    private final JwtHelper jwtHelper;
    private final SaveRecordingParametersUseCase saveRecordingParametersUseCase;
    private static final Logger LOGGER = LoggerFactory.getLogger(SaveRecordingParametersController.class);

    @PostMapping("/parameters")
    public SaveRecordingParametersDTO saveRecordingParameters(@RequestBody RecordingParametersInput recordingParametersInput,
                                                              @RequestHeader("Authorization") String token,
                                                              @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);

        try{
            var videoCall =  saveRecordingParametersUseCase.saveFirstTimeAgora(recordingParametersInput);
            var recording = saveRecordingParametersUseCase.saveRecordingParameters(recordingParametersInput);
            return SaveRecordingParametersDTO.builder()
                    .videoCall(videoCall).recording(recording).build();
        }catch (VideoCallNotFoundException ex){
            LOGGER.error("There was an exception in the saving of the parameters for the video call");
            throw ex;
        }
    }
}
