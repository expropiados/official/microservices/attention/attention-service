package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.mappers.AvailabilityMapper;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetAvailabilityUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/specialists")
public class GetAvailabilityController {

    private final JwtHelper jwtHelper;
    private final GetAvailabilityUseCase getAvailabilityUseCase;
    private final AvailabilityMapper availabilityMapper;
    @GetMapping("/{uuidSpecialist}/availability")
    @ApiOperation("Get availability from specialist")
    public ResponseEntity<List<Availability>> getAvailabilityList(@PathVariable UUID uuidSpecialist,
                                                                  @RequestHeader("Authorization") String token,
                                                                  @RequestHeader("role") Integer role) {
        var tokenBody = jwtHelper.decodeToken(token);
        var userRole = jwtHelper.getRole(tokenBody, role);
        var aval = getAvailabilityUseCase.getAvailability(uuidSpecialist);
        return  ResponseEntity.ok(aval);
    }




}
