package org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisteredVideoCall;

public interface RegisterVideoCallPort {
    String registerVideoCall(RegisteredVideoCall registeredVideoCall, Appointment appointment);
}
