package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.attentionservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.repositories.SpringJpaAppointmentRepository;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.utils.Constants;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.BookAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.CancelAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetPatientByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.PatientSessionsPort;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class PatientSessionPersistentAdapter {

    private final SpringJpaAppointmentRepository springJpaAppointmentRepository;
    private final AppointmentMapper appointmentMapper;




}
