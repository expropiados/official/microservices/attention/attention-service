package org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class VideoCall {
    private Long id;
    private Appointment appointment;
    private UUID videoCallUuid;
    private Integer specialistAgoraUID;
    private Integer patientAgoraUID;
    private String agoraChannelName;
    private String specialistToken;
    private String patientToken;
    private String agoraTokenRecorder;
    private boolean firstTimeAgoraTokenRecorder;
    private LocalDateTime createdAt;
}
