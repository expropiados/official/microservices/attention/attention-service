package org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in;

import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
@Builder
public class GetHoursReport {
    private List<GetMonthlyGeneralReport> seriesLine;
}
