package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

public interface SaveAnnotationsUseCase {
    void saveAnnotations(Long idVideoCall, String annotations);
}
