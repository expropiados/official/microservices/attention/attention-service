package org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in;

public interface FinalizeAppointmentUseCase {
    void finalizeAppointment();
}
