package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.mappers;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.entities.AvailabilityJpaEntity;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AvailabilityMapper {

    @Mapping(source = "idAvailability", target = "id")
    @Mapping(source = "specialistUuid", target = "specialistUuid")
    @Mapping(source = "startTime", target = "startTime")
    @Mapping(source = "endTime", target = "endTime")
    @Mapping(source = "state", target = "state")
    @Mapping(target = "appointment", ignore = true)
    Availability toAvailability(AvailabilityJpaEntity availabilityJpaEntity);
    List<Availability> toAvailabilityList(List<AvailabilityJpaEntity> availabilityJpaEntities);

    @InheritInverseConfiguration
    AvailabilityJpaEntity toAvailabilityEntity(Availability availability);
    List<AvailabilityJpaEntity> toAvailabilityEntityList(List<Availability> availabilities);

}
