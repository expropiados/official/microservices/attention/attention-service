package org.expropiados.cuidemonos.attentionservice.modules.patient.adapter.in;

import org.expropiados.cuidemonos.attentionservice.modules.patient.domain.PreAppointmentPoll;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PreApoointmentPollMapper {
    @Mapping(source = "idMedicalReason", target = "medicalReasonId")
    @Mapping(source = "additionalInfo", target = "additionalInfo")
    @Mapping(source = "anotherReason", target = "anotherReason")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "appointmentId", ignore = true)
    PreAppointmentPoll toPrePoll(PreAppointmentPollDTO preAppointmentPollDTO);
}
