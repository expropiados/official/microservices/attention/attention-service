package org.expropiados.cuidemonos.attentionservice.modules.appointment.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Resource {
    private Long resourceId;
    private Long appointmentDetailId;
    private String name;
    private Integer type;
    private String url;
    private Integer owner;
}
