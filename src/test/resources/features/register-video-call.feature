Feature: Register a video call
  Scenario: Registering a video call successfully
    Given a specific session UUID
    When the video call is registered
    Then the user gets the name of the video call session
