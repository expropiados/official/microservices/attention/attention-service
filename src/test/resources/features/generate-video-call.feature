Feature: Generate a video call
  Scenario: Generate a video call successfully
    Given the right input for a video call
    When the video call generates
    Then it returns the token and the app id
