TRUNCATE table appointment CASCADE;

INSERT INTO appointment
    (appointment_id,end_time, start_time, state, patient_uuid, specialist_uuid)
VALUES (1, '2021-05-30 13:00:00', '2021-05-30 15:00:00', 1, '7d09d7ff-2bda-4d99-91d1-5ecb7d1a9499', '31ea6b7b-ed28-4699-853c-8447da1048d3');

INSERT INTO appointment
    (appointment_id, end_time, start_time, state, patient_uuid, specialist_uuid)
VALUES (2, '2021-05-30 15:00:00', '2021-05-30 17:00:00', 1, 'f540dc56-a422-4504-a441-603fe0ae1b9a', '83a74bce-52ed-42c9-a774-86d5b6543dcd');

INSERT INTO appointment
    (appointment_id, end_time, start_time, state, patient_uuid, specialist_uuid)
VALUES (3, '2021-05-30 17:00:00', '2021-05-30 19:00:00', 1, '82a5fe27-f2bf-46e2-83c2-7beb1af139b3', 'a1f85f70-95cb-4660-9063-b2a75fe159cd');

COMMIT;
