TRUNCATE table appointment CASCADE;

INSERT INTO appointment
    (appointment_id,end_time, start_time, state, patient_uuid, specialist_uuid)
VALUES (1, '2021-05-30 13:00:00', '2021-05-30 15:00:00', 1, '7d09d7ff-2bda-4d99-91d1-5ecb7d1a9499', '31ea6b7b-ed28-4699-853c-8447da1048d3');

TRUNCATE table appointment_result CASCADE;

INSERT INTO appointment_result (appointment_result_id, annotations, appointment_id, results)
VALUES (1, null, 1,  null);