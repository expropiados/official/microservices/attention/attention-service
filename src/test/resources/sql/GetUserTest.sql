TRUNCATE TABLE backoffice.general_user CASCADE;
INSERT INTO backoffice.general_user(user_id, user_uuid, document, father_lastname, mother_lastname, name)
VALUES (1, '6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b', '77208434', 'Fernandez', 'Murga',
            'Gianmarco'),
        (2, '56e896a8-aa21-4ff3-9f8a-a652e9474ba4', '66524258', 'Quezada', 'Aquino', 'Marco');

TRUNCATE TABLE backoffice.specialist CASCADE;
INSERT INTO backoffice.specialist(address, email, is_blocked, phone_number, profile_image, user_id, uuid)
VALUES('Mz. Lote 15 Urb Los Pinos', 'm.quezada@pucp.edu.pe', false, '986547123',
       'https://videollamada.s3.amazonaws.com/marcoQ.PNG', 2, '56e896a8-aa21-4ff3-9f8a-a652e9474ba4');

TRUNCATE TABLE backoffice.patient CASCADE;
INSERT INTO backoffice.patient(address, email, is_blocked, phone_number, profile_image, user_id, uuid)
VALUES('Mz lote 20 Urb El Alamo','steven.fernandez@pucp.edu.pe',false, '987654321',
       'https://videollamada.s3.amazonaws.com/gm.PNG',1, '6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b');