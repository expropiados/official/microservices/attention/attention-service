TRUNCATE TABLE general_user CASCADE;
INSERT INTO general_user(user_id, document, type_document, father_lastname, mother_lastname, name, user_uuid)
VALUES (1, '77208434', 0,'Fernandez', 'Murga',
        'Gianmarco', '6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b'),
       (2, '66524258', 0, 'Quezada', 'Aquino', 'Marco', '56e896a8-aa21-4ff3-9f8a-a652e9474ba4');

TRUNCATE TABLE availability CASCADE;
INSERT INTO availability(availability_id, end_time, specialist_uuid, start_time, state)
VALUES (1,'2021-06-08 20:32:53.842522', '56e896a8-aa21-4ff3-9f8a-a652e9474ba4', '2021-06-08 20:02:53.842522',0),
       (2,'2021-06-08 19:32:53.842522', '56e896a8-aa21-4ff3-9f8a-a652e9474ba4', '2021-06-08 19:32:53.842522',0),
       (3,'2021-06-08 18:32:53.842522', '56e896a8-aa21-4ff3-9f8a-a652e9474ba4', '2021-06-08 18:32:53.842522',0),
       (4,'2021-06-08 15:32:53.842522', '56e896a8-aa21-4ff3-9f8a-a652e9474ba4', '2021-06-08 15:32:53.842522',1),
       (5,'2021-06-10 18:32:53.842522', '56e896a8-aa21-4ff3-9f8a-a652e9474ba4', '2021-06-10 18:32:53.842522',0),
       (6,'2021-06-08 17:32:53.842522', '6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b', '2021-06-08 17:32:53.842522',0),
       (7,'2021-06-08 16:32:53.842522', '6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b', '2021-06-08 16:32:53.842522',0),
       (8,'2021-06-10 17:32:53.842522', '6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b', '2021-06-10 17:32:53.842522',0);

