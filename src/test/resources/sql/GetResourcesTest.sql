TRUNCATE TABLE appointment CASCADE;
INSERT INTO appointment(appointment_id, end_time, patient_uuid, specialist_uuid, start_time, state)
VALUES (1, '2021-06-09 05:37:55.804038', '6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b',
        '56e896a8-aa21-4ff3-9f8a-a652e9474ba4', '2021-06-09 05:07:55.804038',0);

TRUNCATE TABLE appointment_result CASCADE;
INSERT INTO appointment_result (appointment_result_id, annotations, appointment_id, results)
VALUES (1, 'Todo Ok', 1,  'Todo Ok');

TRUNCATE TABLE resource CASCADE;
INSERT INTO resource(resource_id,appointment_result_id, name, owner, type, url)
VALUES (1, 1 , 'Instagram', 1, 0 ,'https://www.instagram.com/?hl=es-la'),
       (2, 1, 'Agua.xlsx', 1, 1, 'https://s3.us-east-2.amazonaws.com/expropiados-s3/documents/%2FAgua.xlsx');