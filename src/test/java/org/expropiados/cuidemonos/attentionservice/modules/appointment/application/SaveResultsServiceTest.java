package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveResultsPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class SaveResultsServiceTest {

    private final SaveResultsPort saveResultsPort = Mockito.mock(SaveResultsPort.class);

    private final SaveResultsService saveResultsService = new SaveResultsService(saveResultsPort);

    private DetailAppointment appointmentDetail;

    @BeforeEach
    public void init(){
        appointmentDetail = DetailAppointment.builder().appointmentDetailId(1L).results("Esto es una prueba").build();
    }

    @Test
    void saveResultsSuccess(){

        var idAppointmentDetail = 1L;
        var results = "Esto es una prueba";

        when(saveResultsPort.saveResults(idAppointmentDetail, results)).thenReturn(appointmentDetail);

        var result = saveResultsService.saveResults(idAppointmentDetail, results);

        assertThat(result.getAppointmentDetailId()).isEqualTo(appointmentDetail.getAppointmentDetailId());
        assertThat(result.getResults()).isEqualTo(appointmentDetail.getResults());

    }

}
