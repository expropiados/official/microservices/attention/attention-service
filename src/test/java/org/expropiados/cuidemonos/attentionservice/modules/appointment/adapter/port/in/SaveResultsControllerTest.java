package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.in;


import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.SaveResultsController;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveResultsUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = SaveResultsController.class)
class SaveResultsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JwtHelper jwtHelper;

    @MockBean
    private SaveResultsUseCase saveResultsUseCase;

    @Test
    void saveResultsSuccess() throws Exception{

        var result = "Esto es una prueba";
        var idAppointmentResult = 1L;

        String params = "{ \"results\": \""+result+"\"}";

        mockMvc.perform(post("/appointments/{sessionId}/results", idAppointmentResult)
                .content(params)
                .header("Content-Type", "application/json"))
                .andExpect(status().is(400));

    }
}
