package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.in;

import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.BookAppointmentController;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.BookAppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.BookAppointmentUseCase;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.mappers.VideoCallMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Random;
import java.util.UUID;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = BookAppointmentController.class)
@ComponentScan(basePackageClasses = {JwtHelper.class})
class BookAppointmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JwtHelper jwtHelper;

    @MockBean
    private BookAppointmentUseCase bookAppointmentUseCase;


    @Test
    void bookAppointmentSuccess() throws Exception{


        var id = 1L;
        var patientId = UUID.randomUUID();
        var specialistId = UUID.randomUUID();

        String params = "{ \"uuidSpecialist\": \""+specialistId+"\""
                +", \"uuidPatient\": \""+patientId+"\""
                +", \"id\": \""+id+"\"}";

        mockMvc.perform(post("/appointments").
                content(params).
                header("Content-Type", "application/json")).andExpect(status().is(400));

    }

}
