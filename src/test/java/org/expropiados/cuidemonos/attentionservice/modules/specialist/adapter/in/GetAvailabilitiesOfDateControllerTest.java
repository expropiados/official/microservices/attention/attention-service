package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.in.mappers.GetAvailabilitiesOfDateMapper;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.in.GetAvailabilitiesOfDateUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.UUID;

@Tag("IntegrationTest")
@WebMvcTest(controllers = GetAvailabilitiesOfDateController.class)
@ComponentScan(basePackageClasses = {GetAvailabilitiesOfDateMapper.class})
@Disabled("disabled for JWT Helper")
class GetAvailabilitiesOfDateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GetAvailabilitiesOfDateUseCase getAvailabilitiesOfDateUseCase;

    @Test
    void getAvailabilitiesOfDateSuccess() throws Exception{

        var dateBegin = LocalDate.of(2021,6, 9);
        var dateEnd = LocalDate.of(2021,6, 15);

        mockMvc.perform(get("/specialists/availability")
                .param("dateBegin", String.valueOf(dateBegin))
                .param("dateEnd", String.valueOf(dateEnd))
        ).andExpect(status().isOk());

        then(getAvailabilitiesOfDateUseCase).should().getSpecialistsAvailabilitiesOfRangeDate(dateBegin, dateEnd);
    }

}
