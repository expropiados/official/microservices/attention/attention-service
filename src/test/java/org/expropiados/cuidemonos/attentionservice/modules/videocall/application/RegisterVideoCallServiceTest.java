package org.expropiados.cuidemonos.attentionservice.modules.videocall.application;

import org.expropiados.cuidemonos.attentionservice.config.LocalDateTimePeruZone;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisteredVideoCall;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.RegisterVideoCallPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.util.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

@Tag("UnitTest")
class RegisterVideoCallServiceTest {

    private final RegisterVideoCallPort registerVideoCallPort = Mockito.mock(RegisterVideoCallPort.class);
    private final GetAppointmentPort getAppointmentPort = Mockito.mock(GetAppointmentPort.class);
    private final RegisterVideoCallService registerVideoCallService =
            new RegisterVideoCallService(registerVideoCallPort, getAppointmentPort);
    private RegisteredVideoCall registeredVideoCall;
    private Appointment appointment;

    @BeforeEach
    public void init(){
        registeredVideoCall = new RegisteredVideoCall(
                new RandomString().nextString(),
                new RandomString().randomIntegerValue(),
                new RandomString().randomIntegerValue(),
                1L);
        appointment = Appointment.builder().id(1L).specialistUuid(UUID.randomUUID())
                .patientUuid(UUID.randomUUID()).startTime(LocalDateTimePeruZone.now())
                .endTime(LocalDateTimePeruZone.now().plusHours(2)).state(1).build();
    }

    @Test
    void registerVideoCallSuccess(){
        var uuid = registeredVideoCall.getAppointmentId();
        when(registerVideoCallPort.registerVideoCall(any(RegisteredVideoCall.class), any(Appointment.class))).thenReturn(registeredVideoCall.getAgoraChannelName());
        when(getAppointmentPort.getAppointment(1L)).thenReturn(Optional.of(appointment));
        var result = registerVideoCallService.registerVideoCall(uuid);

        assertThat(result).isEqualTo(registeredVideoCall.getAgoraChannelName());
    }
}
