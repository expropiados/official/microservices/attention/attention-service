package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.out.persistence;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.impl.AppointmentDetailPersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentDetailMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({AppointmentDetailPersistenceAdapter.class})
@ComponentScan(basePackageClasses = {AppointmentDetailMapper.class})
@ContextConfiguration
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Sql({"classpath:sql/GetDetailAppointmentTest.sql"})
@Disabled("Disable because of but sql")
class AppointmentDetailPersistenceAdapterTest {

    @Autowired
    private AppointmentDetailPersistenceAdapter appointmentDetailPersistenceAdapter;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Test
    void getDetailAppointmentSuccess(){

        var idAppointment = 1L;

        var detailAppointment = appointmentDetailPersistenceAdapter.getDetailAppointment(idAppointment);

        assertThat(detailAppointment.getAnnotations()).isEqualTo("Todo Ok");
        assertThat(detailAppointment.getAppointmentDetailId()).isEqualTo(1L);
        assertThat(detailAppointment.getAppointment().getId()).isEqualTo(1L);

        assertThat(detailAppointment.getAppointment().getStartTime().format(formatter)).isEqualTo("2021-06-09 05:07:55");
        assertThat(detailAppointment.getAppointment().getEndTime().format(formatter)).isEqualTo("2021-06-09 05:37:55");
        assertThat(detailAppointment.getAppointment().getPatientUuid())
                .isEqualTo(UUID.fromString("6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b"));
        assertThat(detailAppointment.getAppointment().getSpecialistUuid())
                .isEqualTo(UUID.fromString("56e896a8-aa21-4ff3-9f8a-a652e9474ba4"));

    }

    @Test
    @Sql({"classpath:sql/SaveResultsTest.sql"})
    void saveResultsSuccess(){

        var results = "Esto es una prueba";

        var resultsUpdated = appointmentDetailPersistenceAdapter.saveResults(1L, results);

        assertThat(resultsUpdated.getResults()).isEqualTo(results);

    }



}
