package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.persistence;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.impl.VideoCallPersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.mappers.VideoCallMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Tag("IntegrationTest")
@DataJpaTest
@Import({VideoCallPersistenceAdapter.class})
@ContextConfiguration
@ComponentScan(basePackageClasses = {VideoCallMapper.class, AppointmentMapper.class})
@ActiveProfiles("test")
@Sql({"classpath:sql/SaveAppointmentTest.sql", "classpath:sql/SaveVideoCallTest.sql"})
@Disabled("Disable because of but sql")
class SaveTokenPersistenceTest {

    @Autowired
    private VideoCallPersistenceAdapter videoCallPersistenceAdapter;

    @Autowired
    private SpringJpaVideoCallRepository videoCallRepository;

    @Test
    void saveTokenSpecialistSuccess(){
        videoCallPersistenceAdapter.saveToken(1L,
                                "0063aa2abbbfb17466395c87626637ae2deIACQXqn8a4leBqlDjm+uEZ55tNzEUe9Zj83M9X+LsV/HiHH3mfjSY0iIIgCEC01jgfurYAQAAQA9qqpgAgA9qqpgAwA9qqpgBAA9qqpg",
                                1);
        var entity =videoCallRepository.findById(1L);
        assertThat(entity).isPresent();
        assertThat(entity.get().getSpecialistToken()).isNotNull();

    }

    @Test
    void saveTokenStudentSuccess(){
        videoCallPersistenceAdapter.saveToken(1L,
                "0233aa2abbbfb17466395c87626637sk4meIACQXqn8a4leBqlDjm+uEZ55tNzEUe9Zj83M9X+LsV/HiHH3mfjSY0iIIgCEC01jgfurYAQAAQA9qqpgAgA9qqpgAwA9qqpgBAA9qqpg",
                2);
        var entity =videoCallRepository.findById(1L);
        assertThat(entity).isPresent();
        assertThat(entity.get().getPatientToken()).isNotNull();
    }
}
