package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.persistence;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.impl.VideoCallPersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.mappers.VideoCallMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({VideoCallPersistenceAdapter.class})
@ComponentScan(basePackageClasses = {VideoCallMapper.class, AppointmentMapper.class})
@ContextConfiguration
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Disabled("Disable because of but sql")
class VideoCallPersistenceAdapterTest {

    @Autowired
    private VideoCallPersistenceAdapter videoCallPersistenceAdapter;

    @Test
    @Sql("classpath:sql/GetVideoCallByIdAppointmentTest.sql")
    void getVideoCallByIdAppointmentSuccess(){

        Long idAppointment = 1L;
        var videoCall = videoCallPersistenceAdapter.getVideoCallByIdAppointment(idAppointment);

        if(videoCall.isPresent()) {
            assertThat(videoCall.get().getAgoraChannelName()).isEqualTo("Cita 1");
            assertThat(videoCall.get().getId()).isEqualTo(1L);
            assertThat(videoCall.get().getPatientAgoraUID()).isEqualTo(2);
            assertThat(videoCall.get().getSpecialistAgoraUID()).isEqualTo(2);
            assertThat(videoCall.get().getAppointment().getId()).isEqualTo(1L);
        }

    }

}
