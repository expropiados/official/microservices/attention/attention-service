package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.in;

import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.CancelAppointmentController;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.CancelAppointmentUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = CancelAppointmentController.class)
class CancelAppointmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JwtHelper jwtHelper;

    @MockBean
    private CancelAppointmentUseCase cancelAppointmentUseCase;

    @Test
    void cancelAppointmentSuccess() throws Exception{

        var idAppointment = 1L;

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/appointments/{idAppointment}/cancel", idAppointment)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }

}
