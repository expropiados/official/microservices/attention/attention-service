package org.expropiados.cuidemonos.attentionservice.modules.videocall.application;

import org.expropiados.cuidemonos.attentionservice.config.LocalDateTimePeruZone;
import org.expropiados.cuidemonos.attentionservice.config.attention.VideocallingAppConfig;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.GenerateVideoCallInput;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.SaveTokenPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;

@Tag("UnitTest")
@Disabled("Disable until a better way to test third party code like Agora util classes without using env variables!")
class GenerateVideoCallServiceTest {


    private final SaveTokenPort saveTokenPort = Mockito.mock(SaveTokenPort.class);
    private final VideocallingAppConfig videocallingAppConfig = new VideocallingAppConfig();
    private final GenerateVideoCallService generateVideoCallService = new GenerateVideoCallService(saveTokenPort, videocallingAppConfig);
    private VideoCall videoCall;

    @BeforeEach
    void init(){
        Appointment appointment = Appointment.builder().id(1L).specialistUuid(UUID.randomUUID())
                .patientUuid(UUID.randomUUID()).startTime(LocalDateTime.now().minusHours(2))
                .endTime(LocalDateTime.now()).state(1).build();
        videoCall = VideoCall.builder()
                .id(1L).appointment(appointment)
                .specialistAgoraUID(12345678).patientAgoraUID(87654321)
                .agoraChannelName("ksdme2nfi39l").createdAt(LocalDateTimePeruZone.now()).build();
    }

    @Test
    void generateVideoCallSuccess(){
        GenerateVideoCallInput generateVideoCallInput = new GenerateVideoCallInput(
                videoCall, UUID.randomUUID(), 1
        );

        doNothing().when(saveTokenPort).saveToken(1L,
                "06553f34383a46cd9f98a656f379792e", 1);
        var result = generateVideoCallService.generateVideoCalling(generateVideoCallInput);
        assertThat(result).isNotNull();
        assertThat(result.getToken().length()).isEqualTo(139);
        assertThat(result.getAppId()).isNotNull();
    }
}
