package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.in;

import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.SaveDocumentResourceController;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.SaveLinkResourceController;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveResourceUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = SaveDocumentResourceController.class)
class SaveDocumentResourceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SaveResourceUseCase saveResourceUseCase;

    @MockBean
    private JwtHelper jwtHelper;

    @Test
    void saveDocumentResourceControllerSuccess() throws Exception {

        var id = 1L;
        var multipartFile = new MockMultipartFile("file", "hello.txt",
                MediaType.TEXT_PLAIN_VALUE, "Hello, World".getBytes());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/appointments/appointment-detail/{idAppointmentDetail}/resource/document", id)
                .file(multipartFile)
                .characterEncoding("UTF-8")
                .header("Content-Type", "application/json")).andExpect(status().is(400));
    }



}
