package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.CancelAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class CancelAppointmentServiceTest {

    private final CancelAppointmentPort cancelAppointmentPort = Mockito.mock(CancelAppointmentPort.class);

    private final CancelAppointmentService cancelAppointmentService = new CancelAppointmentService(cancelAppointmentPort);

    private Appointment appointment;

    @BeforeEach
    public void init(){
        appointment = Appointment.builder().id(1L).specialistUuid(UUID.randomUUID()).patientUuid(UUID.randomUUID())
                .startTime(LocalDateTime.now()).endTime(LocalDateTime.now().plusMinutes(60)).state(1).build();
    }

    @Test
    void cancelAppointmentSuccess(){

        var idAppointment = 1L;

        when(cancelAppointmentPort.cancelAppointment(idAppointment)).thenReturn(appointment);

        var result = cancelAppointmentService.cancelAppointment(idAppointment);

        assertThat(result.getId()).isEqualTo(appointment.getId());
        assertThat(result.getSpecialistUuid()).isEqualTo(appointment.getSpecialistUuid());
        assertThat(result.getPatientUuid()).isEqualTo(appointment.getPatientUuid());
        assertThat(result.getStartTime()).isEqualTo(appointment.getStartTime());
        assertThat(result.getEndTime()).isEqualTo(appointment.getEndTime());
        assertThat(result.getState()).isEqualTo(appointment.getState());

    }

}
