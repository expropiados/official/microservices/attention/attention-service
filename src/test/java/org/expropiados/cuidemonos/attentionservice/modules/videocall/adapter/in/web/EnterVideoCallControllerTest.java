package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web;

import org.expropiados.cuidemonos.attentionservice.config.LocalDateTimePeruZone;
import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.*;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = EnterVideoCallController.class)
class EnterVideoCallControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ValidateEntryCallUseCase validateEntryCallUseCase;
    @MockBean
    private GenerateVideoCallUseCase generateVideoCallUseCase;
    @MockBean
    private GetVideoCallCredentialsUseCase getVideoCallCredentialsUseCase;

    @MockBean
    private JwtHelper jwtHelper;

    @MockBean
    private GetRecordingDataUseCase getRecordingDataUseCase;

    private static final UUID USER_UUID = UUID.randomUUID();

    private static final String AGORA_NAME = "ksdme2nfi39l";

    private VideoCall videoCall;

    @BeforeEach
    void init(){
        Appointment appointment = Appointment.builder().id(1L).specialistUuid(UUID.randomUUID())
                .patientUuid(UUID.randomUUID()).startTime(LocalDateTimePeruZone.now().minusHours(2))
                .endTime(LocalDateTimePeruZone.now().plusHours(2)).state(1).build();
        videoCall = VideoCall.builder().id(1L).appointment(appointment).specialistAgoraUID(12345678)
                .patientAgoraUID(87654321).agoraChannelName(AGORA_NAME).createdAt(LocalDateTimePeruZone.now())
        .build();
    }

    @Test
    void enterVideoCallSpecialistSuccess() throws Exception {
        var enterVideoCall = new EnterVideoCallInput(USER_UUID, 1, AGORA_NAME);
        String params = "{ \"agoraName\": \""+AGORA_NAME+"\"," +
                "\"userType\": \"1\", \"userUUID\":\""+ USER_UUID+"\"}";
        mockMvc.perform(post("/video_call/entry").
                content(params).
                header("Content-Type", "application/json")).andExpect(status().is(400));
    }

    @Test
    void enterVideoCallPatientSuccess() throws Exception {
        var enterVideoCall = new EnterVideoCallInput(USER_UUID, 2, AGORA_NAME);
        String params = "{ \"agoraName\": \""+AGORA_NAME+"\"," +
                "\"userType\": \"2\", \"userUUID\":\""+ USER_UUID+"\"}";
        mockMvc.perform(post("/video_call/entry").
                content(params).
                header("Content-Type", "application/json")).andExpect(status().is(400));
    }
}
