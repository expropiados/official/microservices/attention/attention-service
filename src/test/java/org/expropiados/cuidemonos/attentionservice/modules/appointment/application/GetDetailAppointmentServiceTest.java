package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetDetailAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.out.GetResourcePort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.ResourceOwner;
import org.expropiados.cuidemonos.attentionservice.modules.patient.application.port.out.GetPrePollPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetPatientByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetSpecialistByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetUserPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.out.GetVideoCallPort;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.domain.models.VideoCall;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
@Disabled("Disabled for pass in gitlab")
class GetDetailAppointmentServiceTest {

    private final GetDetailAppointmentPort getDetailAppointmentPort = Mockito.mock(GetDetailAppointmentPort.class);
    private final GetResourcePort getResourcePort = Mockito.mock(GetResourcePort.class);
    private final GetUserPort getUserPort = Mockito.mock(GetUserPort.class);
    private final GetPatientByUserUuidPort getPatientByUserUuidPort = Mockito.mock(GetPatientByUserUuidPort.class);
    private final GetSpecialistByUserUuidPort getSpecialistByUserUuidPort = Mockito.mock(GetSpecialistByUserUuidPort.class);
    private final GetVideoCallPort getVideoCallPort = Mockito.mock(GetVideoCallPort.class);
    private final GetPrePollPort getPrePollPort = Mockito.mock(GetPrePollPort.class);

    private final GetDetailAppointmentService getDetailAppointmentService =
            new GetDetailAppointmentService(getDetailAppointmentPort, getResourcePort, getPatientByUserUuidPort,
                    getSpecialistByUserUuidPort, getVideoCallPort, getPrePollPort);

    private DetailAppointment detailAppointment;
    private Appointment appointment;
    private User patient;
    private User specialist;
    private ResourceOwner patientResources;
    private ResourceOwner specialistResources;
    private VideoCall videoCall;
    private final List<Resource> resources = new ArrayList<>();
    private final List<Resource> linkSpecialistResources = new ArrayList<>();
    private final List<Resource> documentSpecialistResources = new ArrayList<>();
    private final List<Resource> linkPatientResources = new ArrayList<>();
    private final List<Resource> documentPatientResources = new ArrayList<>();
    private final List<Resource> listPatientResources = new ArrayList<>();
    private final List<Resource> listSpecialistResources = new ArrayList<>();


    @BeforeEach
    public void init(){
        appointment = Appointment.builder().id(1L).startTime(LocalDateTime.now()).
                endTime(LocalDateTime.now().plusMinutes(30)).state(2).
                patientUuid(UUID.fromString("6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b")).
                specialistUuid(UUID.fromString("56e896a8-aa21-4ff3-9f8a-a652e9474ba4")).videoCallUuid(UUID.randomUUID()).build();
        detailAppointment = DetailAppointment.builder().appointmentDetailId(1L).annotations("Este es una prueba")
                .results("Esto es una prueba").appointment(appointment).build();
        patient = User.builder().uuid(UUID.fromString("6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b"))
                .dni("77208434").email("steven.fernandez@pucp.edu.pe").fatherLastname("Fernandez").motherLastname("Murga")
                .name("Gianmarco").phoneNumber("987654321").profileImageUrl("https://videollamada.s3.amazonaws.com/gm.PNG")
                .build();
        specialist = User.builder().uuid(UUID.fromString("56e896a8-aa21-4ff3-9f8a-a652e9474ba4"))
                .dni("66524258").email("m.quezada@pucp.edu.pe").fatherLastname("Quezada").motherLastname("Aquino")
                .name("Marco").phoneNumber("985623475").profileImageUrl("https://videollamada.s3.amazonaws.com/marcoQ.PNG")
                .build();
        Resource resourceSpecialistLink = Resource.builder().resourceId(1L).appointmentDetailId(1L).name("Instagram")
                .owner(1).type(0).url("https://www.instagram.com/?hl=es-la").build();
        resources.add(resourceSpecialistLink);
        listSpecialistResources.add(resourceSpecialistLink);
        Resource resourceSpecialistDocument = Resource.builder().resourceId(1L).appointmentDetailId(1L).name("Agua.xlsx")
                .owner(1).type(1).url("https://www.awsAgua.com").build();
        resources.add(resourceSpecialistDocument);
        listSpecialistResources.add(resourceSpecialistDocument);
        linkSpecialistResources.add(resourceSpecialistLink);
        documentSpecialistResources.add(resourceSpecialistDocument);
        specialistResources = ResourceOwner.builder()
                .linkResources(linkSpecialistResources).documentResources(documentSpecialistResources).build();
        Resource resourcePatientLink = Resource.builder().resourceId(2L).appointmentDetailId(1L).name("Mentimeter")
                .owner(0).type(0).url("https://mentimeter.com").build();
        resources.add(resourcePatientLink);
        listPatientResources.add(resourcePatientLink);
        Resource resourcePatientDocument = Resource.builder().resourceId(2L).appointmentDetailId(1L).name("Codigo.py")
                .owner(0).type(1).url("https://awsCodigo.com").build();
        resources.add(resourcePatientDocument);
        listPatientResources.add(resourcePatientDocument);
        linkPatientResources.add(resourcePatientLink);
        documentPatientResources.add(resourcePatientDocument);
        patientResources = ResourceOwner.builder()
                .linkResources(linkPatientResources).documentResources(documentPatientResources).build();
        videoCall = VideoCall.builder().id(1L).appointment(appointment).videoCallUuid(UUID.randomUUID()).build();
    }

    @Test
    void getResourcesOfTypeSuccess(){
        ArrayList<Resource> resourcesOfType = new ArrayList<>();
        listPatientResources.forEach(resource -> {
            if(resource.getType().equals(0)) resourcesOfType.add(resource);
        });
        assertThat(resourcesOfType.size()).isEqualTo(1);
    }

    @Test
    void getResourcesOfOwnerSuccess(){
        ArrayList<Resource> resourcesOfOwner = new ArrayList<>();
        resources.forEach(resource -> {
            if(resource.getOwner().equals(0)) resourcesOfOwner.add(resource);
        });
        assertThat(resourcesOfOwner.size()).isEqualTo(2);
    }

    @Test
    void validateIsCancelableSuccess(){
        var result = getDetailAppointmentService.validateIsCancelable(detailAppointment.getAppointment());
        assertThat(result).isFalse();
        var start = LocalDateTime.now().plusHours(3L);
        detailAppointment.getAppointment().setStartTime(start);
        var resultData = getDetailAppointmentService.validateIsCancelable(detailAppointment.getAppointment());
        assertThat(resultData).isFalse();
        var start2 = LocalDateTime.now().plusHours(4L);
        detailAppointment.getAppointment().setStartTime(start2);
        var resultData2 = getDetailAppointmentService.validateIsCancelable(detailAppointment.getAppointment());
        assertThat(resultData2).isTrue();
    }

    @Test
    void setGetDetailAppointmentSuccess(){

        when(getDetailAppointmentPort.getDetailAppointment(1L)).thenReturn(detailAppointment);

        when(getVideoCallPort.getVideoCallByIdAppointment(detailAppointment.getAppointment().getId())).thenReturn(Optional.ofNullable(videoCall));

        detailAppointment.getAppointment().setVideoCallUuid(videoCall.getVideoCallUuid());

        when(getUserPort.getUser(detailAppointment.getAppointment().getPatientUuid()))
                .thenReturn(java.util.Optional.ofNullable((patient)));

        when(getUserPort.getUser(detailAppointment.getAppointment().getSpecialistUuid()))
                .thenReturn(java.util.Optional.ofNullable(specialist));

        detailAppointment.getAppointment().setPatient(patient);
        detailAppointment.getAppointment().setSpecialist(specialist);

        when(getResourcePort.getResources(1L)).thenReturn(resources);

       detailAppointment.setResourcesSpecialist(specialistResources);
       detailAppointment.setResourcesPatient(patientResources);

        var result = getDetailAppointmentService.getDetailAppointment(1L);


        assertThat(result.getAppointmentDetailId()).isEqualTo(detailAppointment.getAppointmentDetailId());
        assertThat(result.getAnnotations()).isEqualTo(detailAppointment.getAnnotations());
        assertThat(result.getResults()).isEqualTo(detailAppointment.getResults());

        assertThat(result.getAppointment().getSpecialistUuid()).isEqualTo(detailAppointment.getAppointment().getSpecialistUuid());
        assertThat(result.getAppointment().getPatientUuid()).isEqualTo(detailAppointment.getAppointment().getPatientUuid());
        assertThat(result.getAppointment().getStartTime()).isEqualTo(detailAppointment.getAppointment().getStartTime());
        assertThat(result.getAppointment().getEndTime()).isEqualTo(detailAppointment.getAppointment().getEndTime());
        assertThat(result.getAppointment().getState()).isEqualTo(detailAppointment.getAppointment().getState());

        assertThat(result.getAppointment().getPatient().getUuid()).isEqualTo(detailAppointment.getAppointment().getPatient().getUuid());
        assertThat(result.getAppointment().getPatient().getMotherLastname()).isEqualTo(detailAppointment.getAppointment().getPatient().getMotherLastname());
        assertThat(result.getAppointment().getPatient().getFatherLastname()).isEqualTo(detailAppointment.getAppointment().getPatient().getFatherLastname());
        assertThat(result.getAppointment().getPatient().getName()).isEqualTo(detailAppointment.getAppointment().getPatient().getName());
        assertThat(result.getAppointment().getPatient().getDni()).isEqualTo(detailAppointment.getAppointment().getPatient().getDni());
        assertThat(result.getAppointment().getPatient().getEmail()).isEqualTo(detailAppointment.getAppointment().getPatient().getEmail());
        assertThat(result.getAppointment().getPatient().getPhoneNumber()).isEqualTo(detailAppointment.getAppointment().getPatient().getPhoneNumber());
        assertThat(result.getAppointment().getPatient().getProfileImageUrl()).isEqualTo(detailAppointment.getAppointment().getPatient().getProfileImageUrl());

        assertThat(result.getAppointment().getSpecialist().getUuid()).isEqualTo(detailAppointment.getAppointment().getSpecialist().getUuid());
        assertThat(result.getAppointment().getSpecialist().getMotherLastname()).isEqualTo(detailAppointment.getAppointment().getSpecialist().getMotherLastname());
        assertThat(result.getAppointment().getSpecialist().getFatherLastname()).isEqualTo(detailAppointment.getAppointment().getSpecialist().getFatherLastname());
        assertThat(result.getAppointment().getSpecialist().getName()).isEqualTo(detailAppointment.getAppointment().getSpecialist().getName());
        assertThat(result.getAppointment().getSpecialist().getDni()).isEqualTo(detailAppointment.getAppointment().getSpecialist().getDni());
        assertThat(result.getAppointment().getSpecialist().getEmail()).isEqualTo(detailAppointment.getAppointment().getSpecialist().getEmail());
        assertThat(result.getAppointment().getSpecialist().getPhoneNumber()).isEqualTo(detailAppointment.getAppointment().getSpecialist().getPhoneNumber());
        assertThat(result.getAppointment().getSpecialist().getProfileImageUrl()).isEqualTo(detailAppointment.getAppointment().getSpecialist().getProfileImageUrl());

        assertThat(result.getResourcesPatient().getDocumentResources().size()).isEqualTo(1);
        assertThat(result.getResourcesPatient().getLinkResources().size()).isEqualTo(1);

        assertThat(result.getResourcesSpecialist().getDocumentResources().size()).isEqualTo(1);
        assertThat(result.getResourcesSpecialist().getLinkResources().size()).isEqualTo(1);

    }


}
