package org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.impl.AvailabilityPersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.adapter.out.persistence.mappers.AvailabilityMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({AvailabilityPersistenceAdapter.class})
@ComponentScan(basePackageClasses = {AvailabilityMapper.class})
@ContextConfiguration
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Disabled("Disable because of but sql")
class AvailabilityPersistenceAdapterTest {

    @Autowired
    private AvailabilityPersistenceAdapter availabilityPersistenceAdapter;
    private LocalDate dateBegin;
    private LocalDate dateEnd;

    @BeforeEach
    void init(){
        dateBegin = LocalDate.of(2021, 6, 8);
        dateEnd = LocalDate.of(2021, 6, 9);
    }


    @Test
    @Sql("classpath:sql/GetAvailabilitiesOfDateTest.sql")
    void getAvailabilitiesOfDateSuccess(){

        var availabilities = availabilityPersistenceAdapter.getSpecialistsAvailabilitiesOfDate(dateBegin, dateEnd);
        assertThat(availabilities.size()).isEqualTo(5);

    }

}
