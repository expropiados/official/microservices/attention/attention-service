package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.out.persistence;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.impl.AppointmentPersistentAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({AppointmentPersistentAdapter.class})
@ComponentScan(basePackageClasses = {AppointmentMapper.class})
@ContextConfiguration
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Disabled("Disable because of but sql")
class AppointmentPersistenceAdapterTest {

    @Autowired
    private AppointmentPersistentAdapter appointmentPersistentAdapter;

    @Test
    void bookAppointmentSuccess(){
        var appointment = Appointment.builder().id(1L).specialistUuid(UUID.randomUUID())
                .patientUuid(UUID.randomUUID()).startTime(LocalDateTime.now()).endTime(LocalDateTime.now())
                .state(1).build();

        var appointmentBooked = appointmentPersistentAdapter.bookAppointment(appointment);

        assertThat(appointmentBooked.getId()).isEqualTo(appointment.getId());
        assertThat(appointmentBooked.getSpecialistUuid()).isEqualTo(appointment.getSpecialistUuid());
        assertThat(appointmentBooked.getPatientUuid()).isEqualTo(appointment.getPatientUuid());
        assertThat(appointmentBooked.getStartTime()).isEqualTo(appointment.getStartTime());
        assertThat(appointmentBooked.getEndTime()).isEqualTo(appointment.getEndTime());
        assertThat(appointmentBooked.getState()).isEqualTo(appointment.getState());

    }

    @Test
    @Sql({"classpath:sql/CancelAppointmentTest.sql"})
    void cancelAppointmentSuccess(){
        var appointmentUpdated = appointmentPersistentAdapter.cancelAppointment(1L);

        assertThat(appointmentUpdated.getId()).isEqualTo(1L);
        assertThat(appointmentUpdated.getState()).isEqualTo(1);
        assertThat(appointmentUpdated.getPatientUuid()).isEqualTo(UUID.fromString("7d09d7ff-2bda-4d99-91d1-5ecb7d1a9499"));
        assertThat(appointmentUpdated.getSpecialistUuid()).isEqualTo(UUID.fromString("31ea6b7b-ed28-4699-853c-8447da1048d3"));
        assertThat(appointmentUpdated.getStartTime()).isEqualTo(
                LocalDateTime.of(2021, 5, 30, 13, 0, 0));
        assertThat(appointmentUpdated.getEndTime()).isEqualTo(
                LocalDateTime.of(2021, 5, 30, 15, 0, 0 ));

    }

}
