package org.expropiados.cuidemonos.attentionservice.modules.user.adapter.port.out.presistence;

import org.assertj.core.api.Assertions;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.impl.UserPersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.mappers.PatientMapper;
import org.expropiados.cuidemonos.attentionservice.modules.user.adapter.out.persistence.mappers.SpecialistMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({UserPersistenceAdapter.class})
@ComponentScan(basePackageClasses = {SpecialistMapper.class, PatientMapper.class})
@ContextConfiguration
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Disabled("Disable because of but sql")
class UserPersistenceAdapterTest {

    @Autowired
    private UserPersistenceAdapter userPersistenceAdapter;
    private final List<UUID> uuids = new ArrayList<>();
    private final UUID uuidPatient = UUID.fromString("6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b");
    private final UUID uuidSpecialist = UUID.fromString("56e896a8-aa21-4ff3-9f8a-a652e9474ba4");

    @BeforeEach
    void init(){
        uuids.add(UUID.fromString("56e896a8-aa21-4ff3-9f8a-a652e9474ba4"));
        uuids.add(UUID.fromString("56e896a8-aa21-4ff3-9f8a-a652e9474ba4"));
        uuids.add(UUID.fromString("56e896a8-aa21-4ff3-9f8a-a652e9474ba4"));
        uuids.add(UUID.fromString("6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b"));
        uuids.add(UUID.fromString("6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b"));
    }

    @Test
    @Sql("classpath:sql/GetUserTest.sql")
    void listPatientsSuccess(){

        var mapPatients = userPersistenceAdapter.listPatients(uuids);
        assertThat(mapPatients.size()).isEqualTo(1);

    }

    @Test
    @Sql("classpath:sql/ListSpecialistsTest.sql")
    void listSpecialistsSuccess(){

        var specialists = userPersistenceAdapter.listSpecialists(uuids);

        Assertions.assertThat(specialists).hasSize(1);

    }

    @Test
    @Sql("classpath:sql/GetUserTest.sql")
    void getPatientByUserUuidSuccess(){

        var patient = userPersistenceAdapter.getPatientByUserUuid(uuidPatient);
        patient.ifPresent(user -> {
            assertThat(user.getUuid()).isEqualTo(UUID.fromString("6bbd85e2-c41c-11eb-81d1-16fd24d7dd8b"));
            assertThat(user.getDni()).isEqualTo("77208434");
            assertThat(user.getName()).isEqualTo("Gianmarco");
            assertThat(user.getFatherLastname()).isEqualTo("Fernandez");
            assertThat(user.getMotherLastname()).isEqualTo("Murga");
            assertThat(user.getEmail()).isEqualTo("steven.fernandez@pucp.edu.pe");
            assertThat(user.getPhoneNumber()).isEqualTo("987654321");
            assertThat(user.getProfileImageUrl()).isEqualTo("https://videollamada.s3.amazonaws.com/gm.PNG");
        });

    }

    @Test
    @Sql("classpath:sql/GetUserTest.sql")
    void getSpecialistByUserUuidSuccess(){

        var specialist = userPersistenceAdapter.getSpecialistByUserUuid(uuidSpecialist);
        specialist.ifPresent(user -> {
            assertThat(user.getUuid()).isEqualTo(UUID.fromString("56e896a8-aa21-4ff3-9f8a-a652e9474ba4"));
            assertThat(user.getDni()).isEqualTo("66524258");
            assertThat(user.getName()).isEqualTo("Marco");
            assertThat(user.getFatherLastname()).isEqualTo("Quezada");
            assertThat(user.getMotherLastname()).isEqualTo("Aquino");
            assertThat(user.getEmail()).isEqualTo("m.quezada@pucp.edu.pe");
            assertThat(user.getPhoneNumber()).isEqualTo("986547123");
            assertThat(user.getProfileImageUrl()).isEqualTo("https://videollamada.s3.amazonaws.com/marcoQ.PNG");
        });

    }

}
