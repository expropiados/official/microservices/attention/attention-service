package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveLinkResourceDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveResourcePort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;
import org.expropiados.cuidemonos.attentionservice.thirdparty.StorageAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.Random;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class SaveResourceServiceTest {

    private final SaveResourcePort saveResourcePort = Mockito.mock(SaveResourcePort.class);
    private final StorageAdapter storageAdapter = Mockito.mock(StorageAdapter.class);

    private final SaveResourceService saveResourceService = new SaveResourceService(saveResourcePort, storageAdapter);

    private Resource linkResource;
    private Resource documentResource;
    private final SaveLinkResourceDTO saveLinkResourceDTO = new SaveLinkResourceDTO();
    private final MultipartFile multipartFile = new MockMultipartFile("file", "hello.txt",
            MediaType.TEXT_PLAIN_VALUE, "Hello, World".getBytes());

    @BeforeEach
    public void init(){
        linkResource = Resource.builder().resourceId(new Random().nextLong()).appointmentDetailId(1L)
                .name("Mentimeter").type(0).owner(1).url("www.mentimeter.com").build();
        documentResource = Resource.builder().resourceId(new Random().nextLong()).appointmentDetailId(1L)
                .name("Agua.xlsx").type(1).owner(0).url("www.awsS3Url.com").build();
    }

    @Test
    void saveLinkResourceSuccess(){

        when(saveResourcePort.saveResource(any(Resource.class))).thenReturn(linkResource);

        saveLinkResourceDTO.setName(linkResource.getName());
        saveLinkResourceDTO.setUrl(linkResource.getUrl());

        var result = saveResourceService.saveLinkResource(1L, saveLinkResourceDTO);

        assertThat(result.getResourceId()).isEqualTo(linkResource.getResourceId());
        assertThat(result.getAppointmentDetailId()).isEqualTo(linkResource.getAppointmentDetailId());
        assertThat(result.getType()).isEqualTo(linkResource.getType());
        assertThat(result.getOwner()).isEqualTo(linkResource.getOwner());
        assertThat(result.getName()).isEqualTo(linkResource.getName());

    }

    @Test
    void saveDocumentResourceSuccess(){

        String documentUrl = "www.awsS3Url.com";
        when(storageAdapter.saveFile(multipartFile)).thenReturn(documentUrl);
        when(saveResourcePort.saveResource(any(Resource.class))).thenReturn(documentResource);

        var result = saveResourceService.saveDocumentResource(1L, multipartFile, 1);

        assertThat(result.getResourceId()).isEqualTo(documentResource.getResourceId());
        assertThat(result.getAppointmentDetailId()).isEqualTo(documentResource.getAppointmentDetailId());
        assertThat(result.getName()).isEqualTo(documentResource.getName());
        assertThat(result.getType()).isEqualTo(documentResource.getType());
        assertThat(result.getOwner()).isEqualTo(documentResource.getOwner());
        assertThat(result.getUrl()).isEqualTo(documentUrl);

    }

}
