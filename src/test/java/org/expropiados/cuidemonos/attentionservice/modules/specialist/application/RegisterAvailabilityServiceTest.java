package org.expropiados.cuidemonos.attentionservice.modules.specialist.application;

import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.DeleteAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.RegisterAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@Tag("UnitTest")
class RegisterAvailabilityServiceTest {

    private final RegisterAvailabilityPort registerAvailabilityPort = Mockito.mock(RegisterAvailabilityPort.class);
    private final DeleteAvailabilityPort deleteAvailabilityPort = Mockito.mock(DeleteAvailabilityPort.class);
    private final GetAvailabilityPort getAvailabilityPort = Mockito.mock(GetAvailabilityPort.class);

    private final RegisterAvailabilityService registerAvailabilityService =
            new RegisterAvailabilityService(registerAvailabilityPort, deleteAvailabilityPort, getAvailabilityPort);

    private final List<Availability> availabilities = new ArrayList<>();
    private final UUID specialistUuid = UUID.randomUUID();
    private final LocalDate dateBegin = LocalDate.of(2021, 6, 23);
    private final LocalDate dateEnd = LocalDate.of(2021, 6, 24);
    private final Map<LocalDateTime, Availability> map = new HashMap<>();
    private final Integer replicate = 1;

    @BeforeEach
    public void init(){
        var availability = Availability.builder().id(1L).startTime(LocalDateTime.now())
                .endTime(LocalDateTime.now().plusMinutes(30)).state(0).build();
        availabilities.add(availability);
        map.put(LocalDateTime.now(),availability);
    }

    @Test
    void registerAvailabilitySuccess(){
        if(replicate==0){
            doNothing().when(deleteAvailabilityPort).deleteAvailability(specialistUuid, dateBegin, dateEnd);
            doNothing().when(registerAvailabilityPort).registerAvailability(availabilities);
        }
        else {
            doNothing().when(deleteAvailabilityPort).deleteAvailability(specialistUuid, dateBegin, dateEnd);
            doNothing().when(registerAvailabilityPort).registerAvailability(availabilities);
            registerAvailabilityService.registerAvailability(specialistUuid, availabilities, 2, dateBegin, dateEnd);
        }
    }

    @Test
    void replicateAvailabilitySuccess(){
        doNothing().when(registerAvailabilityPort).registerAvailability(availabilities);
        var result = registerAvailabilityService.replicateAvailability(availabilities, map, specialistUuid,2);
    }

    @Test
    void completeAvailabilitySuccess(){
        doNothing().when(registerAvailabilityPort).registerAvailability(availabilities);
        registerAvailabilityService.completeAvailability(availabilities,UUID.randomUUID());
    }

    @Test
    void getAvailabilityFreeSuccess(){
        doNothing().when(registerAvailabilityPort).registerAvailability(availabilities);
        registerAvailabilityService.getAvailabilityFree(availabilities);
    }

    @Test
    void createReplicatedAvailabilitySuccess(){
        doNothing().when(registerAvailabilityPort).registerAvailability(availabilities);
        registerAvailabilityService.createReplicatedAvailability(availabilities, map, specialistUuid, replicate);
    }



}
