package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.persistence;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.AppointmentMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.impl.VideoCallPersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.out.mappers.VideoCallMapper;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisteredVideoCall;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.util.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({VideoCallPersistenceAdapter.class})
@ComponentScan(basePackageClasses = {VideoCallMapper.class, AppointmentMapper.class})
@ContextConfiguration
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Sql("classpath:sql/SaveAppointmentTest.sql")
@Disabled("Disable because of but sql")
class RegisterVideoCallPersistenceTest {
    @Autowired
    private VideoCallPersistenceAdapter videoCallPersistenceAdapter;
    private Appointment appointment;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @BeforeEach
    void init(){
        UUID specialistUUID = UUID.fromString("7d09d7ff-2bda-4d99-91d1-5ecb7d1a9499");
        UUID tokenUUID = UUID.fromString("31ea6b7b-ed28-4699-853c-8447da1048d3");
        String str = "2021-05-30 13:00:00";
        String str2 = "2021-05-30 15:00:00";
        LocalDateTime startDate = LocalDateTime.parse(str, formatter);
        LocalDateTime endDate = LocalDateTime.parse(str2, formatter);
        appointment = Appointment.builder().id(1L).specialistUuid(specialistUUID).patientUuid(tokenUUID)
                .startTime(startDate).endTime(endDate).state(1).build();
    }

    @Test
    void registerVideoCallSuccess(){
        var registeredVideoCall = new RegisteredVideoCall(
                new RandomString().nextString(),
                new RandomString().randomIntegerValue(),
                new RandomString().randomIntegerValue(),
                1L);
        var videoCall = videoCallPersistenceAdapter.registerVideoCall(registeredVideoCall, appointment);
        assertThat(videoCall).isEqualTo(registeredVideoCall.getAgoraChannelName());
    }
}
