package org.expropiados.cuidemonos.attentionservice.modules.appointment.application;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.BookAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SaveDetailAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.SendEmailForAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Appointment;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.DetailAppointment;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilityPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetPatientByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.GetSpecialistByUserUuidPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class BookAppointmentServiceTest {

    private final BookAppointmentPort bookAppointmentPort = Mockito.mock(BookAppointmentPort.class);
    private final GetAvailabilityPort getAvailabilityPort = Mockito.mock(GetAvailabilityPort.class);
    private final SaveDetailAppointmentPort saveDetailAppointmentPort = Mockito.mock(SaveDetailAppointmentPort.class);
    private final GetPatientByUserUuidPort getPatientByUserUuidPort = Mockito.mock(GetPatientByUserUuidPort.class);
    private final GetSpecialistByUserUuidPort getSpecialistByUserUuidPort = Mockito.mock(GetSpecialistByUserUuidPort.class);
    private final SendEmailForAppointmentPort sendEmailForAppointmentPort = Mockito.mock(SendEmailForAppointmentPort.class);

    private final BookAppointmentService bookAppointmentService =
            new BookAppointmentService(bookAppointmentPort,getAvailabilityPort, saveDetailAppointmentPort,
                    getSpecialistByUserUuidPort, getPatientByUserUuidPort, sendEmailForAppointmentPort);

    private Availability availability;
    private User patient;
    private User specialist;
    private Appointment appointment;
    private DetailAppointment detailAppointment;

    @BeforeEach
    public void init(){
        availability = Availability.builder().id(new Random().nextLong()).startTime(LocalDateTime.now())
                .endTime(LocalDateTime.now().plusMinutes(60)).state(1).build();
        patient = User.builder().email("gianmarco101997@gmail.com").dni("77208434").name("Gianmarco")
                .fatherLastname("Fernandez").motherLastname("Murga").build();
        specialist = User.builder().email("gianmarco101997@gmail.com").dni("77208434").name("Gianmarco")
                .fatherLastname("Fernandez").motherLastname("Murga").build();
        appointment = Appointment.builder().id(new Random().nextLong()).specialistUuid(UUID.randomUUID())
                .patientUuid(UUID.randomUUID()).patient(patient).specialist(specialist).build();
        detailAppointment = DetailAppointment.builder().appointmentDetailId(new Random().nextLong())
                .appointment(appointment).build();
    }

    @Test
    void bookAppointmentSuccess(){

        when(getAvailabilityPort.changeStateAvailability(availability.getId()))
                .thenReturn(Optional.ofNullable(availability));

        appointment.setStartTime(availability.getStartTime());
        appointment.setEndTime(availability.getEndTime());
        appointment.setState(0);

        when(bookAppointmentPort.bookAppointment(any(Appointment.class))).thenReturn(appointment);

        doNothing().when(saveDetailAppointmentPort).saveDetailAppointment(detailAppointment);

        var result = bookAppointmentService.bookAppointment(appointment,1L);

        assertThat(result.getId()).isEqualTo(appointment.getId());
        assertThat(result.getPatientUuid()).isEqualTo(appointment.getPatientUuid());
        assertThat(result.getSpecialistUuid()).isEqualTo(appointment.getSpecialistUuid());
        assertThat(result.getStartTime()).isEqualTo(appointment.getStartTime());
        assertThat(result.getEndTime()).isEqualTo(appointment.getEndTime());
        assertThat(result.getState()).isEqualTo(appointment.getState());

    }
}
