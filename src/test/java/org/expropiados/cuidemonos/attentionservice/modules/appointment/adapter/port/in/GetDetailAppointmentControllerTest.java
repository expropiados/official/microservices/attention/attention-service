package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.in;

import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.GetDetailAppointmentController;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.mappers.GetDetailAppointmentDTOMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.in.GetDetailAppointmentUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = GetDetailAppointmentController.class)
@ComponentScan(basePackageClasses = {GetDetailAppointmentDTOMapper.class})
class GetDetailAppointmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GetDetailAppointmentUseCase getDetailAppointmentUseCase;

    @MockBean
    private GetDetailAppointmentDTOMapper getDetailAppointmentDTOMapper;

    @MockBean
    private JwtHelper jwtHelper;

    @Test
    void getDetailAppointmentSuccess() throws Exception {

        var idAppointment = 1L;

        mockMvc.perform(get("/appointments/{idAppointment}/appointment-detail", idAppointment))
                .andExpect(status().is(400));

    }

}
