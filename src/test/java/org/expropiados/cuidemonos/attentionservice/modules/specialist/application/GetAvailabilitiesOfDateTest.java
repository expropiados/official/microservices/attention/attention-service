package org.expropiados.cuidemonos.attentionservice.modules.specialist.application;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.out.GetAppointmentPort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.commands.AvailabilityOfDateCommand;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.application.port.out.GetAvailabilitiesOfDatePort;
import org.expropiados.cuidemonos.attentionservice.modules.specialist.domain.Availability;
import org.expropiados.cuidemonos.attentionservice.modules.user.application.port.out.ListPatientsPort;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.Specialist;
import org.expropiados.cuidemonos.attentionservice.modules.user.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
@Disabled("disabled for test passed")
class GetAvailabilitiesOfDateTest {

    private final GetAvailabilitiesOfDatePort getAvailabilitiesOfDatePort = Mockito.mock(GetAvailabilitiesOfDatePort.class);
    private final GetAppointmentPort getAppointmentPort = Mockito.mock(GetAppointmentPort.class);
    private final ListPatientsPort listPatientsPort = Mockito.mock(ListPatientsPort.class);

    private final GetAvailabilitiesOfDateService getAvailabilitiesOfDateService =
            new GetAvailabilitiesOfDateService(getAvailabilitiesOfDatePort, getAppointmentPort,listPatientsPort);

    private final LocalDate dateBegin = LocalDate.now();
    private final LocalDate dateEnd = LocalDate.now().plusDays(1L);
    private final UUID uuidSpecialist = UUID.fromString("fcf0a95e-bf7b-11eb-bca3-16fd24d7dd8b");
    private final ArrayList<Availability> availabilities = new ArrayList<>();
    private final ArrayList<Availability> availabilitiesOfDate = new ArrayList<>();
    private final ArrayList<UUID> uuids = new ArrayList<>();
    private final Map<UUID, User> mapSpecialist = new HashMap<>();
    private final ArrayList<Specialist> specialists = new ArrayList<>();
    private final ArrayList<AvailabilityOfDateCommand> availabilityOfDateCommands= new ArrayList<>();
    private final ArrayList<AvailabilityOfDateCommand> availabilityOfDateCommandsTest= new ArrayList<>();
    private final ArrayList<Specialist> specialistsOfDate = new ArrayList<>();

    @BeforeEach
    public void init(){
        var availability1 = Availability.builder().id(1L).specialistUuid(uuidSpecialist)
                .startTime(LocalDateTime.now().plusHours(4L))
                .endTime(LocalDateTime.now().plusHours(5L))
                        .state(0).build();
        availabilities.add(availability1);
        var user = User.builder().uuid(uuidSpecialist).dni("06939096")
                .name("Marco").fatherLastname("Quezada").motherLastname("Aquino")
                .profileImageUrl("https://videollamada.s3.amazonaws.com/marcoQ.PNG").build();
        mapSpecialist.put(uuidSpecialist, user);
        var specialist = Specialist.builder().user(user).availabilities(availabilities).build();
        specialistsOfDate.add(specialist);
        availabilityOfDateCommands.add(AvailabilityOfDateCommand.builder().date(dateBegin)
                .specialists(specialistsOfDate).build());
    }


    @Test
    void getAvailabilitiesOfDateSuccess(){

        Stream.iterate(dateBegin, date -> date.plusDays(1L))
                .limit(ChronoUnit.DAYS.between(dateBegin, dateEnd)+1)
                .forEach(date -> availabilityOfDateCommandsTest.add(AvailabilityOfDateCommand.builder()
                        .date(date).build()));

        assertThat(availabilityOfDateCommandsTest.size()).isEqualTo(2);

        when(getAvailabilitiesOfDatePort.getSpecialistsAvailabilitiesOfDate(dateBegin, dateEnd)).thenReturn(availabilities);

        availabilities.forEach(availability -> uuids.add(availability.getSpecialistUuid()));
        assertThat(uuids.size()).isEqualTo(availabilities.size());

        when(listPatientsPort.listSpecialists(uuids)).thenReturn(mapSpecialist);

        mapSpecialist.forEach((uuid, user) -> {
            var specialist = Specialist.builder().user(user).build();
            specialists.add(specialist);
        });

        specialists.forEach(specialist -> {
            var uuidSpe = specialist.getUser().getUuid();
            ArrayList<Availability> availabilitiesList = new ArrayList<>();
            availabilities.forEach(availability -> {
                if (availability.getSpecialistUuid().equals(uuidSpe)) availabilitiesList.add(availability);
            });
            specialist.setAvailabilities(availabilitiesList);
        });

        availabilityOfDateCommands.forEach(availabilityOfDateCommand -> {
            var dateStart = availabilityOfDateCommand.getDate().atStartOfDay();
            var dateFinalize = availabilityOfDateCommand.getDate().plusDays(1L).atStartOfDay();
            List<Specialist> specialistForDate = new ArrayList<>();
            specialists.forEach(specialist -> {
                List<Availability> availabilityList = new ArrayList<>();
                specialist.getAvailabilities().forEach(availability -> {
                    if(availability.getStartTime().isAfter(dateStart) &&
                            availability.getEndTime().isBefore(dateFinalize)
                            && availability.getStartTime().isAfter(LocalDateTime.now())) availabilityList.add(availability);
                });
                if(availabilityList.size() != 0)
                    specialistForDate.add(Specialist.builder().user(specialist.getUser())
                            .availabilities(availabilityList).build());
            });
            availabilityOfDateCommand.setSpecialists(specialistForDate);
        });

        var result = getAvailabilitiesOfDateService.getSpecialistsAvailabilitiesOfRangeDate(dateBegin, dateEnd);

        assertThat(result.size()).isEqualTo(2);
        assertThat(result.get(0).getDate()).isEqualTo(LocalDate.now());
        assertThat(result.get(1).getDate()).isEqualTo(LocalDate.now().plusDays(1L));
        assertThat(result.get(0).getSpecialists().size()).isEqualTo(1);
        assertThat(result.get(1).getSpecialists().size()).isZero();
        assertThat(result.get(0).getSpecialists().get(0).getAvailabilities().size()).isEqualTo(1);
    }

}
