package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.out.persistence;

import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.impl.ResourcePersistenceAdapter;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.out.persistence.mappers.ResourceMapper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.domain.Resource;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({ResourcePersistenceAdapter.class})
@ComponentScan(basePackageClasses = {ResourceMapper.class})
@ContextConfiguration
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@Sql({"classpath:sql/GetResourcesTest.sql"})
@Disabled("Disable because of but sql")
class ResourcePersistenceAdapterTest {

    @Autowired
    private ResourcePersistenceAdapter resourcePersistenceAdapter;

    @Test
    void getResourcesSuccess(){

        var idAppointmentDetail = 1L;

        var resources = resourcePersistenceAdapter.getResources(idAppointmentDetail);

        assertThat(resources.size()).isEqualTo(2);

    }

    @Test
    @Sql({"classpath:sql/SaveResourceTest.sql"})
    void saveResourceSuccess(){
        var resource = Resource.builder().resourceId(3L).appointmentDetailId(1L)
                .name("Agua.xlsx").type(0).owner(1).url("www.awsS3Url.com").build();

        var resourceSaved = resourcePersistenceAdapter.saveResource(resource);

        assertThat(resourceSaved.getAppointmentDetailId()).isEqualTo(resource.getAppointmentDetailId());
        assertThat(resourceSaved.getName()).isEqualTo(resource.getName());
        assertThat(resourceSaved.getOwner()).isEqualTo(resource.getOwner());
        assertThat(resourceSaved.getType()).isEqualTo(resource.getType());
        assertThat(resourceSaved.getUrl()).isEqualTo(resource.getUrl());

    }



}
