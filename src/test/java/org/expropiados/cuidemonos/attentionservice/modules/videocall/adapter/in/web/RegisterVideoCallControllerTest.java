package org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web;

import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.adapter.in.web.RegisterVideoCallController;
import org.expropiados.cuidemonos.attentionservice.modules.videocall.application.port.in.RegisterVideoCallUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = RegisterVideoCallController.class)
class RegisterVideoCallControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RegisterVideoCallUseCase registerVideoCallUseCase;

    @MockBean
    private JwtHelper jwtHelper;

    @Test
    void registerVideoCallSuccess() throws Exception {
        String params = "{ \"appointmentId\": \""+1L+"\"}";
        mockMvc.perform(post("/video_call").
                content(params).
                header("Content-Type", "application/json")).andExpect(status().is(400));
    }
}
