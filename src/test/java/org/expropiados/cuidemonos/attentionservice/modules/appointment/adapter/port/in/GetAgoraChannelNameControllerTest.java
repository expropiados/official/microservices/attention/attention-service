package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.in;

import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.GetAgoraChannelNameController;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.GetAgoraChannelNameUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = GetAgoraChannelNameController.class)
class GetAgoraChannelNameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JwtHelper jwtHelper;

    @MockBean
    private GetAgoraChannelNameUseCase getAgoraChannelNameUseCase;

    @Test
    void getAgoraChannelNameSuccess() throws Exception {

        var idAppointment = 1L;

        mockMvc.perform(get("/appointments/{idAppointment}/video-call/agora-channel", idAppointment))
                .andExpect(status().is(400));
    }

}
