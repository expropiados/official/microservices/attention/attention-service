package org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.port.in;

import org.expropiados.cuidemonos.attentionservice.helpers.jwt.JwtHelper;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.adapter.in.SaveLinkResourceController;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveLinkResourceDTO;
import org.expropiados.cuidemonos.attentionservice.modules.appointment.application.port.in.SaveResourceUseCase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = SaveLinkResourceController.class)
class SaveLinkResourceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SaveResourceUseCase saveResourceUseCase;
    private final SaveLinkResourceDTO saveLinkResourceDTO = new SaveLinkResourceDTO();

    @MockBean
    private JwtHelper jwtHelper;

    @Test
    void saveLinkResourceControllerSuccess() throws Exception {

        var id = 1L;
        var name = "Mentimeter";
        var url = "www.mentimeter.com";
        saveLinkResourceDTO.setName(name);
        saveLinkResourceDTO.setUrl(url);

        String params = "{ \"name\": \""+name+"\""
                +", \"url\": \""+url+"\"}";

        mockMvc.perform(post("/appointments/appointment-detail/{idAppointmentDetail}/resource/link", id)
                .content(params)
                .header("Content-Type", "application/json")).andExpect(status().is(400));

    }



}
